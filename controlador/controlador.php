<?php 

	require_once dirname(__DIR__). "/modelo/modelo.php";

	require_once dirname(__DIR__). "/modelo/config.php";

	require_once dirname(__DIR__). "/admin/modelos/salas.modelo.php";



	class Vista

	{

		#Llamada a la plantilla



		static public function plantilla()

		{

			include "vistas/plantilla.php";

		}



		#Interaccion del usuario



		static public function valorPagina()

		{

			if(isset($_GET['ruta']))

			{

				if($_GET['ruta']== 'registro')

				{ 

					$_GET['ruta'] = 'admin';

					$pag = $_GET['ruta'];

				}

				else

				{

					$pag= $_GET['ruta'];

				}

			}

			else

			{

				$pag= 'inicio';

			}



			return $pag;	        

		}



		static public function enlacesPaginasControlador()

		{

			$pag=Vista::valorPagina();



			if (!empty($pag))

			{

				$enlacesController = $pag;	

			}

			else

			{

				$enlacesController = "inicio";

			}



			$respuesta = VistaModelo::enlacesPaginasModelo($enlacesController);



			include $respuesta;

		}



		static public function baseUrl()

		{

			$host = $_SERVER["HTTP_HOST"];

		    $url  = $_SERVER["REQUEST_URI"];

			$dir  = "bingo/" ;



		    if($host == 'localhost' || $host == '192.168.1.16')

		    {

		    	$baseUrl="http://".$host."/";
				$baseUrl = $baseUrl.$dir;

		    }

		    else

		    {

		    	$baseUrl="https://".$host."/";
				$baseUrl = $baseUrl;

		    }			


		    return $baseUrl;

		}



		static public function redirigir()

		{

		    if(!isset($_SESSION["user"]) && empty($_SESSION["user"]))

		    {

		     	echo '<script>



						swal({

							  type: "error",

							  title: "Debe de iniciar sesion para acceder a esta seccion",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "inicio";



										}

									})



						</script>';		        

		    }

		}



		static public function salaScript($id, $tiempo, $id_juego, $baseUrl)

		{

			$script ='<script>

						/*=============================================

						CONTADOR SALAS NORMAL

						=============================================*/	

							var end'.$id.' = new Date("'.$tiempo.'");

						    var _second'.$id.' = 1000;

						    var _minute'.$id.' = _second'.$id.' * 60;

						    var _hour'.$id.' = _minute'.$id.' * 60;

						    var _day'.$id.' = _hour'.$id.' * 24;

						    var timer'.$id.';



						    function showRemaining'.$id.'() {

						        var now'.$id.' = new Date();

						        var distance'.$id.' = end'.$id.' - now'.$id.';

						        if (distance'.$id.' < 0) {



						            clearInterval(timer'.$id.');																				

									$.ajax({

					                    url: "'.$baseUrl.'public/tiempo.php",

					                    data: {action: "consulta",

					                    	   id_juego: '.$id_juego.'},

					                    	   type: "POST",

					                    success: function (response) 

					                    {

					                    	let cartilla = JSON.parse(response);

					        				if(cartilla[0]["total"] == 0)

					        				{

					        					$.ajax({

								                    url: "'.$baseUrl.'public/tiempo.php",

								                    data: {action: "conteo",

								                    	   tiempo: now'.$id.',

								                    	   idSala : '.$id.'},

								                    type: "POST",

								                    success: function (response) 

								                    {

								                    	location.reload();

								                    }

								            	});

					        				}

					        				else

					        				{

					        					$("#countdown'.$id.'").css({"display" : "none"});

												$("#countdownoff'.$id.'").css({"display" : "block","background-color": "#2E2963"});

					        				}

					                    }

					            	});

						        }

						        else

						        {

							        var days'.$id.' = Math.floor(distance'.$id.' / _day'.$id.');

							        var hours'.$id.' = Math.floor((distance'.$id.' % _day'.$id.') / _hour'.$id.');

							        var minutes'.$id.' = Math.floor((distance'.$id.' % _hour'.$id.') / _minute'.$id.');

							        var seconds'.$id.' = Math.floor((distance'.$id.' % _minute'.$id.') / _second'.$id.');



							        if(minutes'.$id.' != 0 && seconds'.$id.' < 10)

						        	{

						        		$("#countdown'.$id.'").html(minutes'.$id.'+":0"+seconds'.$id.');

						        	}

						        	else if(minutes'.$id.' == 0 && seconds'.$id.' < 10)

						        	{

						        		$("#countdown'.$id.'").html("0"+seconds'.$id.');

						        	}

						        	else if(minutes'.$id.' > 0)

						        	{						        		

						        		$("#countdown'.$id.'").html(minutes'.$id.'+":"+seconds'.$id.');

						        	}

						        	else

						        	{

						        		$("#countdown'.$id.'").html("0"+seconds'.$id.');

						        	}

						        	

						        }

						    }



						    timer'.$id.' = setInterval(showRemaining'.$id.', 1000);

						</script>';



			return $script;

		}

	}



	class Usuario

	{

		static public function crearUsuario()

		{

			if(isset($_POST['nuevoUsuario']))

			{

				$tabla= 'usuario';

				$rol = "jugador";

				$item = null;

				$valor = null;



				$respuesta = UsuarioModelo::iniciaSesionModelo($tabla, $item, $valor);

				//$ruta = $_GET['ruta'];

				$validar = '';

				$validarC = '';



				foreach ($respuesta as $key => $value) 

				{

					if($value['usuario'] == strtolower($_POST["nuevoUsuario"]))

					{

						$validar="existe";

					}

					if($value['correo'] == strtolower($_POST["nuevoCorreo"]))

					{

						$validarC="existe";

					}						

				}



				if($validar == 'existe')

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "El nombre de usuario ya esta en uso pruebe con otro",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "registrarse";



										}

									})



						</script>'; 

				}

				elseif($validarC == 'existe')

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "El correo electronico ya esta en uso pruebe con otro",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "registrarse";



										}

									})



						</script>'; 

				}				

				else

				{

 

					if(openssl_decrypt($_POST["tipoRegistro"], COD, KEY) == '1')

					{

						$datos = array("rol"              => "jugador",

									   "usuario"          => strtolower($_POST["nuevoUsuario"]),

									   "nombre"           => $_POST["nuevoNombre"],

									   "fecha_nacimiento" => $_POST["nuevaFecha"],

									   "apellido"         => $_POST["nuevoApellido"],

									   "pais"             => $_POST["nuevoPais"],

									   "correo"           =>strtolower($_POST["nuevoCorreo"]) ,

									   "ciudad"           =>$_POST["nuevaCiudad"],

									   "contrasena"       =>openssl_encrypt($_POST["nuevaContrasena"], COD, KEY),

									   "patrocinador"     => "admin",

									   "imagen"           => "vistas/img/perfil/default.jpg");

					}

					elseif (openssl_decrypt($_POST["tipoRegistro"], COD, KEY) == '0') 

					{

						$datos = array("rol"              => "jugador",

									   "usuario"          => strtolower($_POST["nuevoUsuario"]),

									   "nombre"           => $_POST["nuevoNombre"],

									   "fecha_nacimiento" => $_POST["nuevaFecha"],

									   "apellido"         => $_POST["nuevoApellido"],

									   "pais"             => $_POST["nuevoPais"],

									   "correo"           =>strtolower($_POST["nuevoCorreo"]) ,

									   "ciudad"           =>$_POST["nuevaCiudad"],

									   "contrasena"       =>openssl_encrypt($_POST["nuevaContrasena"], COD, KEY),

									   "patrocinador"     => strtolower($ruta),

									   "imaegn"           => "vistas/img/perfil/default.jpg");

					}



					$respuesta = UsuarioModelo::crearUsuarioModelo($tabla, $rol, $datos);



					if($respuesta=="ok")

					{

						$saldo = '0.00';

						$monedas = '0.00';

						$titular_cuenta = "";

						$identificacion = "";

						$banco = "";

						$tipo_cuenta = "";

						$num_cuenta = "";



						$Usuario = UsuarioModelo::obtenerUsuarioModelo($tabla);

						$tabla= 'usuario';

						$item = "usuario";

						$valor = strtolower($_POST["nuevoUsuario"]);

						$respuesta = UsuarioModelo::iniciaSesionModelo($tabla, $item, $valor);

						
						$tabla= 'cuenta';

						$id = $respuesta[0]['id'];

						$datos = array(	"id_usuario"     => $id,

										"saldo"          => $saldo,

										"monedas"        => $monedas,

										"titular_cuenta" => $titular_cuenta,

										"identificacion" => $identificacion,

										"banco"          => $banco,

										"tipo_cuenta"    => $tipo_cuenta,

										"numero_cuenta"  => $num_cuenta);



						$crearCuenta = UsuarioModelo::crearCuentaModelo($tabla, $datos);



						if($crearCuenta == 'ok')

						{

							echo '<script>



								swal({

									  type: "success",

									  title: "Usuario registrado correctamente",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "inicio";



												}

											})



								</script>';

						}

						else

						{
							$crearCuenta = implode(" ",$crearCuenta);
							echo '<script>



								swal({

									  type: "error",

									  title: "'.$crearCuenta.' Usuario creado, pero hubo un error al registrar su cuenta de saldo contacte con soporte",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "ayuda";



												}

											})



								</script>';							

						}

					}

					else

					{

						echo '<script>



							swal({

								  type: "error",

								  title: "Ups, algo salio mal. Intente nuevamente. Si este mensaje persiste contacte con soporte",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "registrarse";



											}

										})



							</script>';					

					}

				}

			}

		}



        static public function contacto()

        {

            if(isset($_POST["nombreContacto"]))

            {

        		$correoCliente = $_POST['correoContacto'];

        		$nombreCliente = ucwords($_POST['nombreContacto']);

        		$asuntoMensaje = $_POST['asuntoContacto'];

        		$msg='

    

    			<br />

    			      <div class="box-c  col-lg-5" style="margin-left: 2%;padding-right: 6px;padding-left: 2px;">

    

    				        <div class="box-header-c with-border">

    				    	<img src="https://ganafutbol.com/vistas/img/icono-negro.png" alt="anacon100" border="0">

    				         <h3 style="color: black;"><center>CONTACTO</center></h3>

    

    				        </div>

    

    				        <div class="box-body-c">

    				            <p>Saludos cordiales</p>

    				            <p>Mi nombre es '.ucwords($_POST['nombreContacto']).' '.ucwords($_POST['apellidoContacto']).'</p>

    				            <p><strong>'.$_POST['mensajeContacto'].'.</strong></p>

    				            <p>Sin mas que decir me despido esperando su pronta respuesta a mi dirección de correo electronico: '.$correoCliente.'</p>

    

    				          </div>

    				          <div class="box-footer-c" style="border-bottom-left-radius: 16px;border-bottom-right-radius: 16px;background-color: green;"></div>

    				    

    

    				      </div>';					

    

    			$mail = new phpmailer();

    			$mail -> From = "soporte@solucioneskp.com";

    			$mail -> FromName = $nombreCliente;

    			$mail -> Subject = $asuntoContacto;

    

    			$mail -> AddAddress ('soporte@solucioneskp.com');

    

    			$mail -> Body = $msg;

    

    			$mail -> IsHTML (true);

    			if($mail -> Send ())

    			{

    				echo '<script>

    

    					swal({

    						  type: "success",

    						  title: "Su mensaje ha sido enviado correctamente, nos pondremos en contacto.",

    						  showConfirmButton: true,

    						  confirmButtonText: "Cerrar"

    						  }).then(function(result){

    									if (result.value) {

    

    									window.location = "contacto";

    

    									}

    								})

    

    					</script>';						

    

    			}

    			else

    			{

    				echo '<script>

    

    					swal({

    						  type: "error",

    						  title: "Ocurrio un error al enviar el mensaje. Nuestro equipo de soporte esta trabajando para solventar esto pedimos disculpa.",

    						  showConfirmButton: true,

    						  confirmButtonText: "Cerrar"

    						  }).then(function(result){

    									if (result.value) {

    

    									window.location = "contacto";

    

    									}

    								})

    

    					</script>';	

    

    			}    

            }

        }



		static public function aceptarTerminos()

		{

			if(isset($_POST['terminos']))

			{

				$tabla = "usuario";

				$item  = "id";

				$valor = $_SESSION['user_id'];

				$datos = openssl_decrypt($_POST["terminos"], COD, KEY);



				$respuesta = UsuarioModelo::aceptarTerminosModelo($tabla, $item, $valor, $datos);



                if($respuesta == 'ok')

                {

    				echo '<script>

    

    					swal({

    						  type: "success",

    						  title: "Terminos y condiciones aceptados",

    						  showConfirmButton: true,

    						  confirmButtonText: "Cerrar"

    						  }).then(function(result){

    									if (result.value) {

    

    									window.location = "billetera";

    

    									}

    								})

    					</script>';

    					die();                

                }

                else

                {

                	unset($_SESSION);

    				echo '<script>

    

    					swal({

    						  type: "error",

    						  title: "Ocurrio un error, ingrese nuevamente",

    						  showConfirmButton: true,

    						  confirmButtonText: "Cerrar"

    						  }).then(function(result){

    									if (result.value) {

    

    									window.location = "ingreso";

    

    									}

    								})

    					</script>';                	

                }				

			}

		}



		static public function verificarTerminos()

		{

			$tabla = "usuario";

			$item  = "id";

			$valor = $_SESSION['user_id'];



			$respuesta = UsuarioModelo::verificarTerminosModelo($tabla, $item, $valor);



			return $respuesta;

		}			



		static public function validarCorreo()

		{

			if(isset($_POST['recuperaCorreo']))

			{

	            if( $_SESSION['token'] == null)

	            {

		            $tabla = "usuario";

		            $item  = "correo";

		            $valor = strtolower($_POST['recuperaCorreo']);



		            $respuesta = UsuarioModelo::validarCorreoModelo($tabla, $item, $valor);



		            if($respuesta[0]['correo'] == $valor)

		            {	

	    				$tabla = "usuario";

			            $item  = "id";

			            $valor = $respuesta[0]['id'];

		            	$token = mt_rand(10000, 99999);

		            	$_SESSION['token'] = $token;

		            	$_SESSION['userid'] = $respuesta[0]['id'];



		            	$actualizarToken = UsuarioModelo::actualizarTokenModelo($tabla, $item, $valor, $token);



		            	if($actualizarToken == 'ok')

		            	{	

		            		$correoCliente = $respuesta[0]['correo'];

		            		$nombreCliente = $respuesta[0]['usuario'];

		            		$msg='

			

							<br />

							      <div class="box-c  col-lg-5" style="margin-left: 2%;padding-right: 6px;padding-left: 2px;">



								        <div class="box-header-c with-border">

								    	<img src="https://ganafutbol.com/vistas/img/icono-negro.png" alt="anacon100" border="0">

								         <h3 style="color: black;"><center>Recuperar Contraseña</center></h3>



								        </div>



								        <div class="box-body-c">

								          

								            <p><strong>Hola '.ucwords($respuesta[0]["usuario"]).'.</strong> Codigo para cambiar tu contraseña si no lo solicitaste haz caso omizo de este mensaje.</p>

								            <p>codigo: '.$token.'</p>

								            <p>Este codigo sirve para poder cambiar la contraseña cuando la ha olvidado, solo puede solicitar un codigo a la vez, si el codigo vence debera solicitar otro.</p>



								          </div>

								          <div class="box-footer-c" style="border-bottom-left-radius: 16px;border-bottom-right-radius: 16px;background-color: green;"></div>

								    



								      </div>';					



							$mail = new phpmailer();

							$mail -> From = "info@ganafutbol.com";

							$mail -> FromName = $nombreCliente;

							$mail -> Subject = "GANA FUTBOL Recuperar contraseña";



							$mail -> AddAddress ($correoCliente);



							$mail -> Body = $msg;



							$mail -> IsHTML (true);

							if($mail -> Send ())

							{

								echo '<script>



									swal({

										  type: "success",

										  title: "Correo validado, se ha enviado un codigo de seguridad utilizalo para cambiar tu contraseña.",

										  showConfirmButton: true,

										  confirmButtonText: "Cerrar"

										  }).then(function(result){

													if (result.value) {



													window.location = "recuperaContrasenaToken";



													}

												})



									</script>';						



							}else

							{

								echo '<script>



									swal({

										  type: "error",

										  title: "Correo validado, pero ocurrio un error al enviar el codigo. Puede ser que no sea una dirreccion de correo electronica valida",

										  showConfirmButton: true,

										  confirmButtonText: "Cerrar"

										  }).then(function(result){

													if (result.value) {



													window.location = "recuperaContrasena";



													}

												})



									</script>';	



							}	         

		    			}

			            else

			            {

		    				echo '<script>

		    

		    					swal({

		    						  type: "error",

		    						  title: "Error al enviar el codigo",

		    						  showConfirmButton: true,

		    						  confirmButtonText: "Cerrar"

		    						  }).then(function(result){

		    									if (result.value) {

		    

		    									window.location = "recuperaContrasena";

		    

		    									}

		    								})

		    					</script>';

			            }		    				

		            }

		            else

		            {

	    				echo '<script>

	    

	    					swal({

	    						  type: "error",

	    						  title: "El correo indicado no esta registrado",

	    						  showConfirmButton: true,

	    						  confirmButtonText: "Cerrar"

	    						  }).then(function(result){

	    									if (result.value) {

	    

	    									window.location = "recuperaContrasena";

	    

	    									}

	    								})

	    					</script>';

		            }	

		        }

                else

	            {

					echo '<script>



						swal({

							  type: "error",

							  title: "Ya fue enviado un codigo a su correo.",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "recuperaContrasenaToken";



										}

									})

						</script>';

	            }				

			}		

		}

		

		static public function validarToken()

		{

			if(isset($_POST["recuperaToken"]))

			{

				if($_SESSION['token'] != null && $_SESSION['token'] == $_POST["recuperaToken"])

				{

		            $tabla = "usuario";

		            $item  = "codigo";

		            $valor = $_POST["recuperaToken"];



		            $respuesta = UsuarioModelo::validarTokenModelo($tabla, $item, $valor);		

			    	

			    	if($respuesta[0]["codigo"] == $valor)

			    	{			

	    				echo '<script>

	    					swal({

	    						  type: "success",

	    						  title: "Codigo correcto, ahora ingresa tu nueva contraseña",

	    						  showConfirmButton: true,

	    						  confirmButtonText: "Cerrar"

	    						  }).then(function(result){

	    									if (result.value) {	

	    									

								            window.location = "recuperaContrasenaCambiar";



	    									}

	    								})

	    					</script>';	   



    				}

    				else

    				{

	    				echo '<script>

	    

	    					swal({

	    						  type: "error",

	    						  title: "Codigo utilizado o caducado. Solicite uno nuevo",

	    						  showConfirmButton: true,

	    						  confirmButtonText: "Cerrar"

	    						  }).then(function(result){

	    									if (result.value) {

	    

	    									window.location = "recuperaContrasena";

	    

	    									}

	    								})

	    					</script>';	    					

    				}

				}

				else

				{

    				echo '<script>

    

    					swal({

    						  type: "error",

    						  title: "Codigo de recuperación invalido o caducado. Solicite uno nuevo",

    						  showConfirmButton: true,

    						  confirmButtonText: "Cerrar"

    						  }).then(function(result){

    									if (result.value) {

    

    									window.location = "recuperaContrasena";

    

    									}

    								})

    					</script>';	    					

				}

			}

		}



		static public function cambiarContrasena()

		{



			if(isset($_POST["recuperaContrasena"]))

			{

				$tabla = "usuario";

				$item  = "id";

				$token = mt_rand(10000, 99999);

				$valor = $_SESSION['userid'];

				$contrasena = openssl_encrypt($_POST["recuperaContrasena"], COD, KEY);

				$datos = array( "contrasena" => $contrasena,

								"token"      => $token);



				$respuesta = UsuarioModelo::actualizarContrasenaModelo($tabla, $item, $valor, $datos);



				if($respuesta == 'ok')

				{

    				echo '<script>

    

    					swal({

    						  type: "success",

    						  title: "Su nueva contraseña fue guardada con exito",

    						  showConfirmButton: true,

    						  confirmButtonText: "Cerrar"

    						  }).then(function(result){

    									if (result.value) {

    

    									window.location = "ingreso";

    

    									}

    								})

    					</script>';						

				}

				else

				{

    				echo '<script>

    

    					swal({

    						  type: "error",

    						  title: "No se pudo guardar su contraseña",

    						  showConfirmButton: true,

    						  confirmButtonText: "Cerrar"

    						  }).then(function(result){

    									if (result.value) {

    

    									window.location = "recuperaContrasena";

    

    									}

    								})

    					</script>';						

				}

			}

		}		



		static public function iniciaSesion()

		{

			if(isset($_POST['usuario']))

			{

				$tabla= 'usuario';

				$item = "usuario";

				$valor = strtolower($_POST['usuario']);

				$usuario = strtolower($_POST['usuario']);

				$contrasena =$_POST['contrasena'];



				$respuesta = UsuarioModelo::iniciaSesionModelo($tabla, $item, $valor);



				if(!empty($respuesta) && isset($respuesta))

				{

					if($respuesta[0]['usuario'] == $usuario && openssl_decrypt($respuesta[0]['contrasena'], COD, KEY)== $contrasena)

					{

						$_SESSION['user'] = strtolower($usuario);

						$_SESSION['user_id'] = $respuesta[0]['id'];

						$_SESSION['user_rol'] = $respuesta[0]['rol'];

						echo '<script>



							swal({

								  type: "success",

								  title: "Datos correctos ¡Bienvenido!",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "sala";



											}

										})

							</script>';

					}

					elseif($respuesta[0]['usuario'] != $usuario && openssl_decrypt($respuesta[0]['contrasena'], COD, KEY)== $contrasena)

					{

						echo '<script>



							swal({

								  type: "error",

								  title: "Ups, usuario incorrecto",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											$("#modalIniciaSesion").modal("show");



											}

										})



							</script>';	



					}

					elseif($respuesta[0]['usuario'] == $usuario && openssl_decrypt($respuesta[0]['contrasena'], COD, KEY)!= $contrasena)

					{

						echo '<script>



							swal({

								  type: "error",

								  title: "Ups, contrasena erronea.",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											$("#modalIniciaSesion").modal("show");



											}

										})



							</script>';	

					}

					else

					{

						echo '<script>



							swal({

								  type: "error",

								  title: "Ups, ha habido un error contacte con soporte",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "registrarse";



											}

										})



							</script>';	

					}

				}	

				else

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "Los datos suministrados no existen, cree su cuenta",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										$("#modalRegistro").modal("show");



										}

									})



						</script>';	

				}										

			}		

		}



		static public function recargaUsuario()

		{

			if(isset($_POST['titularRecarga']))

			{	

				$parametros = EstiloModelo::mostrarParametrosModelo();



				if($_POST['montoRecarga'] < $parametros["minimo_parametros"])

				{

					$montoRecarga = $_POST['montoRecarga'];

					echo '<script>



						swal({

							  type: "error",

							  title: "Usted intenta recargar un monto de $ '.number_format($montoRecarga,2).'<br> Pero la recarga minima permitida es de $ '.$parametros["minimo_parametros"].'",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "recargas";



										}

									})



						</script>';						

				}				

				else

				{                    

                    if($_POST['montoRecarga'] >= 300)

                    {

                        //$monto =$_POST['montoRecarga']+ (10*$_POST['montoRecarga']/100);

                        $monto =$_POST['montoRecarga'];

                    }

                    else

                    {

                        $monto =$_POST['montoRecarga'];

                    }

                    

					$tabla='recargas';



					$datos = array(	"id_usuario"             => $_SESSION['user_id'],

									"titular_recarga"        => $_POST["titularRecarga"],

									"cedula_recarga"         => $_POST["documentoRecarga"],

									"banco_recarga"          => $_POST["bancoRecarga"],

									"monto_recarga"          => $monto,

									"fecha_recarga"          => $_POST["fechaRecarga"],

									"numero_referencia"      => $_POST["referenciaRecarga"],

  									"estado_recarga"         => 'pendiente',

									"tipo_transaccion"       => 'recarga',

									"monto_transaccion"      => $monto,

									"referencia_transaccion" => date("Y").$monto,

  									"estado_transaccion"     => 'pendiente');



					$respuesta = UsuarioModelo::recargaUsuarioModelo($tabla, $datos);



					if($respuesta== 'ok')

					{

						echo '<script>



							swal({

								  type: "success",

								  title: "Orden de recarga creada",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "recargas";



											}

										})



							</script>';								

					}

					else

					{

						echo '<script>



							swal({

								  type: "error",

								  title: "Ups, ha habido un error al recarga intente de nuevo, si el error persiste contacte con soporte",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "recargas";



											}

										})



							</script>';	

					}

					

				}

			}

		}



		static public function retirarUsuario()

		{

			if(isset($_POST['montoRetirar']))

			{	

				$parametros   = EstiloModelo::mostrarParametrosModelo();				



				if($_POST['montoRetirar'] < $parametros["minimo_retiro"])

				{

					$montoRetirar = $_POST['montoRetirar'];

					echo '<script>



						swal({

							  type: "error",

							  title: "Usted intenta retirar un monto de $ '.number_format($montoRetirar,2).'<br> Pero el retiro minimo permitida es de $ '.$parametros["minimo_retiro"].'",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "retirar";



										}

									})



						</script>';						

				}				

				else

				{      					

                    if($_POST['montoRetirar'] >= 300)

                    {

                        //$monto =$_POST['montoRetirar']+ (10*$_POST['montoRetirar']/100);

                        $monto =$_POST['montoRetirar'];

                    }

                    else

                    {

                        $monto =$_POST['montoRetirar'];

                    }

                    

					$tabla='retirar';



					$datos = array(	"id_usuario"             => $_SESSION['user_id'],

									"monto_retiro"           => $monto,

  									"estado_retiro"          => 'pendiente',

  									"fecha_retiro"           => date("Y-m-d"),

									"tipo_transaccion"       => 'retiro',

									"monto_transaccion"      => $monto,

									"referencia_transaccion" => date("Y").$monto,

  									"estado_transaccion"     => 'pendiente');



					$limiteDiario = UsuarioModelo::mostrarRetirarUsuarioModelo($tabla, $datos);



					if($limiteDiario["retiro"]+$monto <= $parametros["maximo_retiro"])

					{

						$respuesta = UsuarioModelo::retirarUsuarioModelo($tabla, $datos);



						if($respuesta== 'ok')

						{

							echo '<script>



								swal({

									  type: "success",

									  title: "Orden de retiro creada",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "retirar";



												}

											})



								</script>';								

						}

						else

						{

							echo '<script>



								swal({

									  type: "error",

									  title: "Ups, ha habido un error al retirar intente de nuevo, si el error persiste contacte con soporte",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "retirar";



												}

											})



								</script>';	

						}						

					}

					else

					{

						if($parametros["maximo_retiro"]-$limiteDiario["retiro"] > 0)

						{

							$intente = '<br> Puede pedir un monto de $'.number_format($parametros["maximo_retiro"]-$limiteDiario["retiro"],2).' o intentar mañana';

						}

						else

						{

							$intente = '<br> Intente nuevamente mañana';

						}

						echo '<script>



							swal({

								  type: "error",

								  title: "El dia de hoy usted ha solicitado un monto total de $ '.number_format($limiteDiario["retiro"],2).' para retirar. <br> El retiro maximo permitida diario es de $ '.$parametros["maximo_retiro"].$intente.'",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "retirar";



											}

										})



							</script>';							

					}

					

				}

			}

		}





		static public function mostrarBalance()

		{

			$tabla ='cuenta';

			$id_usuario =$_SESSION['user_id'];



			$respuesta = UsuarioModelo::mostrarBalanceModelo($tabla, $id_usuario);



			return $respuesta;			

		}			



		static public function cambiarInfoPerfil()

		{

			if(isset($_POST["1"]))

			{

				/*=============================================

				VALIDAR IMAGEN

				=============================================*/



			   	$ruta = $_POST["imagenActual"];



			   	if(isset($_FILES["editarImagen"]["tmp_name"]) && !empty($_FILES["editarImagen"]["tmp_name"])){



					list($ancho, $alto) = getimagesize($_FILES["editarImagen"]["tmp_name"]);



					$nuevoAncho = 168;

					$nuevoAlto = 178;



					/*=============================================

					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO

					=============================================*/



					$directorio = "vistas/img/perfil/".$_SESSION['user_id'];



					mkdir($directorio, 0755);



					/*=============================================

					PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD

					=============================================*/



					if(!empty($_POST["imagenActual"]) && $_POST["imagenActual"] != "vistas/img/perfil/default.jpg"){



						unlink($_POST["imagenActual"]);

						rmdir($_POST["imagenActual"]);



					}else{



						mkdir($directorio, 0755);	

					

					}



					

					/*=============================================

					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP

					=============================================*/



					if($_FILES["editarImagen"]["type"] == "image/jpeg"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta = "vistas/img/perfil/".$_SESSION['user_id']."/".$aleatorio.".jpg";



						$origen = imagecreatefromjpeg($_FILES["editarImagen"]["tmp_name"]);						



						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);



						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);



						imagejpeg($destino, $ruta);



					}



					if($_FILES["editarImagena"]["type"] == "image/png"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta = "vistas/img/perfil/".$_SESSION['user_id']."/".$aleatorio.".png";



						$origen = imagecreatefrompng($_FILES["editarImagen"]["tmp_name"]);						



						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);



						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);



						imagepng($destino, $ruta);



					}

				}



				$tabla = "usuario";

				$item  = "id";

				$valor = "1";



 				$datos = array ("fecha_nacimiento" => $_POST["editarFecha"],

								"pais"             => $_POST["editarPais"],

								"ciudad"           => $_POST["editarCiudad"],

								"imagen"           => $ruta,

								"id_usuario"       => $_SESSION["user_id"]);



				$respuesta = UsuarioModelo::cambiarInfoPerfilrModelo($tabla, $item, $valor, $datos);



				if($respuesta == 'ok')

				{

					echo '<script>



						swal({

							  type: "success",

							  title: "¡Datos actualizados!",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "perfil";



										}

									})



						</script>';	

				}

				else

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "No pudimos modificar tus datos. Si es primera vez que recibes este error verifica los datos y intenta nuevamente, <a href="contacto">si el error persiste contacta con soporte</a>",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "perfil";



										}

									})



						</script>';	

				}

			}

			elseif(isset($_POST["2"]))

			{

				$tabla = "usuario";

				$item  = "id";

				$valor = $_SESSION["user_id"];



				$contraseña = UsuarioModelo::mostrarInfoPerfilModelo($tabla, $item, $valor);



				if($_POST["contrasena"] == openssl_decrypt($contraseña["contrasena"], COD, KEY))

				{

					if($_POST["editarContrasena"] == $_POST["validarContrasena"])

					{



						$tabla = "usuario";

						$item  = "id";

						$valor = "2";



		 				$datos = array ("contrasena" => openssl_encrypt($_POST["editarContrasena"], COD, KEY),

										"id_usuario" => $_SESSION["user_id"]);



						$respuesta = UsuarioModelo::cambiarInfoPerfilrModelo($tabla, $item, $valor, $datos);



						if($respuesta == 'ok')

						{

							echo '<script>



								swal({

									  type: "success",

									  title: "¡Datos actualizados!",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "perfil";



												}

											})



								</script>';	

						}

						else

						{

							echo '<script>



								swal({

									  type: "error",

									  title: "No pudimos modificar tus datos. Si es primera vez que recibes este error verifica los datos y intenta nuevamente, <a href="contacto">si el error persiste contacta con soporte</a>",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "perfil";



												}

											})



								</script>';	

						}						

					}

					else

					{

						echo '<script>



							swal({

								  type: "error",

								  title: "La contraseña y la validacion deben ser iguales.",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "perfil";



											}

										})



							</script>';							

					}

				}

				else

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "La contraseña actual no es correcta.",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "perfil";



										}

									})



						</script>';							

				}



			}

			elseif(isset($_POST["3"]))

			{

				$tabla = "usuario";

				$item  = "id";

				$valor = $_SESSION["user_id"];



				$contraseña = UsuarioModelo::mostrarInfoPerfilModelo($tabla, $item, $valor);



				if($_POST["contrasena"] == openssl_decrypt($contraseña["contrasena"], COD, KEY))

				{

					$tabla = "usuario";

					$item  = "id";

					$valor = "3";



	 				$datos = array ("correo"     => $_POST["editarCorreo"],

	 								"telefono"   => $_POST["editarTelefono"],

									"id_usuario" => $_SESSION["user_id"]);



					$respuesta = UsuarioModelo::cambiarInfoPerfilrModelo($tabla, $item, $valor, $datos);	



					if($respuesta == 'ok')

					{

						echo '<script>



							swal({

								  type: "success",

								  title: "¡Datos actualizados!",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "perfil";



											}

										})



							</script>';	

					}

					else

					{

						echo '<script>



							swal({

								  type: "error",

								  title: "No pudimos modificar tus datos. Si es primera vez que recibes este error verifica los datos y intenta nuevamente, <a href="contacto">si el error persiste contacta con soporte</a>",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "perfil";



											}

										})



							</script>';	

					}

				}

				else

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "La contraseña actual no es correcta.",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "perfil";



										}

									})



						</script>';							

				}				

			}

			elseif(isset($_POST["4"]))

			{

				$tabla = "usuario";

				$item  = "id";

				$valor = $_SESSION["user_id"];



				$contraseña = UsuarioModelo::mostrarInfoPerfilModelo($tabla, $item, $valor);



				if($_POST["contrasena"] == openssl_decrypt($contraseña["contrasena"], COD, KEY))

				{

					$tabla = "cuenta_banco";

					$item  = "id";

					$valor = "4";



	 				$datos = array ("cedula"        => $_POST["editarCedula"],

	 								"titular"       => $_POST["editarTitular"],

	 								"correo"        => $_POST["editarCorreoTitular"],

	 								"banco"         => $_POST["editarBanco"],

	 								"tipo_cuenta"   => $_POST["editarTipoCuenta"],

	 								"numero_cuenta" => $_POST["editarNumeroCuenta"],

									"id_usuario"    => $_SESSION["user_id"]);



					$respuesta = UsuarioModelo::cambiarInfoPerfilrModelo($tabla, $item, $valor, $datos);	



					if($respuesta == 'ok')

					{

						echo '<script>



							swal({

								  type: "success",

								  title: "¡Datos actualizados!",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "perfil";



											}

										})



							</script>';	

					}

					else

					{

						echo '<script>



							swal({

								  type: "error",

								  title: "No pudimos modificar tus datos. Si es primera vez que recibes este error verifica los datos y intenta nuevamente, <a href="contacto">si el error persiste contacta con soporte</a>",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  }).then(function(result){

											if (result.value) {



											window.location = "perfil";



											}

										})



							</script>';	

					}

				}

				else

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "La contraseña actual no es correcta.",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "perfil";



										}

									})



						</script>';							

				}				

			}

			elseif(isset($_POST["5"]))

			{

				$tabla = "usuario";

				$item  = "id";

				$valor = $_SESSION["user_id"];



				$contraseña = UsuarioModelo::mostrarInfoPerfilModelo($tabla, $item, $valor);



				if($_POST["contrasena"] == openssl_decrypt($contraseña["contrasena"], COD, KEY))

				{

					$tabla='cuenta';

					$tabla2='usuario';



					$datos = array( "id_usuario" => $_SESSION['user_id'],

								 	"monto"      => $_POST["enviarMonto"],

								 	"usuario"    => strtolower($_POST["enviarUsuario"]));



					if(isset($_POST["enviarUsuario"]))

					{

						$respuesta = UsuarioModelo::transferiSaldoModelo($tabla, $tabla2, $datos);



						if ($respuesta== 'ok')

						{

							echo '<script>



								swal({

									  type: "success",

									  title: "Transferencia exitosa",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "perfil";



												}

											})



								</script>';						

						}

						elseif($respuesta == 'insuficiente')

						{

							echo '<script>



								swal({

									  type: "error",

									  title: "El monto enviado es superior al disponible",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "perfil";



												}

											})



								</script>';						

						}

						elseif($respuesta == 'igual')

						{

							echo '<script>



								swal({

									  type: "error",

									  title: "No puede transferirse saldo a si mismo",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "perfil";



												}

											})



								</script>';						

						}					

					    elseif($respuesta == 'noexiste')

						{

							echo '<script>



								swal({

									  type: "error",

									  title: "El usuario al que intenta transferir no existe",

									  showConfirmButton: true,

									  confirmButtonText: "Cerrar"

									  }).then(function(result){

												if (result.value) {



												window.location = "perfil";



												}

											})



								</script>';						

						}

					}		

				}

				else

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "La contraseña actual no es correcta.",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "perfil";



										}

									})



						</script>';							

				}				

			}

			elseif(isset($_POST["6"]))

			{

		   		/*=============================================

				VALIDAR IMAGEN

				=============================================*/



			   	$ruta = $_POST["imagenLogoActual"];



			   	if(isset($_FILES["editarImagenLogo"]["tmp_name"]) && !empty($_FILES["editarImagenLogo"]["tmp_name"])){



					list($ancho, $alto) = getimagesize($_FILES["editarImagenLogo"]["tmp_name"]);



					$nuevoAncho = 175;

					$nuevoAlto = 25;



					/*=============================================

					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO

					=============================================*/



					$directorio = "vistas/img/parametros/logo";



					/*=============================================

					PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD

					=============================================*/



					if(!empty($_POST["imagenLogoActual"]) && $_POST["imagenLogoActual"] != "vistas/img/parametros/logo.png"){



						unlink($_POST["imagenLogoActual"]);



					}else{



						//mkdir($directorio, 0755);	

					

					}

					

					/*=============================================

					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP

					=============================================*/



					if($_FILES["editarImagenLogo"]["type"] == "image/jpeg"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta = "vistas/img/parametros/logo/logo.jpg";



						$destino = $_FILES["editarImagenLogo"]["tmp_name"];

						

						copy($destino, $ruta);



					}



					if($_FILES["editarImagenLogo"]["type"] == "image/png"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta = "vistas/img/parametros/logo/logo.png";



						$destino = $_FILES["editarImagenLogo"]["tmp_name"];

						

						copy($destino, $ruta);	



					}



				}		



			   	$ruta2 = $_POST["imagenFaviconActual"];



			   	if(isset($_FILES["editarImagenFavicon"]["tmp_name"]) && !empty($_FILES["editarImagenFavicon"]["tmp_name"])){



					list($ancho, $alto) = getimagesize($_FILES["editarImagenFavicon"]["tmp_name"]);



					$nuevoAncho = 64;

					$nuevoAlto = 64;



					/*=============================================

					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO

					=============================================*/



					$directorio = "vistas/img/parametros/favicon";



					/*=============================================

					PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD

					=============================================*/



					if(!empty($_POST["imagenFaviconActual"]) && $_POST["imagenFaviconActual"] != "vistas/img/parametros/favicon.png"){



						unlink($_POST["imagenFaviconActual"]);



					}else{



						//mkdir($directorio, 0755);	

					

					}

					

					/*=============================================

					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP

					=============================================*/



					if($_FILES["editarImagenFavicon"]["type"] == "image/jpeg"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta2 = "vistas/img/parametros/favicon/favicon.jpg";



						$destino = $_FILES["editarImagenFavicon"]["tmp_name"];

						

						copy($destino, $ruta2);	



					}



					if($_FILES["editarImagenFavicon"]["type"] == "image/png"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta2 = "vistas/img/parametros/favicon/favicon.png";												



						$destino = $_FILES["editarImagenFavicon"]["tmp_name"];

						

						copy($destino, $ruta2);	



					}



				}	

				

			   	$ruta3 = $_POST["imagenFooterActual"];



			   	if(isset($_FILES["editarImagenFooter"]["tmp_name"]) && !empty($_FILES["editarImagenFooter"]["tmp_name"])){



					list($ancho, $alto) = getimagesize($_FILES["editarImagenFooter"]["tmp_name"]);



					$nuevoAncho = 486;

					$nuevoAlto = 451;



					/*=============================================

					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO

					=============================================*/



					$directorio = "vistas/img/parametros/footer";



					/*=============================================

					PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD

					=============================================*/



					if(!empty($_POST["imagenFooterActual"]) && $_POST["imagenFooterActual"] != "vistas/img/parametros/footerImg.png"){



						unlink($_POST["imagenFooterActual"]);



					}else{



						//mkdir($directorio, 0755);	

					

					}

					

					/*=============================================

					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP

					=============================================*/



					if($_FILES["editarImagenFooter"]["type"] == "image/jpeg"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta3 = "vistas/img/parametros/footer/footer.jpg";;						



						$destino = $_FILES["editarImagenFooter"]["tmp_name"];						



						copy($destino, $ruta3);						



					}



					if($_FILES["editarImagenFooter"]["type"] == "image/png"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta3 = "vistas/img/parametros/footer/footer.png";



						$destino = $_FILES["editarImagenFooter"]["tmp_name"];



     					copy($destino, $ruta3);						



					}



				}									





				$tabla = "parametros";

				$item  = "id";

				$valor = "6";



 				$datos = array ("nombre"         => $_POST["editarNombre"],

								"minimo_recarga" => $_POST["editarMinimoRe"],

								"maximo_recarga" => $_POST["editarMaximoRe"],

								"minimo_retiro"  => $_POST["editarMinimo"],

								"maximo_retiro"  => $_POST["editarMaximo"],

								"imagen"         => $ruta,

								"imagen2"        => $ruta2,

								"imagen3"        => $ruta3);



				$respuesta = UsuarioModelo::cambiarInfoPerfilrModelo($tabla, $item, $valor, $datos);



				if($respuesta == 'ok')

				{

					echo '<script>



						swal({

							  type: "success",

							  title: "¡Datos actualizados!",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "inicio";



										}

									})



						</script>';	

				}

				else

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "No pudimos modificar tus datos. Si es primera vez que recibes este error verifica los datos y intenta nuevamente, <a href="contacto">si el error persiste contacta con soporte</a>",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "inicio";



										}

									})



						</script>';	

				}

			}

			elseif(isset($_POST["7"]))

			{

		   		/*=============================================

				VALIDAR IMAGEN

				=============================================*/



			   	$ruta = $_POST["imagenBancoActual"];



			   	if(isset($_FILES["editarImagenBanco"]["tmp_name"]) && !empty($_FILES["editarImagenBanco"]["tmp_name"])){



					list($ancho, $alto) = getimagesize($_FILES["editarImagenBanco"]["tmp_name"]);



					$nuevoAncho = 175;

					$nuevoAlto = 25;



					/*=============================================

					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO

					=============================================*/



					$directorio = "vistas/img/parametros/banco";



					/*=============================================

					PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD

					=============================================*/



					if(!empty($_POST["imagenBancoActual"]) && $_POST["imagenBancoActual"] != "vistas/img/parametros/logo.png"){



						unlink($_POST["imagenBancoActual"]);



					}else{



						//mkdir($directorio, 0755);	

					

					}

					

					/*=============================================

					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP

					=============================================*/



					if($_FILES["editarImagenBanco"]["type"] == "image/jpeg"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta = "vistas/img/parametros/banco/banco.jpg";



						$destino = $_FILES["editarImagenBanco"]["tmp_name"];

						

						copy($destino, $ruta);



					}



					if($_FILES["editarImagenBanco"]["type"] == "image/png"){



						/*=============================================

						GUARDAMOS LA IMAGEN EN EL DIRECTORIO

						=============================================*/



						$aleatorio = mt_rand(100,999);



						$ruta = "vistas/img/parametros/banco/banco.png";



						$destino = $_FILES["editarImagenBanco"]["tmp_name"];

						

						copy($destino, $ruta);	



					}



				}									



				$tabla = "parametros";

				$item  = "id";

				$valor = "7";



 				$datos = array ("titular"  => $_POST["editarTitular"],

								"cedula"   => $_POST["editarCedula"],

								"correo"   => $_POST["editarCorreo"],

								"banco"    => $_POST["editarBanco"],

								"tipo"     => $_POST["editarTipo"],

								"imagen"   => $ruta,

								"numero"   => $_POST["editarNumero"],

								"telefono" => $_POST["editarTelefono"]);



				$respuesta = UsuarioModelo::cambiarInfoPerfilrModelo($tabla, $item, $valor, $datos);



				if($respuesta == 'ok')

				{

					echo '<script>



						swal({

							  type: "success",

							  title: "¡Datos actualizados!",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "inicio";



										}

									})



						</script>';	

				}

				else

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "No pudimos modificar tus datos. Si es primera vez que recibes este error verifica los datos y intenta nuevamente, <a href="contacto">si el error persiste contacta con soporte</a>",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "inicio";



										}

									})



						</script>';	

				}

			}			

		}



		static public function mostrarInfoPerfil($item, $valor)

		{

			$tabla = "usuario";



			$respuesta = UsuarioModelo::mostrarInfoPerfilModelo($tabla, $item, $valor);



			return $respuesta;

		}





		static public function mostrarInfoPerfilCuenta($item, $valor)

		{

			$tabla = "cuenta_banco";



			$respuesta = UsuarioModelo::mostrarInfoPerfilModelo($tabla, $item, $valor);



			return $respuesta;

		}



		static public function cambiarSaldoJugar()

		{

			if(isset($_POST['nuevoMonto']))

			{

				$tabla= 'cuenta';

				$id_usuario =$_SESSION['user_id'];



				$respuesta = UsuarioModelo::cambiarSaldoJugarModelo($tabla, $id_usuario, $_POST['nuevoMonto']);

				

				if($respuesta== 'ok')

				{

					echo '<script>



						swal({

							  type: "success",

							  title: "Recarga exitosa",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "billetera";



										}

									})



						</script>';	

				}

				elseif($respuesta == 'sinfondos')

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "Fondos insuficiente realice una recarga",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "billetera";



										}

									})



						</script>';						

				}

				unset($_POST['nuevoMonto']);

			}

		}	



		static public function mostrarReferidos()

		{

			$tabla ="usuario";

			$item = "patrocinador";

			$valor = $_SESSION['user'];



			$respuesta = UsuarioModelo::mostrarReferidosModelo($tabla, $item, $valor);



			return $respuesta;

		}

	}



	class Juego

	{

	    private $called;

	    private $board;

	    private $daubed;

	    private $last;

	    private $cantar;	       



	    public function __construct(){

	        $range = range(1, 75);

	        $chunks = array_chunk($range, 15);

	        

	        $this->board = [];

	        

	        foreach($chunks AS $chunk){

	            shuffle($chunk);

	            $this->board = array_merge($this->board, array_slice($chunk, 0, 5));

	        }

	        

	        $this->board[12] = 'D';

	        

	        $this->last = time();

	        $this->called[] = 0;

	        $this->cantar=0;

	    }

	    

	    public function pintarCartilla($cartilla, $i, $sala){



	        $output = '<div class="col-lg-3 col-md-6" style="margin-top: 6%;">

                            <div class="bingo-board">

                               <div class="bingo-column">

                                  <div class="number letter"><img src="vistas/img/bingo-letter1.png" class="bingo-letter mb-2" alt=""></div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[0].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[1].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[2].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[3].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[4].'</div>

                               </div>

                               <div class="bingo-column">

                                  <div class="number letter"><img src="vistas/img/bingo-letter2.png" class="bingo-letter mt-2" alt=""></div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[5].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[6].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[7].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[8].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[9].'</div>

                               </div>

                               <div class="bingo-column">

                                  <div class="number letter"><img src="vistas/img/bingo-letter3.png" class="bingo-letter mb-2" alt=""></div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[10].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[11].'</div>

                                  <div class="number daubed"><img src="admin/vistas/img/parametros/favicon.png" width="40px"></div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[13].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[14].'</div>

                               </div>

                               <div class="bingo-column">

                                  <div class="number letter"><img src="vistas/img/bingo-letter4.png" class="bingo-letter mt-2" alt=""></div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[15].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[16].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[17].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[18].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[19].'</div>

                               </div>

                               <div class="bingo-column">

                                  <div class="number letter"><img src="vistas/img/bingo-letter5.png" class="bingo-letter mb-2" alt=""></div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[20].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[21].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[22].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[23].'</div>

                                  <div class="number" sala="'.$sala.'" cartilla="'.$i.'">'.$cartilla[24].'</div>

                               </div>

                               <div class="clearfix"></div>

                               <button type="button" class="cata strategy" usuario="'.openssl_encrypt($_SESSION["user_id"], COD, KEY).'" sala="'.$sala.'" style="margin-top: 7%; margin-left: 30%;" cartilla="'.$i.'" id="bingo">BINGO!!</button>

                            </div>

                        </div>';	



	        return $output;

	    }

	    

		static public function comprarCartillas()

		{

			if(isset($_POST['dataForm']))

			{

				$params = array();

        		parse_str($_POST["dataForm"], $params);



        		$tabla = "sala";

				$item  = "id";

				$valor = openssl_decrypt($params["idSala"], COD, KEY);

				$orden = null;



				$sala = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);



				$tabla ='cuenta';

				$id_usuario = openssl_decrypt($params["userId"], COD, KEY);



				$saldo = UsuarioModelo::mostrarBalanceModelo($tabla, $id_usuario);



				if($saldo[0]['saldo'] >= $params["compra"])

				{				

					date_default_timezone_set('America/Guayaquil');

					$date   = date('Y-m-d');		

	        		$tabla  = "tmp_apuesta";

	        		$item   = "id_usuario";

	        		$item2  = "id_sala";

 	        		$datos  = array("valor_usuario" => openssl_decrypt($params["userId"], COD, KEY),

	        						"valor_sala"    => openssl_decrypt($params["idSala"], COD, KEY));



	        		$contarCartilla = JuegoModelo::contarCartilllaModelo($tabla, $item, $item2, $datos);



        			if(isset($contarCartilla) && !empty($contarCartilla))

        			{

        				$numCartillas = openssl_decrypt($params["numCartilla"], COD, KEY) + $contarCartilla[0]["totalCartilla"];

        				$tabla = "apuesta";

        				$item  = "id_usuario";

        				$item2 = "id_juego";



	        			$datos = array ("id_sala"       => openssl_decrypt($params["idSala"], COD, KEY),

	        							"num_cartilla"  => $numCartillas,

	        							"valor_usuario" => openssl_decrypt($params["userId"], COD, KEY),

	        							"valor_juego"   => $contarCartilla[0]["id_juego"],

	        							"precio"        => $sala["precio_cartilla_sala"]);



	        			$respuesta = JuegoModelo::comprarCartillasModelo($tabla, $item , $item2, $datos);	        			

        			}

        			else

        			{	       				

        				$numCartillas = openssl_decrypt($params["numCartilla"], COD, KEY);

        				$tabla = "apuesta";

        				$item  = null;

        				$item2 = null;



	        			$datos = array ("id_sala"      => openssl_decrypt($params["idSala"], COD, KEY),

	        							"id_usuario"   => openssl_decrypt($params["userId"], COD, KEY),

	        							"num_cartilla" => $numCartillas,

	        							"valor_fecha"  => $date,

	        							"id_juego"     => $sala["id_juego_sala"],

	        							"precio"       => $sala["precio_cartilla_sala"]);



	        			$respuesta = JuegoModelo::comprarCartillasModelo($tabla, $item , $item2, $datos);

        			}

					

        			if($respuesta == "ok")

        			{	

						$tabla     = "cuenta";

						$item      = "saldo";    

						$itemCond  = "id_usuario";

						$valorCond = openssl_decrypt($params["userId"], COD, KEY);

						$_SESSION["sala"][openssl_decrypt($params["idSala"], COD, KEY)][$valorCond] = $numCartillas;

						$premio2   = null;						



						$datos = array(	"item_valor"             => $saldo[0]['saldo'] - $params["compra"],

										"id_usuario"             => $valorCond,

										"tipo_transaccion"       => 'compra',

										"monto_transaccion"      => $params["compra"],

										"referencia_transaccion" => $sala["id_juego_sala"],

										"estado_transaccion"     => 'completada');



						$saldoActualizar = UsuarioModelo::actualizarSaldoModelo($tabla, $item, $datos, $itemCond, $valorCond, $premio2);



						if($saldoActualizar == 'ok')

						{

							echo '<script>

									swal({

										  type: "success",

										  title: "Felicidades.",

										  text: "¡Su compra se realizo con exito!",

										  showConfirmButton: true,

										  confirmButtonText: "Cerrar"

										  }).then(function(result){

												if (result.value) {



												location.reload();



												}

											})

									</script>';

									echo json_encode($sala);

						}

						else

						{

							echo '<script>

									swal({

										  type: "Error",

										  title: "Lo siento.",

										  text: "¡Ocurrio un error desconocido, intente nuevamente!",

										  showConfirmButton: true,

										  confirmButtonText: "Cerrar"

										  })

									</script>';

						}						



					}

					else

					{

						echo '<script>



							swal({

								  type: "error",

								  title: "Lo sentimos",

								  text: "¡Su compra no se ha podido realizar!",

								  showConfirmButton: true,

								  confirmButtonText: "Cerrar"

								  })

							</script>';	



					}						

				}

				else

				{

					echo '<script>



						swal({

							  type: "error",

							  title: "Sin fondos",

							  text: "¡Su compra no se ha podido realizar, debido a que no posee saldo suficiente. Realice una recarga!",

							  showConfirmButton: true,

							  confirmButtonText: "Cerrar"

							  }).then(function(result){

										if (result.value) {



										window.location = "recarga";



										}

									})

						</script>';	

				}		

			}

 		}



 		static public function obtenerCartilla($num, $sala)

 		{

        	$range = range(1, 75);

            $chunks = array_chunk($range, 15);

            $cartilla= [];

            foreach($chunks AS $chunk){

                shuffle($chunk);

                $cartilla = array_merge($cartilla, array_slice($chunk, 0, 5));

            }

            $cartilla[12] = "D";

            $_SESSION["sala"][$sala]["cartillas"][$num] = $cartilla;

	        return $cartilla;	        		

 		}



 		static public function contarCartillaJugar($valor)

 		{

    		$tabla  = "tmp_apuesta";

    		$item   = "id_usuario";

    		$item2  = "id_sala";

        		$datos  = array("valor_usuario" => $_SESSION["user_id"],

    							"valor_sala"    => $valor);



    		$contarCartilla = JuegoModelo::contarCartilllaModelo($tabla, $item, $item2, $datos);



    		return $contarCartilla;



 		}



 		static public function datosSalas($id)

 		{



			$tabla = "sala";

			$item  = "id";

			$valor = $id;

			$orden = null;



			$sala = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);



    		$tabla  = "tmp_apuesta";

    		$item   = "id_usuario";

    		$item2  = "id_juego";



        	$datos  = array("valor_usuario" => $_SESSION["user_id"],

    						"valor_sala"    => $sala["id_juego_sala"]);



    		$contarCartilla = JuegoModelo::contarCartilllaModelo($tabla, $item, $item2, $datos);



    		return $contarCartilla[0]["totalCartilla"];



 		}



 		static public function contarCartillasSala()

 		{

 			if($_POST["id_juego"])

 			{

 				$tabla = "tmp_apuesta";

 				$item  = "id_juego";



 				$datos = array("item_valor" => $_POST["id_juego"]); 

 				

 				$respuesta = JuegoModelo::contarCartilllaSalaModelo($tabla, $item, $datos);



 				return $respuesta;



 			}

 		} 



 		static public function mostrarPremiosTipo($id)

 		{

 			$tabla = "tmp_apuesta";

			$item  = "id_juego";



			$datos = array("item_valor" => $id); 

			

			$respuesta = JuegoModelo::mostrarPremioTipoMOdelo($tabla, $item, $datos);



			return $respuesta;

 		}		



 		static public function bingoRegistrar()

 		{

 			if($_POST["idSala"])

 			{

				$tabla  = "tmp_apuesta";

        		$item   = "id_usuario";

        		$item2  = "id_sala";

	        	$datos  = array("valor_usuario" => $_SESSION["user_id"],

    							"valor_sala"    => $_POST["idSala"]);



        		$idJuegoAnterior = JuegoModelo::contarCartilllaModelo($tabla, $item, $item2, $datos);

        	

				$item   = "id_sala";

				$valor  = $_POST["idSala"];

				$orden  = null;



				$premio = ControladorSalas::ctrMostrarPremio($item, $valor, $orden);



				$tabla = "sala";

				$item  = "id";

				$orden = null;

				$valor  = $_POST["idSala"];



				$sala = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);    				 



				if($sala["premio_sala"] == 1)

				{

					$premio_valor = $premio["monto_premio"];

				}

				elseif($sala["premio_sala"] == 0)

				{

					$tabla = "tmp_apuesta";

					$item  = "id_juego";



					$datos = array("item_valor" => $sala["id_juego_sala"]); 						



					$contarCartillas = JuegoModelo::mostrarPremioTipoMOdelo($tabla, $item, $datos);

					$premio_valor = (75*($contarCartillas[0]["totalCartilla"]*$sala["precio_cartilla_sala"]))/100;

				}

				elseif($sala["premio_sala"] == 2)

				{

					$tabla = "premio";

					$item  = "id_sala";

					$orden = null;

					$valor = $_POST["idSala"];

					 						



					$premio = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);

					$premio_valor = $premio["descripcion_premio"];

				}



				$tabla = "ganador";

				$item  = 'nuevoJuego';

				$datos = array("id_sala"           => $_POST["idSala"],

            				   "id_usuario"        => $_SESSION["user_id"],

            				   "id_juego_anterior" => $sala["id_juego_sala"],

            				   "premio"            => $premio_valor);	



				$ganador = JuegoModelo::bingoRegistrarModelo($tabla, $item, $datos);  



				if($ganador == 'ok')

				{

					$tabla ='cuenta';

					$id_usuario = $_SESSION["user_id"];



					$saldo = UsuarioModelo::mostrarBalanceModelo($tabla, $id_usuario);



					$tabla     = "cuenta";

					$item      = "saldo";    

					$itemCond  = "id_usuario";

					$valorCond = $_SESSION["user_id"];



					if($sala["premio_sala"] == 2)

					{

						$p = $saldo[0]['saldo'] + 0;

						$estado = 'pendiente';

						$premio2 = 2;

					}

					else

					{

						$p =$saldo[0]['saldo'] + $premio_valor;

						$estado = 'completada';

						$premio2 = null;

					}

					



					$datos = array(	"item_valor"             => $p,

									"id_usuario"             => $valorCond,

									"tipo_transaccion"       => 'premio',

									"monto_transaccion"      => $premio_valor,

									"referencia_transaccion" => $sala["id_juego_sala"],

									"estado_transaccion"     => $estado);



					$saldoActualizar = UsuarioModelo::actualizarSaldoModelo($tabla, $item, $datos, $itemCond, $valorCond, $premio2);	



					if($saldoActualizar == 'ok')

					{

		 				$tabla = "tmp_juego";

						$item  = 'nuevoJuego';

						$id_juego = mt_rand(10000, 99999);

						$meses =  array ( 	"Jan" =>  1,

											"Feb" =>  2, 

											"Mar" =>  3,

											"Apr" =>  4,

											"May" =>  5,

											"Jun" =>  6,

											"Jul" =>  7,

											"Aug" =>  8,

											"Sep" =>  9,

											"Oct" =>  10,

											"Nov" =>  11,

											"Dec" =>  12);



						$h =explode(" ", $_POST["tiempo"]);

						

						$mes = $meses[$h[1]];

						$dia = $h[2];

						$año = $h[3];

			

						$hora = explode(":", $h[4]);

						$minutos = $hora[1]+6;

						if($hora[0] > 12)

						{

							$hora = $hora[0]-12;

							$paam = 'PM';

						}

						elseif($hora[0] < 12)

						{

							$hora = $hora[0];

							$paam = 'AM';

						}

						elseif($hora[0] == 12)

						{

							$hora = $hora[0];

							$paam = 'PM';

						}						



						if($minutos > 59)

						{

							$min = $minutos-59;

							$hora = $hora + 1;

							if($min <10)

							{

								$minutos = "0".$min;

							}

							else

							{

								$minutos = $min;

							}



							if($hora > 12)

							{

								$hora = $hora-12;

								$paam = 'PM';

							}

							if($hora == 12)

							{

								$hora = $hora;

								$paam = 'PM';

							}							

						}



						$called= [];

						$contador =0;



						for ($i=0; $i < 108 ; $i++) 

			        	{

			        		if(count($called) == 75)

			        		{}

			                else 

			                {

			                    $i = rand(1, 75);

			                    while(in_array($i, $called))

			                    {

			                        $i = rand(1, 75);

			                    }

			                    

			                    $called[] = $i;



			                    $datos = array("id_juego"    => $id_juego,

			                				   "numero"      => $i,

			                				   "id_sala"     => $_POST["idSala"],

			                			       "tiempo"      => $mes."/".$dia."/".$año." ".$hora.":".$minutos." ".$paam,

			                			   	   "estado_juego" => 'esperando');



			                    $cambiarIdJuego = JuegoModelo::cantar($tabla, $item, $datos);



			                    if($cambiarIdJuego == 'ok')

			                    {

			                    	$contador++;

			                    }

			                }   

			            }

		                

		                if($contador == count($called))

		                {

		                	$item  = 'cambiar';



							$cambiarJuego = JuegoModelo::cantar($tabla, $item, $datos);



							if($cambiarJuego == 'ok')

							{

								return "true";

							}

		                }         																			

					}

					else

					{

						return "false";

					}				

				}

				else

				{



				}         				   								

 			}

 		}



	    public function getBoard($cartilla, $sala){

	        return array_chunk($_SESSION["sala"][$sala]["cartillas"][$cartilla], 5);

	    }



	    public function getCalled(){

	        return $this->called;

	    }

	    

	    public function obtenerUltimasCinco(){

	        $called = array_slice(array_reverse($this->called), 0, 5);

	        return json_encode($called);

	    }

	    

	    public static function pre($array){

	        echo '<pre>' . print_r($array, true) . '</pre>';

	    }

	    

	    public function daub($number, $cartilla, $sala){

	        foreach($_SESSION["sala"][$sala]["cartillas"][$cartilla] AS $key => $num){

	            if($num == $number){

	                $_SESSION["sala"][$sala]["cartillas"][$cartilla][$key] = 'D';

	                return;

	            }

	        }

	    }

	    

	    public function adminitrarMuestra(){

	        $action = $_POST['action'];

	        $output = [];

	        

	        switch($action){

	            case 'daub':

	                if(empty($_POST['daub'])){

	                    $output['err'] = true;

	                    $output['msg'] = 'Ningun numero se ha enviado!';

	                }else {

	                    $daub = (int) $_POST['daub'];

	                    $cartilla = (int) $_POST["cartilla"];

	                    $sala     = (int) $_POST["idSala"];

	                    

	                    if($daub < 1 || $daub > 75){

	                        $output['err'] = true;

	                        $output['msg'] = 'Numero invalido!';

	                    }else {

	                        if(!in_array($daub, $this->called)){

	                            $output['err'] = true;

	                            $output['msg'] = 'Este numero no ha salido!';

	                        }else {

	                            $output['err'] = false;

	                            $output['msg'] = $daub;

	                            $this->daub($daub, $cartilla, $sala);

	                        }

	                    }

	                }

	                break;

	            case 'call':		            

			        	$tabla = "sala";

	    				$item  = "id";

	    				$valor = $_SESSION["sala_jugar"];

	    				$orden = null;



	    				$sala = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);



						$tabla = "tmp_jugando";

						$item  = 'consultaBingo';

						$datos = array("id_juego" => $_SESSION["id_juego"],

									   "id_sala"  => $sala["id"]);



			        	$consultaBingo = JuegoModelo::cantar($tabla, $item, $datos);



			        	if(isset($consultaBingo[0]) && $consultaBingo[0]["bingo"] != 'si')

	                	{	

							$tabla = "tmp_juego";

			            	$item  = "id_juego";



			            	$datos = array('item_valor' => $sala["id_juego_sala"] );



			            	$numeroJuego = JuegoModelo::cantarNumerosSalaModelo($tabla, $item, $datos);



			                if(isset($numeroJuego) && !empty($numeroJuego))

			                {	                

			                	if(count($this->called) == 76)

			                	{

			                		$output['err'] = true;

			                    	$output['msg'] = 'Todos los numero han sido llamados!';

			                	}

			                	else

			                	{

						        	$this->called[] = $numeroJuego[$this->cantar]["numero"];



						        	$output['msg1'] = $consultaBingo[0]["bingo"];

						        	$output['err'] = $this->called;

			                    	$output['msg'] = $numeroJuego[$this->cantar]["numero"];





			                    	$this->cantar = $this->cantar+1;	                		

			                	}

			                }		            	

		            	}

		            	else

		            	{		

		  		            $tabla = "usuario";

		            		$item  = "id";

		            		$valor = $consultaBingo[0]["id_usuario"];



		            		$mostrarGanador = JuegoModelo::mostrarGanadorBingoModelo($tabla, $item, $valor);



		            		$output['err'] = true;

			                $output['msg'] = $mostrarGanador[0]["nombre"]." ha cantado bingo";

		            	}			       

	                break;

	            case 'bingo':

	            	$cartilla = (int) $_POST["cartilla"];

	            	$sala     = (int) $_POST["idSala"];

	                $output['err'] = false;

	                $output['msg'] = $this->hasBingo($cartilla, $sala);

	                break;

	        }

	        

	        return json_encode($output);

	    }

	    

	    public function hasBingo($cartilla, $sala){

	        $board = $this->getBoard($cartilla, $sala);

	        

	        $bingos = [

	            'h' => 0,

	            'v' => 0,

	            'd' => 0,

	            'c' => 0

	        ];

	        

	        // check verticals

	        foreach($board AS $column){

	            if(implode($column) === 'DDDDD'){

	                $bingos['v']++;

	            }

	        }

	        

	        // check horizontals

	        for($i = 0; $i < 5; $i++){

	            $out = '';

	            for($j = 0; $j < 5; $j++){

	                $out .= $board[$j][$i];

	            }

	            

	            if($out === 'DDDDD'){

	                $bingos['h']++;

	            }

	        }

	        

	        $diags = [

	            $board[0][0] . $board[1][1] . $board[2][2] . $board[3][3] . $board[4][4],

	            $board[0][4] . $board[1][3] . $board[2][2] . $board[3][1] . $board[4][0]

	        ];

	        

	        foreach($diags AS $diag){

	            if($diag === 'DDDDD'){

	                $bingos['d']++;

	            }

	        }

	        

	        $corners = $board[0][0] . $board[4][0] . $board[0][4] . $board[4][4];



	        if($corners === 'DDDD'){

	            $bingos['c'] = 1;

	        }

	        

	        $bingos['total'] = array_sum($bingos);

	        $bingos['num'] = $board;

	        

	        return $bingos;

	    }

	}



	class Estilo

	{

 		static public function mostrarUltimosGanadores()

 		{

			$respuesta = EstiloModelo::mostrarUltimosGanadoresModelo();



			return $respuesta; 			

 		}



 		static public function mostrarGanadores()

 		{

			$respuesta = EstiloModelo::mostrarGanadoresModelo();



			return $respuesta; 			

 		}



 		static public function contarGanadoresSala($item)

 		{

			$respuesta = EstiloModelo::contarGanadoresSalaModelo($item);



			return $respuesta; 			

 		} 		



 		static public function mostrarSlider()

 		{

			$respuesta = EstiloModelo::mostrarSliderModelo();



			return $respuesta; 			

 		}



 		static public function mostrarParametros()

 		{

			$respuesta = EstiloModelo::mostrarParametrosModelo();



			return $respuesta; 			

 		}

	}



?>