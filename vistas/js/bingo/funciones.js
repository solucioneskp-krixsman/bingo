var numberInt;

$(document).ready(function(){
    numberInt = setInterval(getNextNumber, 5000);
    
    $('.number').on('click', function(){
        var $_ = $(this);
        var cartilla = $(this).attr("cartilla");
        var idSala = $(this).attr("sala");
        if($_.hasClass('daubed')){
            return;
        }else {
            var num = $_.text();
            $.ajax({
                url: 'public/bingo.php',
                type: 'POST',
                data: {
                    action: 'daub',
                    daub: num,
                    cartilla:cartilla,
                    idSala   : idSala
                },
                dataType: 'json',
                complete: function(e){
                    e = e.responseJSON;
                    if(e.err == true){
                        swal({
                            title: 'Informacion',
                            text: '¡'+e.msg+'!',
                            type: 'info',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            cancelButtonText: 'Cancelar',
                         }); 
                    }else {
                        $_.addClass('daubed');
                    }
                }
            });
            
            
        }
    });
    
    $('body').on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', '.number', function(){
        if($(this).hasClass('rollOut')){
            $('.called > .number').animate({
                left: '+60px'
            }, 500, 'swing', function(e){
                if($('.called > .number:animated').length === 0 && $('.called > .number').length > 5){
                    $('.called > .number').last().remove();
                    $('.called > .number').css('left', 0);
                }
            });
        }
    });
    
    $('button#bingo').on('click', function(){        
        var cartilla = $(this).attr("cartilla");
        var idSala   = $(this).attr("sala");
        var baseUrl  = $("#baseUrl").val();              
        $.ajax({
            url: 'public/bingo.php',
            type: 'POST',
            data: {
                action: 'bingo',
                cartilla : cartilla,
                idSala   : idSala
            },
            dataType: 'json',
            complete: function(e){
                e = e.responseJSON;
                var h = parseInt(e.msg.h);
                var v = parseInt(e.msg.v);
                var d = parseInt(e.msg.d);
                var c = parseInt(e.msg.c);
                var num = parseInt(e.msg.num);
                var total = parseInt(e.msg.total);
                
                if(total > 0){
                    //alert('You won! You got ' + total + ' bingos in ' + num + ' numbers called!');
                    //alert('Horizontals: ' + h + ', Verticals: ' + v + ', Diagonals: ' + d + ', Corners: ' + c);
                    $(this).attr("disabled","disabled");
                    var now = new Date();
                    $.ajax({
                        url: baseUrl+'public/tiempo.php',
                        type: 'POST',
                        data: {
                            action  : 'bingog',
                            idSala  : idSala,
                            tiempo: now
                        },
                        dataType: 'json',
                        complete: function(e){
                            var e = e.responseJSON;
                            
                            if(e.err == true){
                                swal({
                                      type: "success",
                                      title: 'Felicidades',
                                      text: '¡'+e.msg+'!',
                                      showConfirmButton: true,
                                      confirmButtonText: "Cerrar"
                                      }).then(function(result){
                                                if (result.value) {

                                                window.location = "sala";

                                                }
                                            })                              
                            }else {
                                swal({
                                      type: "error",
                                      title: 'Lo sentimos',
                                      text: '¡'+e.msg+'!',
                                      showConfirmButton: true,
                                      confirmButtonText: "Cerrar"
                                      }).then(function(result){
                                                if (result.value) {

                                                window.location = "sala";

                                                }
                                            })     
                            }
                        }
                    });
    
                    clearInterval(numberInt);
                }else {                    
                    swal({
                        title: 'Falta',
                        text: '¡Usted no tiene bingo!',
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                     }).then(function(result){
                            if (result.value) {

                                $(this).removeAttr("disabled","disabled");  

                            }
                        });                                    
                }
            }
        })
    });
    
    if(init != null){
        for(var i = init.length - 1; i >= 0; i--){
            addNewCalled(init[i]);
        }
    }
});

function addNewCalled(number){
    var letter;
    
    if(number < 1)
        number = 1;
        
    if(number > 75)
        number = 75;
    
    if(number < 16){
        letter = 'b';
    }else
    if(number < 31){
        letter = 'i';
    }else
    if(number < 46){
        letter = 'n';
    }else
    if(number < 61){
        letter = 'g';
    }else {
        letter = 'o';
    }
    
    if($('.called > .number').length >= 5){
        $('.called > .number').last().addClass('rollOut');
    }
    
    $('.called').prepend(
        $('<div></div>').addClass('number animated rollIn ' + letter).text(' '+number)
    );
}

function getNextNumber(){
    $.ajax({
        url: 'public/bingo.php',
        type: 'POST',
        data: {
            action: 'call',
        },
        dataType: 'json',
        complete: function(e){
            var e = e.responseJSON;
            
            if(e.err == true){
                swal({
                    title: 'Informacion',
                    text: '¡'+e.msg+'!',
                    type: 'info',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                 });
                clearInterval(numberInt);
            }else {
                var num = parseInt(e.msg);
                addNewCalled(num);
            }
        }
    });
}

$(window).scroll(function() 
{        
    var scroll = $(window).scrollTop(); 
       
    if (scroll >= 100) 
    {
        $(".called").css({"margin-top": "-8.5%", "z-index": "1000",  "margin-left": "27%", "background-color":"white","border-radius":"25%"});    
    }
    else if(scroll < 100)
    {
       $(".called").css({"margin-top": "0", "z-index": "1000",  "margin-left": "0", "background-color":"#ffffff00"});
    }
});