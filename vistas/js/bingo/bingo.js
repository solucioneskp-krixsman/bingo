$(document).ready(function() {

/*=============================================
SALAS
=============================================*/


	$(".sala").click(function(){
		var id = $(this).attr('id');
		var idSala = id.split("r");
		$("#sala"+idSala[1]).submit();
	});

/*=============================================
CARTILLA
=============================================*/


	$(".cartilla").click(function(){		
		var compra = $(this).attr('compra');
		var id = $(this).attr('id');
		var idCartilla = id.split("r");
		var dataForm  = $('#cartilla'+idCartilla[1]).serialize();
		var baseUrl  = $("#baseUrl").val();

		swal({
		 	title: '¿Está seguro de realizar esta compra?',
		 	html: '<div style="background-color: #4eae60;width: 70px;border-radius: 10px;padding: 6px 0 4px;margin-left: 42%;"><i class="fas fa-dollar-sign"></i> '+compra+'</div>',
		 	type: 'info',
		 	showCancelButton: true,
		 	confirmButtonColor: '#3085d6',
		 	cancelButtonColor: '#d33',
		 	cancelButtonText: 'Cancelar',
		 	confirmButtonText: 'Si, comprar!'
		 }).then(function(result){

		 	if(result.value){

	            $.ajax({
                    url: baseUrl+'public/comprar_cartilla.php',
                    data: {dataForm},
                    type: 'POST',
                    success: function (response) 
                    {
                    	$('#totalCartillas').html(response);
                    }
            	});

		 	}

		 })

	});	

/*=============================================
MOVER DATOS DE USUARIO 
=============================================*/	
	var ancho  = (screen.width);
	var sesion = $("#sesion").val();
	var sesions = $("#sesions").val();

	if (ancho <= 767 && sesion == 'si')
	{
		$("#user-panel-cs").css({"display" : "block"});
		$("#user-panel-ul-grand-cs").css({"display" : "none"});
	}
	if (ancho <= 767 && sesions == 'no')
	{
		$("nav ul li div#user-panel-ss").css({"display" : "block"});
		$("#user-panel-ul-grand-ss").css({"display" : "none"});
	}

});

/*=============================================
IMAGEN REFERENCIA
=============================================*/
$("#imagenModal").on("click", function(){
	var url = $(this).attr("url");
	$(".previsualizarReferencia").attr("src", url);
	
})