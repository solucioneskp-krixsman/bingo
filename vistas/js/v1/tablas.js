/*=============================================
CARTILLA
=============================================*/

$('.juego').DataTable( {
	"language": {

		"sProcessing":     "Procesando...",
		"sLengthMenu":     "",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "No hay cartillas para este pais",
		"sInfo":           "",
		"sInfoEmpty":      "",
		"sInfoFiltered":   "",
		"sInfoPostFix":    "",
		"sSearch":         "",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
			"sFirst":    "",
			"sLast":     "",
			"sNext":     "",
			"sPrevious": ""
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}

	}

});

$('#table_filter').css({'display': 'none'});
$('#table_paginate').css({'display': 'none'});
$('#table_length').css({'display': 'none'});
/*var ancho = screen.width;
if(ancho <= 766)
{
	$('.dataTables_info').css({'font-size': '20px'});
	$('#linkpatrocio').css({'margin-left': '0'});
	$('#cabeLog').css({'margin-left': '40px'});
	$('#rt').css({'margin-bottom': '3%'});
	$('#rt').removeClass('offset-2');
	$('#rb').css({'margin-bottom': '3%'});
	$('#rb').removeClass('offset-2');
	$('#cb').removeClass('offset-3');
	$('#lt').removeClass('text-right');
	$('#lt1').removeClass('text-right');
	$('#bt').css({'margin-left': '0%'});
	$('.carousel-caption').css({'top':'0'});
	$('.carousel-caption h5').css({'font-size': '30px'});
	$('.carousel-caption p').css({'font-size': '30px'});
	$('#texto1').css({'font-size': '30px'});
	$('#flecha').css({	'-moz-transform'   : 'rotate(90deg)',
     					'-o-transform'     : 'rotate(90deg)',
     					'-webkit-transform': 'rotate(90deg)',
     					'transform'        : 'rotate(90deg)',
    					'margin-top'       : '21%',
    					'margin-left'      : '-4%',
    					'text-align'       : 'center',
    					'margin-bottom'    : '25%'});
	$('div .carousel-item img').css({   'max-width': '100%',
                                        'height'   : 'auto'});    					
}
else
{
	$('#transfer label').css({'max-width': '18%'});
	$('.carousel-caption h5').css({'font-size': '60px'});
	$('.carousel-caption p').css({'font-size': '40px'});
}
if(ancho <= 594)
{
	$('#cabecera .container').css({'width':'90%'});
	$('#cabeLog').css({'margin-left': '0px'});
	$('#linkpatrocio').css({'margin-left': '-79px'});
	$('#flechaRecarga').css({'display': 'block'});
	$('#flechaRecargaG').css({'display': 'none'});
}
else
{
	$('#transfer label').css({'max-width': '18%'});
	$('.carousel-caption h5').css({'font-size': '60px'});
	$('.carousel-caption p').css({'font-size': '40px'});
}*/


$(document).ready(function(){
  var height = $('.juego').height();

  $('#banner').css('height', height);
  
});

/*$(document).ready(function(){
  $("button#x5").click(function() {
    var cantidad = $("#cantidad").val();  
    if(cantidad >= 5  || cantidad == 10 || cantidad == 20)
    {
      cantidad = 1;
    }
    var nuevacantidad = Number(cantidad) * 5;
    console.log(nuevacantidad);
    $("#cantidad").val(nuevacantidad);
  });
});

$(document).ready(function(){
  $("button#x10").click(function() {
    var cantidad = $("#cantidad").val();  
    if(cantidad >= 10 || cantidad == 5 || cantidad == 20 )
    {
      cantidad = 1;
    }
    var nuevacantidad = Number(cantidad) * 10;
    console.log(nuevacantidad);
    $("#cantidad").val(nuevacantidad);
  });
});

$(document).ready(function(){
  $("button#x20").click(function() {
    var cantidad = $("#cantidad").val();  
    if(cantidad >= 20 || cantidad == 5 || cantidad == 10)
    {
      cantidad = 1;
    }
    var nuevacantidad = Number(cantidad) * 20;
    console.log(nuevacantidad);
    $("#cantidad").val(nuevacantidad);
  });
});

$(document).ready(function(){
  $("#validar").click(function() {
    $('#check1').css({
      'display' : 'none'
    });
    $('#nuevo').css({
      'display' : 'none'
    });
    $('#check2').css({
      'display' : 'block'
    });
    $('#confirmar').css({
      'display' : 'block'
    });
  });
});*/

/*=============================================
REFERIDOS
=============================================*/

$('.referidos').DataTable( {
	"language": {

		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
		"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}

	}

});

/*=============================================
GANADORES
=============================================*/

$('.ganadores').DataTable( {
	"language": {

		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
		"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}

	}

});

/*=============================================
TRANSACCIONES
=============================================*/

$('.transacciones').DataTable( {
	"language": {

		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
		"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}

	}

});

/*=============================================
DISTRIBUIDOR
=============================================*/

    $('.distribuidor').DataTable( {
	"language": {

		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
		"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}

	}

}).fnDestroy();

/*=============================================
JUEGOS
=============================================*/

$(document).ready(function() {
	$(".img").click(function(){
		var id = $(this).attr('id');
		var idPais = id.split("r");
		console.log(idPais[1]);
		$("#juego"+idPais[1]).submit();
	});
});

/*=============================================
RECARGA
=============================================*/

function copyToClipboard(elemento) {
  var $temp = $("<input>")
  $("body").append($temp);
  $temp.val($(elemento).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
function copyToClipboard2(elemento) {
  var $temp = $("<input>")
  $("body").append($temp);
  $temp.val($(elemento).text()).select();
  document.execCommand("copy");
  $temp.remove();
}

/*=============================================
REFRESCAR
=============================================*/
	
$(document).ready(function(){
	var activo = $('#activo').val();
	var enlace = $('#enlace').val();
	if(activo == 'billetera' || activo == 'recarga')
	{
		setTimeout('document.location.reload()',300000);
	}
	
});	

/*=============================================
CABECERA
=============================================*/

function copylink(elemento) {
  var $temp = $("<input>")
  $("body").append($temp);
  $temp.val($(elemento).text()).select();
  document.execCommand("copy");
  $temp.remove();
  swal({
	  type: "success",
	  title: "Link copiado.",
	  showConfirmButton: true,
	  confirmButtonText: "Cerrar"
	  }).then(function(result){
				if (result.value) {

				window.location = "billetera";

				}
			})
}

/*=============================================
EDITAR USUARIO
=============================================*/
$("#botonaccion").on("click", ".btnEditarUsuario", function(){

	var idUsuario = $(this).attr("idUsuario");

	var datos = new FormData();
	datos.append("idUsuario", idUsuario);

	$.ajax({
		url: "ajax/ajax.php",
		method: "POST",
      	data: datos,
      	cache: false,
     	contentType: false,
     	processData: false,
     	dataType:"json",
     	success: function(respuesta){
        console.log(respuesta);
     		$("#editarBilletera").val(respuesta[1]["bitcoin"]);
     		$("#editarCorreo").val(respuesta[0]["correo"]);
		

     	}

	})


})