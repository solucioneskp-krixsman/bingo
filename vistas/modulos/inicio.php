<?php
	$top10 = Estilo::mostrarUltimosGanadores();	
?>

<img src="vistas\img\background\home-bg-img.jpeg" alt="" class="bg-img">
<section class="home-section">
	<div class="container">
		<div class="row">
			<div class="col-12 mt-cspdn mb-5 mb-lg-3">
				<div class="mt-cspdn">
					<div class="card m-auto weeklyN1 p-4">
						<img src="vistas/img/bingo-stars-rainbow.png" alt="" class="bsr_img">
						<div class="row">
							<div class="col-md-6 text-center pmt-hsss">
								<p class="weetex text-orange"><?php echo !isset($top10[0]["usuario"]) ? 'Disponible' : ucwords($top10[0]["usuario"])?></p>
								<p class="weetex text-white">ganancia:</p>
								<p class="weewor text-white"><span class="mr-2 wos-15">$</span> <?php echo !isset($top10[0]["premio"]) ? '5000' : $top10[0]["premio"]?> USD</p>
							</div>
							<div class="col-md-6 text-center">
								<p class="weetex text-orange">puesto n1 semanal</p>
								<img src="<?php echo !isset($top10[0]["imagen_perfil"]) ? 'vistas/img/perfil/default.jpg' : $top10[0]["imagen_perfil"]?>" alt="" class="place1-profile-photo">
							</div>
						</div>	
						<img src="vistas\img\winner-cup.png" alt="" class="wc-img">
					</div>
				</div>
			</div>
			<div class="col-sm-6 d-sm-block d-flex justify-content-center mb-3">
				<a href="sala">
					<div class="position-relative ccc-img">
						<p>¡ven a ganar!</p>
					</div>
				</a>
			</div>
			<div class="col-sm-6 d-sm-block d-flex justify-content-center mb-3">
				<div class="card top10winners">
					<div class="card-header">
						<h5 style="text-align: center;">TOP 10 DE <br> GANADORES</h5>
					</div>
					<div class="card-body">
						<ul>
							<?php
								foreach ($top10 as $key => $value) 
								{
									echo '<li>
												<div class="mr-2 wos-15">'.($key+1).'</div>
												'.ucwords($value["usuario"]).'
											</li>';
								}

								for ($i=0; $i < (10-count($top10)); $i++) 
								{ 
									if($i == 0)
									{
										$key++;
									}

									echo '<li>
											<div class="mr-2 wos-15">'.($key+1).'</div>
											disponible
										</li>';

									$key++;
								}
							?>														
						</ul>
					</div>
					<div class="tw10Border"></div>
				</div>
			</div>
			<div class="col-12 d-md-none d-block" style="height: 94.59px!important;"></div>
		</div>
	</div>
</section>