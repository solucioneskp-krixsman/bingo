<?php 
	$validarSesion = Vista::redirigir();

	$parametros	= Estilo::mostrarParametros();

	$item  = "id";
	$valor = $_SESSION["user_id"];

	$datosPerfil = Usuario::mostrarInfoPerfil($item, $valor);

	$item  = "id_usuario";
	$datosCuenta = Usuario::mostrarInfoPerfilCuenta($item, $valor);

	if($datosPerfil['imagen_perfil'] != 'vistas/img/perfil/default.jpg')
	{
		$imagen = $datosPerfil['imagen_perfil'];
	}
	else
	{
		$imagen = 'vistas/img/perfil/default.jpg';
	}	
?>
<!-- Perfil -->
<section class="tournaments-section spad">
	<img src="vistas\img\background\room-bg-img.jpeg" alt="" class="bg-img">
	<div class="container">
		<div class="section-title">
			<div class="cata new Bpre">Retire el saldo disponible</div>
        </div>	
        <?php 
        	if(isset($datosCuenta["titular_cuenta"]) && !empty($datosCuenta["titular_cuenta"]))
        	{
        		echo '<div class="row">					
						<div class="col-md-6">
							<div class="card greenc mb-5">
								<div class="card-header">cuenta bancaria</div>
								<div class="card-body">
									<form action="#" method="post">					
										<div class="ti-content">																
											<div class="ti-text" style="padding-left: 5px;">
												<h4>Titular: '.ucwords($datosCuenta["titular_cuenta"]).'</h4>
												<h4>Documento: '.$datosCuenta["cedula"].'</h4>
												<ul>
													<li class="mb-2"><span>Banco:</span> '.ucwords($datosCuenta["banco"]).'</li>
													<li class="mb-2"><span>Tipo de cuenta:</span> '.ucwords($datosCuenta["tipo_cuenta"]).'</li>
													<li class="mb-2"><span>Numerodecuenta:</span>'; 

														if(strlen($datosCuenta["numero_cuenta"]) > 20)
														{
															echo substr($datosCuenta["numero_cuenta"], 0,4).'-'.substr($datosCuenta["numero_cuenta"], 4,4).'-'.substr($datosCuenta["numero_cuenta"], 8,4).'-'.substr($datosCuenta["numero_cuenta"], 12,4).'-'.substr($datosCuenta["numero_cuenta"], 16,4).'-'.substr($datosCuenta["numero_cuenta"], 20,4);
														}
														else
														{
															echo substr($datosCuenta["numero_cuenta"], 0,4).'-'.substr($datosCuenta["numero_cuenta"], 4,4).'-'.substr($datosCuenta["numero_cuenta"], 8,4).'-'.substr($datosCuenta["numero_cuenta"], 12,4).'-'.substr($datosCuenta["numero_cuenta"], 16,4);
														} 

											echo	'</li>
												</ul>
												<div class="d-inline-block p-2 my-2 text-dark" style="background-color: #ADD2D8;"><a href="perfil"><i class="fas fa-file-export"></i> Retirar a otra cuenta</a></div>								
											</div>
											<div class="ti-text" style="padding-left: 5px;">								
												<ul>
													<li><span>Monto a retirar: $</span> <input onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" required="" class="form-control" type="text" name="montoRetirar" placeholder="Ejemplo: 100 (Mínimo $ '.$parametros["minimo_retiro"].' - Máximo $ '.$parametros["maximo_retiro"].')"></li>	
													<div class="d-inline-block p-2 my-2 text-dark" style="background-color: #ADD2D8;"><i class="fa fa-info-circle"></i> Limite diario: $ '.$parametros["maximo_retiro"].'</div>
												</ul>
												<div class="Brec">
													<button type="submit" class="btn">Retirar Fondos</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card redt w-100">
								<div class="card-header">
									información:
								</div>
								<div class="card-body">
									<ul>
										<li><span>Los retiros de saldo se hacen efectivo en tu cuenta bancaria de 1 a 3 días hábiles bancarios desde la fecha de su solicitud.</span> </li>									
										<li><span>En caso de tener algún inconveniente con su Retiro puedes comunicarte con nosotros vía Whatsapp al '.$parametros["telefono_parametros"].' y en menos de 48 horas hábiles atenderemos tu requerimiento.</span> </li>
									</ul>
								</div>
							</div>
						</div>								
					</div>';
        	}
        	else
        	{
        		echo '<div class="row">											
						<div class="col-md-6">
							<div class="card redt w-100">
								<div class="card-header">
									información:
								</div>
								<div class="card-body">
									<ul>
										<li><span>Debe poseer una cuenta bancaria registrada para poder realizar retiros</span> </li>
										<li class="Tmar"><div class="d-inline-block p-2 my-2 text-dark" style="background-color: #ADD2D8;"><a href="perfil"><i class="fas fa-file-export"></i> Registrar cuenta</a></div></li>
										<li><span>En caso de tener algún inconveniente con su Retiro puedes comunicarte con nosotros vía Whatsapp al '.$parametros["telefono_parametros"].' y en menos de 48 horas hábiles atenderemos tu requerimiento.</span> </li>
									</ul>
								</div>
							</div>
						</div>							
					</div>';
        	}
        ?>
		
	</div>
</section>
<!-- Perfil bg -->
<?php 
	$recargar = new Usuario();
	$recargar ->retirarUsuario();
?>