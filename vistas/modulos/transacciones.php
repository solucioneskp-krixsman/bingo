<?php $validarSesion = Vista::redirigir(); ?>
<!-- Page section -->
<section class="page-section spad contact-page transactions">
	<img src="vistas\img\background\room-bg-img.jpeg" alt="" class="bg-img">
	<div class="container">
		<div class="section-title">
			<div class="cata new Bpre">Historial de Transacciones</div>
			<!--<h2 class="Trec">Historial de Transacciones</h2>-->
        </div>		
		<table class="table table-bordered table-striped bg-light dt-responsive transacciones text-dark">
		  <thead>                 
		    <tr>
	           <th style="width:10px">#</th>
	           <th>Tipo</th>
	           <th>Transaccion</th>           		           
	           <th>Monto o Premio</th>
	           <th>Referencia</th>
	           <th>Estado</th>
	           <th>Fecha</th>
		    </tr> 
		  </thead>
		  <tbody>
		    <?php 
	          $item = "id_usuario";
	          $valor = $_SESSION["user_id"];

	          $transacciones = ControladorClientes::ctrMostrarTransacciones($item, $valor);

	          foreach ($transacciones as $key => $value) 
	          {
	            if($value["tipo_transaccion"] == 'premio')
	            {
	            	$cata = 'new';
	            }
	            elseif($value["tipo_transaccion"] == 'compra')
	            {
	            	$cata = 'racing';
	            }
	            elseif($value["tipo_transaccion"] == 'recarga')
	            {
	            	$cata = 'adventure';
	            }
	            else
	            {
	            	$cata = 'strategy';
	            }

	            if($value["estado_transaccion"] == 'aprobada')
	            {
	            	$estado = '<div style="border-radius: 20%;" class="cata strategy">'.ucwords($value["estado_transaccion"]).'</div>';
	            	$modal    = '';
	            }
	            elseif($value["estado_transaccion"] == 'aprobado')
	            {
	            	$estado = '<img src="'.$value["imagen"].'" width="150px">';
	            	$modal    = 'data-toggle="modal" data-target="#modalImagen" id="imagenModal" url="'.$value["imagen"].'"';
	            }
	            else
	            {
	            	$estado = ucwords($value["estado_transaccion"]);
	            	$modal    = '';
	            }

	            if($value["monto_transaccion"] == 0 && !empty($value["premio_transaccion"]))
	            {
	            	$monto = ucwords($value["premio_transaccion"]);
	            }
	            else
	            {
	            	$monto = $value["monto_transaccion"];
	            }

	            echo '<tr>

	                    <td>'.($key+1).'</td>

	                    <td><div style="border-radius: 20%;" class="cata '.$cata.'">'.ucwords($value["tipo_transaccion"]).'</div></td>

	                    <td>'.$value["id_transaccion"].'</td>                    

	                    <td>'.$monto.'</td>

	                    <td>'.$value["referencia_transaccion"].'</td>            

	                    <td '.$modal.'>'.$estado.'</td>

	                    <td>'.$value["fecha_transaccion"].'</td>

	                  </tr>';
	          
	            }
		    ?>
		  </tbody>
		</table>		
	</div>
</section>
<!-- Page section end -->
<?php
    $enviarCorreo = new Usuario;
    $enviarCorreo->contacto();
?>