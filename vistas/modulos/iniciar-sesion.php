<section class="inires">
    <img src="vistas\img\background\room-bg-img.jpeg" alt="" class="bg-img">
    <div class="irbox">
        <form action="#" method="post">
            <div class="wrap-login100">                  
                <span class="login100-form-logo">
                    <i class="zmdi zmdi-landscape"></i>
                </span>

                <span class="login100-form-title p-b-34 p-t-27">
                    Inicia Sesión
                </span>

                <div class="wrap-input100 validate-input" data-validate = "Ingrese Usuario">
                    <input class="input100" type="text" name="usuario" placeholder="Usuario">
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Ingrese Contraseña">
                    <input class="input100" type="password" name="contrasena" placeholder="Contraseña">
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">
                    iniciar
                    </button>
                </div>

                <div class="text-center p-t-90">
                    <a class="txt1" href="recuperaContrasena">
                    Recuperar Contraseña
                    </a>
                </div>
            </div>
        </form>
    </div>
</section>
<?php
    $iniciausuario= new Usuario();
    $iniciausuario-> iniciaSesion();
?>  