<?php 
	$baseUrl = Vista::baseUrl();

	$parametros	= Estilo::mostrarParametros();
 ?>
<!-- Footer section -->
<footer class="footer-section">
	<div class="container">
			    <?php 
		if(!isset($_SESSION["user"]) && empty($_SESSION["user"]))
		{
			echo '	<ul class="footer-menu copyright" style="float: right;">
						<li><a href="inicio"><span class="copyright">Inicio</span></a></li>							
						<li><a href="contacto"><span class="copyright">Contacto</span></a></li>
						<li><a style="color: #4eae60;" data-toggle="modal" data-target="#modalGanar"><span class="copyright">¿Como ganar?</span></a></li>							
					</ul>';
		} 
		else
		{
			$saldoActual = new Usuario();
            $saldo=$saldoActual->mostrarBalance();
            $item  = "id";
			$valor = $_SESSION["user_id"];

			$datosPerfil = Usuario::mostrarInfoPerfil($item, $valor);

            echo '  <ul class="footer-menu copyright" style="float: right;">
						<li><a href="inicio"><span class="copyright">Inicio</span></a></li>
						<li><a href="sala"><span class="copyright">Salas</span></a></li>
						<li><a href="transacciones"><span class="copyright">Transacciones</span></a></li>
						<li><a href="recargas"><span class="copyright">Recargas</span></a></li>
						<li><a href="contacto"><span class="copyright">Contacto</span></a></li>
						<li><a href="salir"><span class="copyright">Salir</span></a></li>
						<li><a style="color: #4eae60;" data-toggle="modal" data-target="#modalGanar"><span class="copyright">¿Como ganar?</span></a></li>							
					</ul>';
		} 
    ?>	
	<p class="copyright">
		<a href="<?php echo $baseUrl ?>" target="_blank"><span class="copyright">Copyright &copy; <?php echo $parametros["nombre"]." ".date("Y"); ?></a>			
	</p>	
	</div>
</footer>
<!-- Footer section end -->