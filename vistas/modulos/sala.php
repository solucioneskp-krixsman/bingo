<?php 
	$validarSesion = Vista::redirigir();
	$baseUrl = Vista::baseUrl();

	$item = null;
	$valor = null;
	$orden = "id";

	$salas = ControladorSalas::ctrMostrarSalas($item, $valor, $orden);

	$premio = ControladorSalas::ctrMostrarPremio($item, $valor, $orden);
	

?>
<!-- Sala section -->
<section class="review-section spad">
<img src="vistas\img\background\room-bg-img.jpeg" alt="" class="bg-img">
	<div class="container">
		<!--<div class="section-title">
			<div class="cata new">Salas</div>
			<h2 style="color: #4eae60; ">Disponibles</h2>
        </div>-->
        <div class="row">
			<?php 
				foreach ($salas as $key => $value) 
				{
					if($value["estado_sala"] == 'activa' && $value["premio_sala"] == 2)
					{
						$fecha = explode(" ", $premio[$key]['fecha_premio']);
						$fecha = explode("/", $fecha[0]);
						$fecha = $fecha[1]."-".$fecha[0]."-".$fecha[2];
						echo '	<div class="col-lg-4 col-md-6"></div>
								<div class="col-lg-4 col-md-6">
									<form style="height: 90%;" id="sala'.$value["id"].'" method="post" action="jugar">
										<input type="hidden" id="idSala" name="idSala" value="'.openssl_encrypt($value["id"], COD, KEY).'">	
										<div class="recent-game-item sala" id="enviar'.$value["id"].'">
											<div class="rgi-thumb set-bg" id="enviar'.$value["id"].'" data-setbg="admin/'.$premio[$key]["imagen_premio"].'">
												<div class="cata adventure"><strong style="color: black;">Premio:</strong> '.$premio[$key]["descripcion_premio"].'</div>
											</div>
											<div class="rgi-content">
												<h4>'.ucwords($value["nombre_sala"]).'</h4>
												<br>											
												<p class="cata racing" style="color: black;">'.$fecha.'</p>
												<br>
												<h5 class="cata strategy" style="color: black; font-size: 20px;" id="count'.$value["id"].'"></h5>
												<h5 class="cata strategy" style="color: black; display: none; font-size: 20px;" id="countoff'.$value["id"].'">Jugando</h5>
												<h5 class="cata strategy" style="color: black; display: none; font-size: 20px;" id="countoffmensaje'.$value["id"].'">Lamentablente no hubieron jugadores suficientes para esta sala</h5>
											</div>
										</div>
									</form>	
								</div>
								<div class="col-lg-4 col-md-6"></div>';
						$time = $premio[$key]["fecha_premio"];
						echo '	<script>
									var end'.$value["id"].' = new Date("'.$time.'");
								    var _second'.$value["id"].' = 1000;
								    var _minute'.$value["id"].' = _second'.$value["id"].' * 60;
								    var _hour'.$value["id"].' = _minute'.$value["id"].' * 60;
								    var _day'.$value["id"].' = _hour'.$value["id"].' * 24;
								    var timer'.$value["id"].';

								    function showRemaining'.$value["id"].'() {
								        var now'.$value["id"].' = new Date();
								        var distance'.$value["id"].' = end'.$value["id"].'- now'.$value["id"].';
								        if (distance'.$value["id"].' < 0) {

								            clearInterval(timer'.$value["id"].');
											$.ajax({
							                    url: "'.$baseUrl.'public/tiempo.php",
							                    data: {action: "consulta",
							                    	   id_juego: '.$value["id_juego_sala"].'},
							                    	   type: "POST",
							                    success: function (response) 
							                    {
							                    	let cartilla = JSON.parse(response);
							        				if(cartilla[0]["total"] == 0)
							        				{
							        					$("#count'.$value["id"].'").css({"display" : "none"});
							        					$("#countoffmensaje'.$value["id"].'").css({"display" : "block"});
							        				}
							        				else
							        				{
							        					$("#count'.$value["id"].'").css({"display" : "none"});
														$("#countoff'.$value["id"].'").css({"display" : "block"});
							        				}
							                    }
							            	});
								        }
								        var days'.$value["id"].' = Math.floor(distance'.$value["id"].' / _day'.$value["id"].');
								        var hours'.$value["id"].' = Math.floor((distance'.$value["id"].' % _day'.$value["id"].') / _hour'.$value["id"].');
								        var minutes'.$value["id"].' = Math.floor((distance'.$value["id"].' % _hour'.$value["id"].') / _minute'.$value["id"].');
								        var seconds'.$value["id"].' = Math.floor((distance'.$value["id"].' % _minute'.$value["id"].') / _second'.$value["id"].');

								        if(days'.$value["id"].' != 0)
								        {
								        	document.getElementById("count'.$value["id"].'").innerHTML = " Faltan, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += days'.$value["id"].' + " dias, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += hours'.$value["id"].' + " Hrs, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += minutes'.$value["id"].' + " Min, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += seconds'.$value["id"].' + " Seg";
								        }
								        else if(days'.$value["id"].' == 0 && hours'.$value["id"].' != 0)
								        {
								        	document.getElementById("count'.$value["id"].'").innerHTML = " Faltan, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += hours'.$value["id"].' + " Hrs, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += minutes'.$value["id"].' + " Min, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += seconds'.$value["id"].' + " Seg";
								        }
								        else if(days'.$value["id"].' == 0 && hours'.$value["id"].' == 0 && minutes'.$value["id"].' != 0)
								        {
								        	document.getElementById("count'.$value["id"].'").innerHTML = " Faltan, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += minutes'.$value["id"].' + " Min, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += seconds'.$value["id"].' + " Seg";
								        }
								        else if(days'.$value["id"].' == 0 && hours'.$value["id"].' == 0 && minutes'.$value["id"].' == 0 && seconds'.$value["id"].' != 0)
								        {
								        	document.getElementById("count'.$value["id"].'").innerHTML = " Faltan, ";
									        document.getElementById("count'.$value["id"].'").innerHTML += seconds'.$value["id"].' + " Seg";
								        }								        

								    }

								    timer'.$value["id"].' = setInterval(showRemaining'.$value["id"].', 1000);
								</script>';
					}
				}
echo '	</div>
		<div class="row">';
				$contarSala = 0;
				foreach ($salas as $key => $value) 
				{	
					if($value["juego_sala"] == 'esperando')
					{
						$time = $value["tiempo_juego_sala"];
					}
					else
					{
						$time = 0;
					}

					if($value["premio_sala"] == 1)
					{
						$premio_valor = $premio[$key]["monto_premio"];
					}
					elseif($value["premio_sala"] == 0)
					{
						$contarCartillas = Juego::mostrarPremiosTipo($value["id_juego_sala"]);
						$premio_valor = (75*($contarCartillas[0]["totalCartilla"]*$value["precio_cartilla_sala"]))/100;
					}


					
					if($value["estado_sala"] == 'activa' && $value["premio_sala"] != 2)
					{
						if($key == 0)
						{
							echo '<div class="col-lg-2 col-12"></div>';
						}

						if($contarSala == 2)
						{
							echo '<div class="col-lg-2 col-12"></div>';
							echo '<div class="col-lg-2 col-12"></div>';
						}

						echo '	<div class="col-lg-4 col-md-6 toCenter d-sm-block" style="margin-top: 5%;">
									<form style="height: 90%;" id="sala'.$value["id"].'" method="post" action="jugar">
										<input type="hidden" id="idSala" name="idSala" value="'.openssl_encrypt($value["id"], COD, KEY).'">
										<div class="review-item sala card" style="background-color: rgb(6 70 80 / 70%); border: none; border-top-left-radius: 35px; border-top-right-radius: 35px; cursor: pointer;" id="enviar'.$value["id"].'">
											<div class="review-cover set-bg sala" id="enviar'.$value["id"].'" style="border-bottom: 1px solid #860d0d; border-radius: 15px;" data-setbg="admin/'.$value["imagen_sala"].'">
												<img src="vistas\img\bingo-stars-rainbow.png" alt="" class="veleda">
												<div class="score bolbin toCenter">
													<div id="countdown'.$value["id"].'" class="numtim toCenter"></div>
												</div>
												<div id="countdownoff'.$value["id"].'" class="cata racing" style="background-color: #2E2963 color: white; display: none;">Jugando</div>
											</div>
											<div class="review-text">
												<h5 class="Bnom">'.ucwords($value["nombre_sala"]).' <span class="Buni">Unirse</span></h5>
												<div class="cata strategy Bpre">Premio</div>
												<p class="Bcan">'.$premio_valor.' <span class="Bsim">$</span></p>
											</div>
										</div>
									</form>
								</div>';

						echo Vista::salaScript($value["id"], $time, $value["id_juego_sala"], $baseUrl);

						$contarSala++;

						if($contarSala > 2)
						{
							$contarSala = 1;
						} 

					}
				}
			?>
		</div>
	</div>
</section>
<!-- Sala section end -->

 