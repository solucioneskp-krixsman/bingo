<?php $parametros = Estilo::mostrarParametros(); ?>
<!-- Header section -->
<header class="header-section">
	<div class="container">
		<!-- logo -->
		<a class="site-logo" href="inicio">
			<img src="admin/<?php echo $parametros["logo"] ?>" alt="">
		</a>			
	    <?php 
			if(!isset($_SESSION["user"]) && empty($_SESSION["user"]))
			{
				echo '	<input type="hidden" id="sesions" value="no">
						<div id="user-panel-ul-grand-ss" class="user-panel">
							<!--a data-toggle="modal" data-target="#modalIniciaSesion" class="iniciar">Iniciar</a> <a data-toggle="modal" data-target="#modalRegistro" class="registrar">Registrar</a-->
							<a href="iniciar-sesion" class="iniciar">Iniciar</a> <a href="registrarse" class="registrar">Registrar</a>
						</div>	
						<!-- responsive -->
						<div class="nav-switch">
							<i class="fa fa-bars"></i>
						</div>
						<!-- site menu -->
						<nav class="main-menu">
							<ul>
								<li><a href="inicio">Inicio</a></li>
								<li><a href="contacto">Contactanos</a></li>
								<li>
									<div id="user-panel-ss" class="user-panel-min">
										<!--a data-toggle="modal" data-target="#modalIniciaSesion" class="iniciar">Iniciar</a> <a data-toggle="modal" data-target="#modalRegistro" class="registrar">Registrar</a-->
										<a href="iniciar-sesion" class="iniciar">Iniciar</a> <a href="registrarse" class="registrar">Registrar</a>
									</div>
								</li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>						
							</ul>
						</nav>';
			} 
			else
			{
				$saldoActual = new Usuario();
                $saldo=$saldoActual->mostrarBalance();
                $item  = "id";
				$valor = $_SESSION["user_id"];

				$datosPerfil = Usuario::mostrarInfoPerfil($item, $valor);

                echo '  <input type="hidden" id="sesion" value="si">    
						<div class="user-panel" style="padding: 5px 20px;">
							<div id="user-panel-ul-grand-cs" class="dropdown d-flex align-items-center user-panel-ul">
								<span class="profile-pic">
									<div class="avatar-sm">
										<img src="'.$datosPerfil["imagen_perfil"].'" alt="..." width="35" class="avatar-img rounded-circle">
									</div>
								</span>
								<a class="btn dropdown-toggle" style="color: #000;box-shadow:none" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="mx-2">
										'.ucwords($datosPerfil["usuario"]).'
									</span>
									</a>

								<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<a class="dropdown-item" href="recargas">
										<i class="fas fa-dollar-sign"></i>
										Recargar
									</a>
									<a class="dropdown-item" href="retirar">
										<i class="fas fa-hand-holding-usd"></i>
										Retirar
									</a>
									<a class="dropdown-item" href="perfil">
									<i class="fas fa-user"></i>
										Perfil
									</a>
								</div>
							</div>
							<!--<ul style="flex-direction: row;" class="navbar-nav topbar-nav ml-md-auto align-items-center">
								<div class="user-panel-money" style="background-color: #4eae60;">
									<li class="nav-item dropdown hidden-caret">
										<a class="nav-link" href="recargas" id="messageDropdown" role="button" aria-haspopup="true" aria-expanded="false">
											<i class="fas fa-dollar-sign"></i>
											<span class="notification">'.$saldo[0]['saldo'].'</span>
										</a>
									</li>
								</div>
								<div class="user-panel-moneda">
									<li class="nav-item dropdown hidden-caret">
										<a class="nav-link" href="retirar" id="notifDropdown" role="button" aria-haspopup="true" aria-expanded="false">
											<i class="fas fa-hand-holding-usd"></i>
											<span class="notification">Retirar</span>
										</a>
									</li>
								</div>
								<div class="user-panel-perfil">
									<li class="nav-item dropdown hidden-caret">
										<a class="profile-pic" href="perfil" aria-expanded="false">
											<div class="avatar-sm">
												<img src="'.$datosPerfil["imagen_perfil"].'" alt="..." width="50" class="avatar-img rounded-circle">		
												<p style="font-size: 13px;position: absolute;color: white;top: 88%;">'.ucwords($datosPerfil["usuario"]).'</p>
											</div>
										</a>
									</li>
								</div>
							</ul>-->
						</div>
						<!-- responsive -->
						<div class="nav-switch">
							<i class="fa fa-bars"></i>
						</div>
						<!-- site menu -->
						<nav class="main-menu">
							<ul>
								<li><a href="inicio">Inicio</a></li>
								<li><a href="sala">Salas</a></li>
								<li><a href="transacciones">Transacciones</a></li>
								<li><a href="recargas">Recargas</a></li>
								<li><a href="contacto">Contacto</a></li>
								<li><a href="salir">Salir</a></li>
								<li>
									<div id="user-panel-cs" style="display: none;" class="user-panel-ul" style="background-color: #2e2963">
										<ul style="flex-direction: row;" class="navbar-nav topbar-nav ml-md-auto align-items-center">
											<div class="user-panel-money" style="background-color: #4eae60;">
												<li class="nav-item dropdown hidden-caret">
													<a class="nav-link" href="recargas" id="messageDropdown" role="button" aria-haspopup="true" aria-expanded="false">
														<i class="fas fa-dollar-sign"></i>
														<span class="notification">'.$saldo[0]['saldo'].' </span>
													</a>
												</li>
											</div>
											<div class="user-panel-moneda">
												<li class="nav-item dropdown hidden-caret">
													<a class="nav-link" href="retirar" id="notifDropdown" role="button" aria-haspopup="true" aria-expanded="false">
														<i class="fas fa-hand-holding-usd"></i>
														<span class="notification">Retirar</span>
													</a>
												</li>
											</div>
											<div class="user-panel-perfil">
												<li class="nav-item dropdown hidden-caret">
													<a class="profile-pic" href="perfil" aria-expanded="false">
														<div class="avatar-sm">
															<img src="'.$datosPerfil["imagen_perfil"].'" alt="..." width="50" class="avatar-img rounded-circle">
														</div>
													</a>
												</li>
											</div>
										</ul>
									</div>
								</li>								
							</ul>
						</nav>';
			} 
	    ?>			
	</div>
</header>
<!-- Header section end -->