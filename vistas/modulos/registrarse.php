<section class="inires">
    <img src="vistas\img\background\byc-bg-img.jpeg" alt="" class="bg-img">
    <div class="irbox">
        <form action="#" method="post">
            <div class="wrap-login100">                  
                <span class="login100-form-logo">
                <i class="zmdi zmdi-landscape"></i>
                </span>

                <span class="login100-form-title p-b-34 p-t-27">
                Registrate
                </span>

                <div class="wrap-input100 validate-input" data-validate = "Ingrese su usuario">
                <input class="input100" type="hidden" name="tipoRegistro" value="<?php echo openssl_encrypt("1", COD, KEY)?>">
                <input class="input100" type="text" name="nuevoUsuario" placeholder="Usuario">
                <span class="focus-input100" data-placeholder="&#xf207;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Ingrese su contraseña">
                <input class="input100" type="password" name="nuevaContrasena" placeholder="Contraseña">
                <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Ingrese su nombre">
                <input class="input100" type="text" name="nuevoNombre" placeholder="Nombre">
                <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Ingrese su apellido">
                <input class="input100" type="text" name="nuevoApellido" placeholder="Apellido">
                <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Ingrese su Fecha de nacimiento">
                <input class="input100" type="date" name="nuevaFecha" placeholder="Fecha de nacimiento">
                <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Ingrese su pais">
                <input class="input100" type="text" name="nuevoPais" placeholder="Pais">
                <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Ingrese su ciudad">
                <input class="input100" type="text" name="nuevaCiudad" placeholder="ciudad">
                <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Ingrese su correo">
                <input class="input100" type="text" name="nuevoCorreo" placeholder="Correo electronico">
                <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="container-login100-form-btn">
                    <button type="submit" id="btnRegistrarModal" class="login100-form-btn">Registrar</button>
                </div>
            </div>          
        </form>
    </div>
</section>
<?php 
    $crearusuario= new Usuario();
    $crearusuario-> crearUsuario();
?>