<?php 
	$validarSesion = Vista::redirigir();

	$item  = "id";
	$valor = $_SESSION["user_id"];

	$datosPerfil = Usuario::mostrarInfoPerfil($item, $valor);

	$datosCuenta = Usuario::mostrarInfoPerfilCuenta($item, $valor);

	if($datosPerfil['imagen_perfil'] != 'vistas/img/perfil/default.jpg')
	{
		$imagen = $datosPerfil['imagen_perfil'];
	}
	else
	{
		$imagen = 'vistas/img/perfil/default.jpg';
	}	
?>
<!-- Perfil -->
<section class="tournaments-section spad">
	<img src="vistas\img\background\room-bg-img.jpeg" alt="" class="bg-img">
	<div class="container">
		<div class="section-title">
			<div class="cata new Bpre">Perfil</div>			
        </div>	
		<div class="row">		
			<div class="col-md-6">
				<div class="card greenc mb-5">
					<div class="card-header">Datos de usuario</div>
					<div class="card-body">
						<form action="#" id="formDatosUsuario" method="post" enctype="multipart/form-data">
							<input required="" type="hidden" value="cambio" name="1">
							<input required="" type="hidden" name="imagenActual" value="<?php echo $imagen ?>">
							<div class="float-lg-left mb-3">
								<div class="d-flex justify-content-center w-100">
									<label for="editarImagen" class="cata strategy mb-3">Cambiar foto perfil<input class="form-control" type="file" id="editarImagen" name="editarImagen" class="nuevaImagenPerfil" style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;color: white;"></label>
								</div>
								<div class="d-flex justify-content-center w-100">
									<img class="previsualizar rounded-circle" src="<?php echo $imagen ?>" width="168">
								</div>
							</div>
							<div class="float-lg-right mb-3 w-mw1200-60">
								<h4><?php echo ucwords($datosPerfil["nombre"])." ".ucwords($datosPerfil["apellido"]) ?></h4>
								<ul>
									<li><span>Fecha Nacimiento:</span> <input required="" class="form-control" type="date" name="editarFecha" value="<?php echo $datosPerfil["fecha_nacimiento"] ?>"></li>
									<li><span>Pais:</span> <input required="" class="form-control" type="text" name="editarPais" value="<?php echo $datosPerfil["pais"] ?>"></li>
									<li><span>Ciudad:</span> <input required="" class="form-control" type="text" name="editarCiudad" value="<?php echo $datosPerfil["ciudad"] ?>"></li>
								</ul>
								<div class="Brec">
									<button type="submit" class="btn guardar">Guardar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card greenc mb-5">
					<div class="card-header">datos de acceso</div>
					<div class="card-body">
						<form action="#" method="post">
							<input required="" type="hidden" value="cambio" name="2">
							<div class="float-lg-left mb-3">
								<div class="d-flex justify-content-center">
									<img src="vistas/img/perfil/datosUsuario.png" alt="" width="168">
								</div>
							</div>
							<div class="float-lg-right mb-3 w-mw1200-60">
								<h4>Usuario: <?php echo ucwords($datosPerfil["usuario"]) ?></h4>
								<ul>
									<li><span>Nueva contraseña:</span> <input required="" class="form-control" type="password" name="editarContrasena" placeholder="Nueva contraseña"></li>
									<li><span>Confirmar contraseña:</span> <input required="" class="form-control" type="password" name="validarContrasena" placeholder="confirmar contraseña"></li>
									<li><span>Contraseña Actual:</span> <input required="" class="form-control" type="password" name="contrasena" placeholder="Contraseña Actual"></li>
								</ul>
								<div class="Brec">
									<button type="submit" class="btn">Cambiar contraseña</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card greenc mb-5">
					<div class="card-header">datos de contacto</div>
					<div class="card-body">
						<form action="#" method="post">
							<input required="" type="hidden" value="cambio" name="3">
							<div class="float-lg-left mb-3">
								<div class="d-flex justify-content-center">
									<img src="vistas/img/perfil/datosContacto.jpg" alt="" width="168">
								</div>
							</div>
							<div class="float-lg-right mb-3 w-mw1200-60">
								<ul>
									<li><span>Correo:</span> <input required="" class="form-control" type="email" name="editarCorreo" value="<?php echo $datosPerfil["correo"] ?>"></li>
									<li><span>Telefono:</span> <input required="" class="form-control" type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" name="editarTelefono" value="<?php echo $datosPerfil["telefono"] ?>"></li>
									<li><span>Contraseña:</span> <input required="" class="form-control" type="password" name="contrasena" placeholder="Contraseña Actual"></li>
								</ul>
								<div class="Brec">
									<button type="submit" class="btn">Cambiar datos</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card greenc mb-5">
					<div class="card-header">transferir saldo</div>
					<div class="card-body">
						<form action="#" method="post">
							<input type="hidden" value="cambio" name="5">
							<div class="float-lg-left mb-3">
								<div class="d-flex justify-content-center">
									<img src="vistas/img/perfil/transferencia.png" alt="" width="168">
								</div>
							</div>
							<div class="float-lg-right mb-3 w-mw1200-60">
								<h4>Transferir saldo a otro Usuario</h4>
								<ul>
									<li><span>Usuario:</span> <input required="" class="form-control" type="text" name="enviarUsuario" placeholder="Nombre de usuario "></li>
									<li><span>Monto:</span> <input required="" class="form-control" type="text" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" name="enviarMonto" placeholder="Ingrese el monto a enviar"></li>
									<li><span>Contraseña:</span> <input required="" class="form-control" type="password" name="contrasena" placeholder="Contraseña Actual"></li>
								</ul>
								<div class="Brec">
									<button type="submit" class="btn">Transferir</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card greenc mb-5">
					<div class="card-header">datos de cuenta</div>
					<div class="card-body">
						<form action="#" method="post">
							<input required="" type="hidden" value="cambio" name="4">
							<div class="float-lg-left mb-3">
								<div class="d-flex justify-content-center">
									<img src="vistas/img/perfil/banco.png" alt="" width="168">
								</div>
							</div>
							<div class="float-lg-right mb-3 w-mw1200-60">
								<ul>
									<li><span>Documento de identidad:</span> <input required="" class="form-control" type="text" name="editarCedula" placeholder="Cedula titular de la cuenta" value="<?php echo $datosCuenta["cedula"] ?>"></li>
									<li><span>Nombre titular:</span> <input required="" class="form-control" type="text" name="editarTitular" placeholder="Nombre titular de la cuenta" value="<?php echo $datosCuenta["titular_cuenta"] ?>"></li>
									<li><span>Correo titular:</span> <input required="" class="form-control" type="text" name="editarCorreoTitular" placeholder="correo electronico" value="<?php echo $datosCuenta["correo_titular"] ?>"></li>
									<li><span>Banco:</span> <input required="" class="form-control" type="text" name="editarBanco" placeholder="Nombre del banco" value="<?php echo $datosCuenta["banco"] ?>"></li>
									<li><span>Tipo cuenta:</span> <input required="" class="form-control" type="text" name="editarTipoCuenta" placeholder="tipo de cuenta" value="<?php echo $datosCuenta["tipo_cuenta"] ?>"></li>
									<li><span>Número de Cuenta:</span> <input required="" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control" type="text" name="editarNumeroCuenta" placeholder="Numero de cuenta" value="<?php echo $datosCuenta["numero_cuenta"] ?>"></li>	
									<li><span>Contraseña:</span> <input required="" class="form-control" type="password" name="contrasena" placeholder="Contraseña Actual"></li>								
								</ul>
								<div class="Brec">
									<button type="submit" class="btn">Cambiar cuenta</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>				
		</div>
	</div>
</section>
<!-- Perfil bg -->
<?php 
	$cambiarDatosUsuario = new Usuario();
	$cambiarDatosUsuario ->cambiarInfoPerfil();
?>