<!-- Page section -->
<section class="page-section spad contact-page tournaments-section">
	<img src="vistas\img\background\room-bg-img.jpeg" alt="" class="bg-img">
	<div class="container fns">
		<!--<div class="map" id="map-canvas"></div>-->
		<div class="section-title">
			<div class="Trec">
			<div class="cata new Bpre">Contactanos</div>
			</div>
        </div>
			<div class="row">
				<div class="col-12">
					<div class="card redt w-100 w-lg-75">
						<div class="card-body">
							<form action="" method="post">
								<div class="row">
									<div class="col-12 text-center mb-2" style="font-size:20px;">
										Enviar un mensaje
									</div>
									<div class="col-md-6 mb-1" style="font-size:15px;">
										Puede usar el formulario para enviarnos correo, referente a sus dudas e inquietudes.
									</div>
									<div class="col-md-6"></div>
									<div class="col-md-6">
										<input type="text" class="form-control mb-3" name="nombreContacto" placeholder="Ingrese su nombre">
									</div>
									<div class="col-md-6">
										<input type="email" class="form-control mb-3" name="correoContacto" placeholder="Ingrese su correo electronico">
									</div>
									<div class="col-12">
										<input type="text" class="form-control mb-3" name="asuntoContacto" placeholder="Tema o asunto">
									</div>
									<div class="col-12">
										<textarea name="mensajeContacto" class="form-control mb-3" placeholder="Escriba su mensaje"></textarea>
									</div>
								</div>
								<div class="Brec">
									<button type="submit" class="btn">ENVIAR</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
	</div>
</section>
<!-- Page section end -->
<?php
    $enviarCorreo = new Usuario;
    $enviarCorreo->contacto();
?>