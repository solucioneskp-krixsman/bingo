<?php

if(isset($_SESSION["user_id"]))
{
    $saldoActual = Usuario::mostrarBalance();
    $item  = "id";
    $valor = $_SESSION["user_id"];

    $datosPerfil = Usuario::mostrarInfoPerfil($item, $valor);

    $color = $_GET["ruta"] == 'inicio' ? 'style="color:#aeaeae;"' : 'style="color:#ffffff;"';

    echo ' <input type="hidden" id="sesion" value="si"> 
            <div class="Iper" '.$color.'>
                <ul>
                    <li>
                        <span class="notification">
                            Saldo:
                            <i class="fas fa-dollar-sign ml-2"></i> 
                            '.$saldoActual[0]['saldo'].' USD           
                        </span>
                    </li>
                </ul>
            </div>';
}
?>