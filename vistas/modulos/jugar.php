<?php 
	$validarSesion = Vista::redirigir();
	$baseUrl = Vista::baseUrl();

	$item  = "id";
	$valor = openssl_decrypt($_POST["idSala"], COD, KEY);
	$orden = null;

	$salas = ControladorSalas::ctrMostrarSalas($item, $valor, $orden);

	$item  = "id_sala";
	$premio = ControladorSalas::ctrMostrarPremio($item, $valor, $orden);

	$cartillaComprada = Juego::contarCartillaJugar($valor);	

	$premio_texto = "Premio";
	$style = 'style="font-size: 25px;"';

	if($salas["premio_sala"] == 1)
	{
		$premio_valor = '<i class="fas fa-dollar-sign"></i> '.$premio["monto_premio"];
	}
	elseif($salas["premio_sala"] == 0)
	{		
		$contarCartillas = Juego::mostrarPremiosTipo($salas["id_juego_sala"]);
		$premio_valor = '<i class="fas fa-dollar-sign"></i> '.(75*($contarCartillas[0]["totalCartilla"]*$salas["precio_cartilla_sala"]))/100;
	} 
	elseif($salas["premio_sala"] == 2)
	{		
		$contarCartillas = Juego::mostrarPremiosTipo($salas["id_juego_sala"]);
		$premio_valor = '<img src="admin/'.$premio["imagen_premio"].'" width="180px">';
		$premio_texto = '<strong style="color: black;">Premio:</strong> '.$premio["descripcion_premio"];
		$style = 'style="font-size: 15px;"';
	} 	 

?>
<!-- Feature section -->
<input type="hidden" id="baseUrl" value="<?php echo $baseUrl ?>">
<section class="tournaments-section spad">
	<img src="vistas\img\background\room-bg-img.jpeg" alt="" class="bg-img">
	<div class="container">
		<div class="section-title" style="margin-bottom: 17px;">
			<div class="called position-fixed"></div>
			<div class="cata new Bpre"><?php echo $premio_texto ?></div>
			<h2 style="color: #4eae60; "><?php echo " ".$premio_valor ?></h2>
		</div>
		<div class="row lol">
			<div class="col-lg-3 col-md-3">
				<div class="recent-game-item" style="background-color: rgb(6 70 80 / 70%); border: none; border-top-left-radius: 35px; border-top-right-radius: 35px;">
					<div class="rgi-thumb set-bg" style="border-bottom: 1px solid rgb(134, 13, 13); border-radius: 15px;" data-setbg="admin/<?php echo $salas["imagen_sala"] ?>">
						<div class="cata racing" id="countdown" <?php echo $style; ?>></div>
						<h5 class="cata racing" style="color: black; display: none; font-size: 15px;" id="countoff">Jugando</h5>		
						<br>
						<div class="cata racing" style="background-color: #2E2963"><?php echo $salas["nombre_sala"] ?></div>			
					</div>
					<div class="rgi-content">
						<h5 style="color: #4eae60; ">Compra tus cartillas </h5>
						<p style="font-size: 18px;"><strong>Cartillas compradas: <span id="totalCartillas"><?php if(isset($cartillaComprada[0]["totalCartilla"]) && !empty($cartillaComprada[0]["totalCartilla"])){echo $cartillaComprada[0]["totalCartilla"];}else{echo 0;} ?></span> </strong></p>
						<a href="#" class="comment"></a>
					</div>
				</div>					
			</div>
			<div class="col-lg-8 col-md-8">
				<div class="row">
					<?php 
						$_SESSION["sala_jugar"] = openssl_decrypt($_POST["idSala"], COD, KEY);
						$_SESSION["id_juego"]   = $salas["id_juego_sala"];
						for ($i=1; $i < 7; $i++) 
						{ 
							echo '	<div id="enviar'.$i.'" compra="'.$salas["precio_cartilla_sala"]*$i.'" class="col-lg-3 col-md-6 cartilla" style="margin-top: 1%;">
										<form style="height: 90%;" id="cartilla'.$i.'" method="post">
											<input type="hidden" id="numCartilla" name="numCartilla" value="'.openssl_encrypt($i, COD, KEY).'">
											<input type="hidden" id="userId" name="userId" value="'.openssl_encrypt($_SESSION['user_id'], COD, KEY).'">
											<input type="hidden" id="idSala" name="idSala" value="'.$_POST["idSala"].'">
											<input type="hidden" id="compra" name="compra" value="'.$salas["precio_cartilla_sala"]*$i.'">
											<div class="feature-item set-bg" style=" height: 200px;" data-setbg="vistas/img/cartilla.jpg">
												<span class="cata racing">+'.$i.'</span>
												<div class="fi-content text-white">
													<h5><a href="#"></a></h5>
													<p><i class="fas fa-dollar-sign"></i> '.$salas["precio_cartilla_sala"]*$i.'</p>							
												</div>
											</div>
										</form>
									</div>';
						}
					?>							
				</div>
			</div>
		</div>		
	</div>
</section>
<!-- Feature section end -->
<script>
/*=============================================
CONTADOR SALAS NORMAL
=============================================*/	
	var premioSala = <?php echo $salas["premio_sala"] ?>;

	if(premioSala != 2)
	{
		var end = new Date("<?php echo $salas["tiempo_juego_sala"] ?>");
	    var _second = 1000;
	    var _minute = _second * 60;
	    var _hour = _minute * 60;
	    var _day = _hour * 24;
	    var timer2;

	    function showRemaining() 
	    {
	        var now = new Date();
	        var distance = end - now;
	        var days = Math.floor(distance / _day);
	        var hours = Math.floor((distance % _day) / _hour);
	        var minutes = Math.floor((distance % _hour) / _minute);
	        var seconds = Math.floor((distance % _minute) / _second);

	        if (distance < 0) 
	        {
	            clearInterval(timer2);																				
				$.ajax({
                    url: "<?php echo $baseUrl ?>public/tiempo.php",
                    data: {action: "consulta",
                    	   id_juego: <?php echo $salas["id_juego_sala"] ?>},
                    	   type: "POST",
                    success: function (response) 
                    {
                    	let cartilla = JSON.parse(response);
        				if(cartilla[0]["total"] == 0)
        				{
        					$.ajax({
			                    url: "<?php echo $baseUrl ?>public/tiempo.php",
			                    data: {action: "conteo",
			                    	   tiempo: now,
			                    	   idSala : <?php echo $salas["id"] ?>},
			                    type: "POST",
			                    success: function (response) 
			                    {
			                    	location.reload();
			                    	
			                    }
			            	});
        				}
        				else
        				{
	        				$.ajax({
					            url: "<?php echo $baseUrl ?>public/tiempo.php",
					            data: {action: 'juego',
					            	   valor : 'jugando',			 				            	   
					            	   idSala : <?php echo openssl_decrypt($_POST["idSala"], COD, KEY) ?>},
					            type: "POST",
					            success: function (response) 
					            {	
					            	let cartilla = JSON.parse(response);
			        				if(response == 0)
			        				{
			        					$("#countdown").css({"display" : "none"});
										$("#countoff").css({"display" : "block"});
			        				}
			        				else
			        				{
			        					$.ajax({
						                    url: '<?php echo $baseUrl ?>public/bingo.php',
						                    data: {restart: true,
						                    	   idSala : <?php echo $salas["id_juego_sala"] ?>,
						                    	   cartillas:cartilla},
						                    type: 'POST',
						                    success: function (response) 
						                    {
					        					$(".lol").load("<?php echo $baseUrl ?>public/bingo.php",{restart: true,
						                    	   idSala : <?php echo $salas["id_juego_sala"] ?>,
						                    	   cartillas:cartilla});

				        					}
	            						}); 
			        				}							            							            	
					            }
					        });
        				}
                    }
            	});	
            }
	        else if(distance > 0)
	        {	        	
	        	$("#countdown").html(minutes+":"+seconds);
	        }

         }	
         timer2 = setInterval(showRemaining, 1000);
	}
	else
	{
		var end = new Date("<?php echo $premio["fecha_premio"] ?>");

	    var _second = 1000;
	    var _minute = _second * 60;
	    var _hour = _minute * 60;
	    var _day = _hour * 24;
	    var timer;

	    function showRemaining() {
	        var now = new Date();
	        var distance = end - now;
	        if (distance < 0) {

	            clearInterval(timer);
				$.ajax({
                    url: "<?php echo $baseUrl ?>public/tiempo.php",
                    data: {action: "consulta",
                    	   id_juego: <?php echo $salas["id_juego_sala"] ?>},
                    	   type: "POST",
                    success: function (response) 
                    {
                    	let cartilla = JSON.parse(response);
        				if(cartilla[0]["total"] == 0)
        				{
        					swal({
							  type: "info",
							  title: "Informacion",
							  text: "¡Lamentablente no hubieron jugadores suficientes para esta sala!",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "sala";

										}
									})
        				}
        				else
        				{
        					
	        				$.ajax({
					            url: "<?php echo $baseUrl ?>public/tiempo.php",
					            data: {action: 'juego',
					            	   valor : 'jugando',			 				            	   
					            	   idSala : <?php echo openssl_decrypt($_POST["idSala"], COD, KEY) ?>},
					            type: "POST",
					            success: function (response) 
					            {	
					            	let cartilla = JSON.parse(response);
			        				if(response == 0)
			        				{
			        					$("#countdown").css({"display" : "none"});
										$("#countoff").css({"display" : "block"});
			        				}
			        				else
			        				{
			        					$.ajax({
						                    url: '<?php echo $baseUrl ?>public/bingo.php',
						                    data: {restart: true,
						                    	   idSala : <?php echo $salas["id_juego_sala"] ?>,
						                    	   cartillas:cartilla},
						                    type: 'POST',
						                    success: function (response) 
						                    {
					        					$(".lol").load("<?php echo $baseUrl ?>public/bingo.php",{restart: true,
						                    	   idSala : <?php echo $salas["id_juego_sala"] ?>,
						                    	   cartillas:cartilla});

				        					}
	            						}); 
			        				}							            							            	
					            }
					        });
			                 
        					
        				}
                    }
            	});
	        }
	        var days = Math.floor(distance / _day);
	        var hours = Math.floor((distance % _day) / _hour);
	        var minutes = Math.floor((distance % _hour) / _minute);
	        var seconds = Math.floor((distance % _minute) / _second);

	        if(days != 0)
	        {
	        	document.getElementById("countdown").innerHTML = " Faltan, ";
		        document.getElementById("countdown").innerHTML += days + " dias, ";
		        document.getElementById("countdown").innerHTML += hours + " Hrs, ";
		        document.getElementById("countdown").innerHTML += minutes + " Min, ";
		        document.getElementById("countdown").innerHTML += seconds + " Seg";
	        }
	        else if(days == 0 && hours != 0)
	        {
	        	document.getElementById("countdown").innerHTML = " Faltan, ";
		        document.getElementById("countdown").innerHTML += hours + " Hrs, ";
		        document.getElementById("countdown").innerHTML += minutes + " Min, ";
		        document.getElementById("countdown").innerHTML += seconds + " Seg";
	        }
	        else if(days == 0 && hours == 0 && minutes != 0)
	        {
	        	document.getElementById("countdown").innerHTML = " Faltan, ";
		        document.getElementById("countdown").innerHTML += minutes + " Min, ";
		        document.getElementById("countdown").innerHTML += seconds + " Seg";
	        }
	        else if(days == 0 && hours == 0 && minutes == 0 && seconds != 0)
	        {
	        	document.getElementById("countdown").innerHTML = " Faltan, ";
		        document.getElementById("countdown").innerHTML += seconds + " Seg";
	        }								        

	    }

	    timer = setInterval(showRemaining, 1000);		
	}
</script>


