<!--=====================================
MODAL INICIA SESION
======================================-->

<div id="modalIniciaSesion" style="background-color: #ADD2D8;" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form method="post" action="#" class="login100-form validate-form">
        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body" style="padding: 0px;">
          <div class="limiter">
              <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
              <div class="container-login100">
                <div class="wrap-login100">                  
                    <span class="login100-form-logo">
                      <i class="zmdi zmdi-landscape"></i>
                    </span>

                    <span class="login100-form-title p-b-34 p-t-27">
                      Inicia Sesión
                    </span>

                    <div class="wrap-input100 validate-input" data-validate = "Ingrese Usuario">
                      <input class="input100" type="text" name="usuario" placeholder="Usuario">
                      <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Ingrese Contraseña">
                      <input class="input100" type="password" name="contrasena" placeholder="Contraseña">
                      <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="container-login100-form-btn">
                      <button type="submit" class="login100-form-btn">
                        iniciar
                      </button>
                    </div>

                    <div class="text-center p-t-90">
                      <a class="txt1" href="recuperaContrasena">
                        Recuperar Contraseña
                      </a>
                    </div>
                </div>
              </div>
            </div>
        </div>
      </form>


    </div>

  </div>

</div>

<!--=====================================
MODAL REGISTRO
======================================-->

<div id="modalRegistro" style="background-color: #ADD2D8;" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form method="post" action="#" class="login101-form validate-form">
        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body" style="padding: 0px;">
          <div class="limiter">
              <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
              <div class="container-login101">
                <div class="wrap-login101">                  
                    <span class="login101-form-logo">
                      <i class="zmdi zmdi-landscape"></i>
                    </span>

                    <span class="login101-form-title p-b-34 p-t-27">
                      Registrate
                    </span>

                    <div class="wrap-input101 validate-input" data-validate = "Ingrese su usuario">
                      <input class="input101" type="hidden" name="tipoRegistro" value="<?php echo openssl_encrypt("1", COD, KEY)?>">
                      <input class="input101" type="text" name="nuevoUsuario" placeholder="Usuario">
                      <span class="focus-input101" data-placeholder="&#xf207;"></span>
                    </div>

                    <div class="wrap-input101 validate-input" data-validate="Ingrese su contraseña">
                      <input class="input101" type="password" name="nuevaContrasena" placeholder="Contraseña">
                      <span class="focus-input101" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="wrap-input101 validate-input" data-validate="Ingrese su nombre">
                      <input class="input101" type="text" name="nuevoNombre" placeholder="Nombre">
                      <span class="focus-input101" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="wrap-input101 validate-input" data-validate="Ingrese su apellido">
                      <input class="input101" type="text" name="nuevoApellido" placeholder="Apellido">
                      <span class="focus-input101" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="wrap-input101 validate-input" data-validate="Ingrese su Fecha de nacimiento">
                      <input class="input101" type="date" name="nuevaFecha" placeholder="Fecha de nacimiento">
                      <span class="focus-input101" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="wrap-input101 validate-input" data-validate="Ingrese su pais">
                      <input class="input101" type="text" name="nuevoPais" placeholder="Pais">
                      <span class="focus-input101" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="wrap-input101 validate-input" data-validate="Ingrese su ciudad">
                      <input class="input101" type="text" name="nuevaCiudad" placeholder="ciudad">
                      <span class="focus-input101" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="wrap-input101 validate-input" data-validate="Ingrese su correo">
                      <input class="input101" type="text" name="nuevoCorreo" placeholder="Correo electronico">
                      <span class="focus-input101" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="container-login101-form-btn">
                      <button type="submit" class="login101-form-btn">
                        Registrar
                      </button>
                    </div>
                </div>
              </div>
            </div>
        </div>
      </form>


    </div>

  </div>

</div>

<!--=====================================
MODAL IMAGEN REFERENCIA
======================================-->

<div id="modalImagen" class="modal fade" role="dialog">
  
  <div class="modal-dialog modal-lg">

    <div class="modal-content">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Imagen referencia</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA SUBIR FOTO -->

             <div class="form-group">
              
              <div class="panel">IMAGEN DE REFERENCIA DE LA TRANSFERENCIA</div>

              <img src="admin/vistas/img/premio/default.jpg" class="img-thumbnail previsualizarReferencia" style="width: 100%;">

            </div>

          </div>
        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

        </div> 

    </div>

  </div>

</div>

<!--=====================================
MODAL COMO GANAR
======================================-->

<div id="modalGanar" class="modal fade" role="dialog">
  
  <div class="modal-dialog modal-lg">

    <div class="modal-content">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">¿Como ganar?</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA SUBIR FOTO -->

             <div class="form-group">
              
              <div class="panel"></div>

              <img src="admin/vistas/img/parametros/ganar.png" class="img-thumbnail previsualizarReferencia" style="width: 100%;">

            </div>

          </div>
        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

        </div> 

    </div>

  </div>

</div>