<?php 
	$validarSesion = Vista::redirigir();
	$baseUrl = Vista::baseUrl();

	$parametros	= Estilo::mostrarParametros();
?>
<input type="hidden" id="baseUrl" value="<?php echo $baseUrl ?>">
<!-- Perfil -->
<section class="tournaments-section spad">
	<img src="vistas\img\background\room-bg-img.jpeg" alt="" class="bg-img">
	<div class="container fns">
		<div class="section-title mb-3">
			<div class="cata new Bpre">Recargas</div>
			<!--<h1 class="Trec">Recargas</h1>-->
        </div>
		<br>
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="card greenc mb-3">
					<div class="card-header">
						realiza una transferencia con los siguientes datos
					</div>
					<div class="card-body">
						<div class="pasos">
							P a s o 1
						</div>
						<ul class="Ldat mt-3">
							<li><span>Documento de identidad:</span> <input required="" readonly="" class="form-control" type="text" name="editarCedula" placeholder="Cedula titular de la cuenta" value="23926767"></li>
							<li><span>Nombre titular:</span> <input required="" readonly="" class="form-control" type="text" name="editarTitular" placeholder="Nombre titular de la cuenta" value="krixsman perez"></li>
							<li><span>Correo titular:</span> <input required="" readonly="" class="form-control" type="text" name="editarCorreoTitular" placeholder="correo electronico" value="krixsman@hotmail.com"></li>
							<li><span>Banco:</span> <input required="" readonly="" class="form-control" type="text" name="editarBanco" placeholder="Nombre del banco" value="venezuela"></li>
							<li><span>Tipo cuenta:</span> <input required="" readonly="" class="form-control" type="text" name="editarTipoCuenta" placeholder="tipo de cuenta" value="ahorro"></li>
							<li><span>Número de Cuenta:</span> <input required="" readonly="" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control" type="text" name="editarNumeroCuenta" placeholder="Numero de cuenta" value="01020229950100023175"></li>	
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<form action="#" method="post">
					<div class="card greenc mb-3">
						<div class="card-header">
							Coloca los datos de tu pago
						</div>
						<div class="card-body">
							<div class="pasos">
								P a s o 2
							</div>
							<ul class="Ldat">
								<li><span>Nombre del titular:</span> <input required="" class="form-control" type="text" name="titularRecarga" placeholder="Nombre del titular de la cuenta de la cual transferiste"></li>
								<li><span>Documento de identidad:</span> <input required="" class="form-control" type="text" name="documentoRecarga" placeholder="Documento del titular de la cuenta"></li>
								<li class="mb-4"><span>Banco:</span> <input required="" class="form-control" type="text" name="bancoRecarga" placeholder="Banco del que transferiste"></li>
								<li><span>Monto de recarga: $</span> <input onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" required="" class="form-control" type="text" name="montoRecarga" placeholder="Ejemplo: 10 (Mínimo $ 10.000 - Máximo $ 5.000.000)"></li>
								<li><span>Fecha:</span> <input required="" class="form-control" type="date" name="fechaRecarga" placeholder="Fecha de transferencia"></li>
								<li><span>Número de referencia:</span> <input onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" required="" class="form-control" type="text" name="referenciaRecarga" placeholder="Número de referencia bancaria"></li>
							</ul>
							<div class="Brec">
								<button type="submit" class="btn">Recargar</button>
							</div>	
						</div>
					</div>
				</form>
			</div>
			<div class="col-lg-4">
				<div class="card redt">
					<div class="card-header">
						información:
					</div>
					<div class="card-body">
						<ul>
							<li>
								Las Recargas de Saldo por lo general son acreditadas en tu cuenta Bingo en menos de 15 
								minutos solo si la transferencia es realizada desde el mismo banco. En ciertas ocaciones 
								podría tomar un poco más pero no debes preocuparte.
							</li>
							<li>
								Si la transferencia es realizada desde otro banco puede tomar hasta 24 horas hábiles 
								bancarias y va a depender de que la misma se haga efectiva en nuestra cuenta bancaria.
							</li>
							<li>
								En caso de tener algún inconveniente con tu Recarga puedes comunicarte con nosotros vía 
								Whatsapp al 584268048551 y en menos de 48 horas hábiles atenderemos tu requerimiento. 
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Perfil bg -->
<?php 
	$recargar = new Usuario();
	$recargar ->recargaUsuario();
?>