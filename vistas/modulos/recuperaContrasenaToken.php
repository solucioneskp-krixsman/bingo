<!-- Page section -->
<section class="page-section spad contact-page" style="background-image: url('vistas/img/review-bg-2.jpg'); height: 82%">
	<div class="container">
		<div class="section-title">
			<div class="cata new">Recuperar contraseña</div>
			<h2 style="color: #4eae60; ">Ingresar Codigo</h2>
        </div>		
		<form method="post" action="">
			<div id="registro" class="container">
			    <div class="row offset-lg-1">
			    	 <div class="col-lg-3">

			        </div>
			        <div class="col-lg-4">
			        	<label style="color: white">Ingresa tu codigo de recuperacion
			        		<input type="number" name="recuperaToken" id="recuperaToken" class="form-control" placeholder="Codigo enviado al correo">
			        	</label>
			        </div>
			        <div class="col-lg-4">
			        	<button style="margin-top: 8%;" class="btn btn-success">Enviar</button>
			        </div>
		    	</div>
		    </div>
		</form>		
	</div>
</section>
<?php

  $validarCorreo= new Usuario();
  $validarCorreo-> validarToken();
?>  