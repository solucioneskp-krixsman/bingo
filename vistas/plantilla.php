<?php 
	session_start();
	//error_reporting(0);
	$activo     = Vista::valorPagina();
	$baseUrl    = Vista::baseUrl();
	$parametros	= Estilo::mostrarParametros();
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
	<title><?php echo "Bingo | ".ucwords($activo); ?></title>
	<meta charset="UTF-8">
	<meta name="description" content="Bingo online">
	<meta name="keywords" content="bingo, online, juegos, juego, cartilla, carton, apuesta, ganar, premio">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon --> 
<!--===============================================================================================-->	  
	<link href="admin/<?php echo $parametros["favicon"] ?>" rel="shortcut icon"/>
<!--===============================================================================================-->
	<!-- Google Fonts -->
<!--===============================================================================================-->	
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i" rel="stylesheet">
<!--===============================================================================================-->
	<!-- Stylesheets -->
<!--===============================================================================================-->	
	<link rel="stylesheet" href="vistas/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="vistas/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="vistas/css/fonts.min.css"/>
	<link rel="stylesheet" href="vistas/css/owl.carousel.css"/>
	<link rel="stylesheet" href="vistas/css/style.css"/>
	<link rel="stylesheet" href="vistas/css/animate.css"/>
<!--===============================================================================================-->	
	<!-- DataTables -->
<!--===============================================================================================-->			
	<link rel="stylesheet" href="vistas/js/DataTables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="vistas/js/DataTables/css/responsive.bootstrap.min.css">
<!--===============================================================================================-->
<!-- Login -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vistas/fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" type="text/css" href="vistas/vendor/animate/animate.css">	
	<link rel="stylesheet" type="text/css" href="vistas/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vistas/vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vistas/vendor/select2/select2.min.css">	
	<link rel="stylesheet" type="text/css" href="vistas/vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="vistas/css/login/util.css">
	<link rel="stylesheet" type="text/css" href="vistas/css/login/main.css">
<!--===============================================================================================-->	
<!--====== Jquery ======-->
<!--===============================================================================================-->
    <script src="vistas/js/jquery-3.5.1.min.js"></script>		
<!--===============================================================================================-->	
<!-- SweetAlert 2 -->
<!--===============================================================================================-->	
	<script src="vistas/js/sweetalert2/sweetalert2.all.js"></script>
	<!-- By default SweetAlert2 doesn't support IE. To enable IE 11 support, include Promise polyfill:-->
	<script src="vistas/js/sweetalert2/core.js"></script>
<!--===============================================================================================-->
<!--====== bingo ======-->
<!--===============================================================================================-->    
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css' />    
    <link rel="stylesheet" type="text/css" href="vistas/css/animate.min.css" />
<!--===============================================================================================-->	    



	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>	

	<?php 
		require_once "vistas/modulos/info.php";
		require_once "vistas/modulos/cabecera.php";
		require_once "vistas/modulos/modal.php"; 
		
		$plantilla = new Vista();
		$plantilla -> enlacesPaginasControlador();
			
		require_once "vistas/modulos/footer.php";
	?>

<!--====== Javascripts ======-->
<!--===============================================================================================-->		
	<script src="vistas/js/popper.min.js"></script>
	<script src="vistas/vendor/animsition/js/animsition.min.js"></script>	
	<script src="vistas/js/bootstrap.min.js"></script>
	<script src="vistas/js/owl.carousel.min.js"></script>
	<script src="vistas/js/jquery.marquee.min.js"></script>
	<script src="vistas/js/main.js"></script>
<!--====== Login ======-->
<!--===============================================================================================-->
	<script src="vistas/vendor/bootstrap/js/popper.js"></script>
	<script src="vistas/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vistas/vendor/select2/select2.min.js"></script>
	<script src="vistas/vendor/daterangepicker/moment.min.js"></script>
	<script src="vistas/vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vistas/vendor/countdowntime/countdowntime.js"></script>
	<script src="vistas/js/login/main.js"></script>
<!--===============================================================================================-->	
<!-- DataTables -->
<!--===============================================================================================-->				
	<script src="vistas/js/DataTables/js/jquery.dataTables.min.js"></script>
	<script src="vistas/js/DataTables/js/dataTables.bootstrap.min.js"></script>
	<script src="vistas/js/DataTables/js/dataTables.responsive.min.js"></script>
	<script src="vistas/js/DataTables/js/responsive.bootstrap.min.js"></script>	
<!--===============================================================================================-->
<!--====== bingo ======-->
<!--===============================================================================================-->
	<script src="vistas/js/bingo/bingo.js"></script>
	<script src="vistas/js/usuario/perfil.js"></script>
<!--===============================================================================================-->
	<a href="https://api.whatsapp.com/send?phone=<?php echo $parametros["telefono_parametros"] ?>&text=Hola buen dia, estoy interesado en" class="btnFlotanteWhatss" target="_blank"><i class="fab fa-whatsapp"></i></a>
	
    </body>
</html>