-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 17-01-2022 a las 14:56:27
-- Versión del servidor: 10.6.5-MariaDB
-- Versión de PHP: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `escuelae_bingo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apuesta`
--

CREATE TABLE `apuesta` (
  `id` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `num_cartilla` int(11) NOT NULL,
  `precio_cartilla` decimal(20,2) NOT NULL,
  `id_juego` int(11) NOT NULL,
  `fecha_apuesta` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `apuesta`
--

INSERT INTO `apuesta` (`id`, `id_sala`, `id_usuario`, `num_cartilla`, `precio_cartilla`, `id_juego`, `fecha_apuesta`) VALUES
(1, 1, 17, 2, 0.00, 74511, '2021-01-27'),
(2, 1, 17, 2, 0.00, 97355, '2021-02-09'),
(3, 1, 14, 6, 0.00, 77341, '2021-02-10'),
(4, 1, 17, 1, 0.00, 77341, '2021-03-02'),
(5, 1, 17, 1, 0.00, 59281, '2021-03-17'),
(6, 1, 1, 1, 0.00, 85504, '2021-08-16'),
(7, 3, 1, 2, 0.00, 71349, '2021-11-19'),
(8, 5, 1, 2, 0.00, 56797, '2021-11-19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carousel`
--

CREATE TABLE `carousel` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` varchar(255) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `estado` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carousel`
--

INSERT INTO `carousel` (`id`, `titulo`, `texto`, `imagen`, `estado`) VALUES
(3, 'Banner de %Prueba%', 'Este es el banner de prueba', 'vistas/img/slider/1/518.jpg', 'activa'),
(4, 'Banner  2 % texto que resalta@', 'Este es el texto de descripcion', 'vistas/img/slider/4/246.png', 'activa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `saldo` decimal(20,2) NOT NULL,
  `monedas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`id`, `id_usuario`, `saldo`, `monedas`) VALUES
(1, 1, 26.00, '0.00'),
(2, 2, 0.00, '0.00'),
(3, 3, 10.00, '0.00'),
(4, 4, 0.00, '0.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta_banco`
--

CREATE TABLE `cuenta_banco` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `cedula` varchar(255) NOT NULL,
  `titular_cuenta` varchar(255) NOT NULL,
  `correo_titular` varchar(255) NOT NULL,
  `banco` varchar(255) NOT NULL,
  `tipo_cuenta` varchar(255) NOT NULL,
  `numero_cuenta` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cuenta_banco`
--

INSERT INTO `cuenta_banco` (`id`, `id_usuario`, `cedula`, `titular_cuenta`, `correo_titular`, `banco`, `tipo_cuenta`, `numero_cuenta`) VALUES
(1, 1, '', '', '', '', '', ''),
(2, 2, '', '', '', '', '', ''),
(3, 3, '', '', '', '', '', ''),
(4, 4, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ganador`
--

CREATE TABLE `ganador` (
  `id` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_juego` int(11) NOT NULL,
  `premio` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ganador`
--

INSERT INTO `ganador` (`id`, `id_sala`, `id_usuario`, `id_juego`, `premio`) VALUES
(1, 1, 17, 74511, '0.015'),
(2, 1, 17, 97355, '0.015'),
(3, 1, 17, 77341, '0.0525'),
(4, 1, 17, 59281, '0.075'),
(5, 1, 1, 85504, '0.375'),
(6, 5, 1, 56797, '10.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juego`
--

CREATE TABLE `juego` (
  `id` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_cartilla` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametros`
--

CREATE TABLE `parametros` (
  `id` int(11) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `favicon` varchar(255) NOT NULL,
  `footerImg` varchar(255) NOT NULL,
  `minimo_parametros` decimal(20,2) NOT NULL,
  `maximo_parametros` decimal(20,2) NOT NULL,
  `telefono_parametros` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `titular_parametros` varchar(255) NOT NULL,
  `cedula_parametros` varchar(255) NOT NULL,
  `correo_parametros` varchar(255) NOT NULL,
  `banco_parametros` varchar(255) NOT NULL,
  `tipo_cuenta_parametros` varchar(255) NOT NULL,
  `numero_cuenta_parametros` varchar(255) NOT NULL,
  `img_banco` varchar(255) NOT NULL,
  `minimo_retiro` decimal(20,2) NOT NULL,
  `maximo_retiro` decimal(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `parametros`
--

INSERT INTO `parametros` (`id`, `logo`, `favicon`, `footerImg`, `minimo_parametros`, `maximo_parametros`, `telefono_parametros`, `nombre`, `titular_parametros`, `cedula_parametros`, `correo_parametros`, `banco_parametros`, `tipo_cuenta_parametros`, `numero_cuenta_parametros`, `img_banco`, `minimo_retiro`, `maximo_retiro`) VALUES
(1, 'vistas/img/parametros/logo.png', 'vistas/img/parametros/favicon/favicon.png', 'vistas/img/parametros/footer/footer.png', 1.00, 20.00, '584268048551', 'Bingo', 'krixsman perez', '23926767', 'krixsman@hotmail.com', 'venezuela', 'ahorro', '01020229950100023175', 'vistas/img/parametros/banco/banco.png', 10.00, 5000.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `premio`
--

CREATE TABLE `premio` (
  `id` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  `imagen_premio` varchar(255) NOT NULL,
  `descripcion_premio` varchar(255) NOT NULL,
  `fecha_premio` varchar(255) NOT NULL,
  `monto_premio` decimal(20,2) NOT NULL,
  `tipo_premio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `premio`
--

INSERT INTO `premio` (`id`, `id_sala`, `imagen_premio`, `descripcion_premio`, `fecha_premio`, `monto_premio`, `tipo_premio`) VALUES
(7, 7, '', '', '', 0.00, 0),
(11, 4, '', '', '', 0.00, 0),
(12, 5, '', 'Premio definido por el administrador', '', 10.00, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recargas`
--

CREATE TABLE `recargas` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_transaccion` int(11) NOT NULL,
  `titular_recarga` varchar(255) NOT NULL,
  `cedula_recarga` varchar(255) NOT NULL,
  `banco_recarga` varchar(255) NOT NULL,
  `monto_recarga` decimal(20,2) NOT NULL,
  `fecha_recarga` varchar(255) NOT NULL,
  `numero_referencia_recarga` varchar(255) NOT NULL,
  `estado_recarga` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `recargas`
--

INSERT INTO `recargas` (`id`, `id_usuario`, `id_transaccion`, `titular_recarga`, `cedula_recarga`, `banco_recarga`, `monto_recarga`, `fecha_recarga`, `numero_referencia_recarga`, `estado_recarga`) VALUES
(1, 17, 69744, 'krixsman', '23926767', 'venezuela', 25.00, '2021-10-19', '545454845575454', 'aprobada'),
(2, 1, 75652, 'krixsman perez', '23926767', 'venezuela', 10.00, '2021-11-17', '142536987', 'aprobada'),
(3, 3, 78642, 'jheremi', '222', '222222', 10.00, '2021-11-19', '2222', 'aprobada'),
(4, 1, 62311, 'krixsman perez', '23926767', 'venezuela', 10.00, '2021-11-19', '12215454', 'aprobada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retirar`
--

CREATE TABLE `retirar` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_transaccion` int(11) NOT NULL,
  `monto_retiro` decimal(20,2) NOT NULL,
  `fecha_retiro` varchar(255) NOT NULL,
  `estado_retiro` varchar(255) NOT NULL,
  `fecha_aprobacion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `numero_referencia_retiro` int(11) NOT NULL,
  `imagen_referencia` varchar(255) NOT NULL,
  `premio_retiro` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sala`
--

CREATE TABLE `sala` (
  `id` int(11) NOT NULL,
  `nombre_sala` varchar(255) NOT NULL,
  `tipo_sala` int(11) NOT NULL,
  `premio_sala` int(11) NOT NULL,
  `imagen_sala` varchar(255) NOT NULL,
  `estado_sala` varchar(255) NOT NULL,
  `juego_sala` varchar(255) NOT NULL,
  `id_juego_sala` int(11) NOT NULL,
  `tiempo_juego_sala` varchar(255) NOT NULL,
  `precio_cartilla_sala` decimal(20,2) NOT NULL,
  `fecha_creacion_sala` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sala`
--

INSERT INTO `sala` (`id`, `nombre_sala`, `tipo_sala`, `premio_sala`, `imagen_sala`, `estado_sala`, `juego_sala`, `id_juego_sala`, `tiempo_juego_sala`, `precio_cartilla_sala`, `fecha_creacion_sala`) VALUES
(4, 'familiar', 0, 0, 'vistas/img/sala/default/default.jpg', 'activa', 'esperando', 98319, '12/12/2021 11:38 PM', 1.00, '2021-11-20 00:29:06'),
(5, 'familiar 2', 0, 1, 'vistas/img/sala/familiar2/233.jpg', 'activa', 'esperando', 94032, '12/12/2021 11:38 PM', 1.00, '2021-11-20 00:29:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp_apuesta`
--

CREATE TABLE `tmp_apuesta` (
  `id` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `num_cartilla` int(11) NOT NULL,
  `id_juego` int(11) NOT NULL,
  `fecha_apuesta` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tmp_apuesta`
--

INSERT INTO `tmp_apuesta` (`id`, `id_sala`, `id_usuario`, `num_cartilla`, `id_juego`, `fecha_apuesta`) VALUES
(132, 3, 1, 2, 71349, '2021-11-19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp_juego`
--

CREATE TABLE `tmp_juego` (
  `id` int(11) NOT NULL,
  `id_juego` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `bingo` varchar(255) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tmp_juego`
--

INSERT INTO `tmp_juego` (`id`, `id_juego`, `numero`, `bingo`, `id_usuario`) VALUES
(1, 98319, 14, '', 0),
(2, 98319, 29, '', 0),
(3, 98319, 64, '', 0),
(4, 98319, 28, '', 0),
(5, 98319, 19, '', 0),
(6, 98319, 9, '', 0),
(7, 98319, 49, '', 0),
(8, 98319, 44, '', 0),
(9, 98319, 16, '', 0),
(10, 98319, 25, '', 0),
(11, 98319, 34, '', 0),
(12, 98319, 41, '', 0),
(13, 98319, 32, '', 0),
(14, 98319, 31, '', 0),
(15, 98319, 13, '', 0),
(16, 98319, 38, '', 0),
(17, 98319, 8, '', 0),
(18, 98319, 24, '', 0),
(19, 98319, 50, '', 0),
(20, 98319, 1, '', 0),
(21, 98319, 12, '', 0),
(22, 98319, 75, '', 0),
(23, 98319, 11, '', 0),
(24, 98319, 2, '', 0),
(25, 98319, 54, '', 0),
(26, 98319, 69, '', 0),
(27, 98319, 36, '', 0),
(28, 98319, 20, '', 0),
(29, 98319, 57, '', 0),
(30, 98319, 4, '', 0),
(31, 98319, 68, '', 0),
(32, 98319, 39, '', 0),
(33, 98319, 6, '', 0),
(34, 98319, 3, '', 0),
(35, 98319, 45, '', 0),
(36, 98319, 67, '', 0),
(37, 98319, 46, '', 0),
(38, 98319, 23, '', 0),
(39, 98319, 55, '', 0),
(40, 98319, 37, '', 0),
(41, 98319, 26, '', 0),
(42, 98319, 56, '', 0),
(43, 98319, 65, '', 0),
(44, 98319, 18, '', 0),
(45, 98319, 66, '', 0),
(46, 98319, 42, '', 0),
(47, 98319, 70, '', 0),
(48, 98319, 7, '', 0),
(49, 98319, 27, '', 0),
(50, 98319, 10, '', 0),
(51, 98319, 15, '', 0),
(52, 98319, 59, '', 0),
(53, 98319, 40, '', 0),
(54, 98319, 17, '', 0),
(55, 98319, 74, '', 0),
(56, 98319, 48, '', 0),
(57, 98319, 30, '', 0),
(58, 98319, 61, '', 0),
(59, 98319, 5, '', 0),
(60, 98319, 73, '', 0),
(61, 98319, 22, '', 0),
(62, 98319, 53, '', 0),
(63, 98319, 71, '', 0),
(64, 98319, 62, '', 0),
(65, 98319, 63, '', 0),
(66, 98319, 43, '', 0),
(67, 98319, 60, '', 0),
(68, 98319, 33, '', 0),
(69, 98319, 52, '', 0),
(70, 98319, 72, '', 0),
(71, 98319, 21, '', 0),
(72, 98319, 58, '', 0),
(73, 98319, 51, '', 0),
(74, 98319, 47, '', 0),
(75, 98319, 35, '', 0),
(151, 94032, 53, '', 0),
(152, 94032, 21, '', 0),
(153, 94032, 33, '', 0),
(154, 94032, 24, '', 0),
(155, 94032, 73, '', 0),
(156, 94032, 28, '', 0),
(157, 94032, 35, '', 0),
(158, 94032, 69, '', 0),
(159, 94032, 63, '', 0),
(160, 94032, 2, '', 0),
(161, 94032, 27, '', 0),
(162, 94032, 32, '', 0),
(163, 94032, 43, '', 0),
(164, 94032, 44, '', 0),
(165, 94032, 40, '', 0),
(166, 94032, 16, '', 0),
(167, 94032, 54, '', 0),
(168, 94032, 58, '', 0),
(169, 94032, 15, '', 0),
(170, 94032, 20, '', 0),
(171, 94032, 70, '', 0),
(172, 94032, 38, '', 0),
(173, 94032, 75, '', 0),
(174, 94032, 68, '', 0),
(175, 94032, 64, '', 0),
(176, 94032, 72, '', 0),
(177, 94032, 11, '', 0),
(178, 94032, 74, '', 0),
(179, 94032, 50, '', 0),
(180, 94032, 55, '', 0),
(181, 94032, 34, '', 0),
(182, 94032, 71, '', 0),
(183, 94032, 19, '', 0),
(184, 94032, 59, '', 0),
(185, 94032, 57, '', 0),
(186, 94032, 46, '', 0),
(187, 94032, 22, '', 0),
(188, 94032, 61, '', 0),
(189, 94032, 14, '', 0),
(190, 94032, 9, '', 0),
(191, 94032, 6, '', 0),
(192, 94032, 47, '', 0),
(193, 94032, 1, '', 0),
(194, 94032, 26, '', 0),
(195, 94032, 31, '', 0),
(196, 94032, 10, '', 0),
(197, 94032, 17, '', 0),
(198, 94032, 12, '', 0),
(199, 94032, 62, '', 0),
(200, 94032, 65, '', 0),
(201, 94032, 23, '', 0),
(202, 94032, 42, '', 0),
(203, 94032, 67, '', 0),
(204, 94032, 37, '', 0),
(205, 94032, 51, '', 0),
(206, 94032, 60, '', 0),
(207, 94032, 25, '', 0),
(208, 94032, 48, '', 0),
(209, 94032, 18, '', 0),
(210, 94032, 36, '', 0),
(211, 94032, 45, '', 0),
(212, 94032, 29, '', 0),
(213, 94032, 3, '', 0),
(214, 94032, 30, '', 0),
(215, 94032, 7, '', 0),
(216, 94032, 5, '', 0),
(217, 94032, 52, '', 0),
(218, 94032, 39, '', 0),
(219, 94032, 4, '', 0),
(220, 94032, 8, '', 0),
(221, 94032, 66, '', 0),
(222, 94032, 56, '', 0),
(223, 94032, 13, '', 0),
(224, 94032, 49, '', 0),
(225, 94032, 41, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp_jugando`
--

CREATE TABLE `tmp_jugando` (
  `id` int(11) NOT NULL,
  `id_sala` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_juego` int(11) NOT NULL,
  `bingo` varchar(255) NOT NULL,
  `premio` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tmp_jugando`
--

INSERT INTO `tmp_jugando` (`id`, `id_sala`, `id_usuario`, `id_juego`, `bingo`, `premio`) VALUES
(1, 4, 0, 98319, '', ''),
(2, 5, 1, 56797, 'si', ''),
(3, 5, 0, 94032, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transacciones`
--

CREATE TABLE `transacciones` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_transaccion` int(11) NOT NULL,
  `tipo_transaccion` varchar(255) NOT NULL,
  `monto_transaccion` decimal(20,2) NOT NULL,
  `referencia_transaccion` int(11) NOT NULL,
  `fecha_transaccion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `estado_transaccion` varchar(255) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `premio_transaccion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `transacciones`
--

INSERT INTO `transacciones` (`id`, `id_usuario`, `id_transaccion`, `tipo_transaccion`, `monto_transaccion`, `referencia_transaccion`, `fecha_transaccion`, `estado_transaccion`, `imagen`, `premio_transaccion`) VALUES
(1, 1, 40964, 'compra', 2.00, 71349, '2021-11-20 00:24:18', 'completada', '', ''),
(2, 1, 93789, 'compra', 2.00, 56797, '2021-11-20 00:30:27', 'completada', '', ''),
(3, 1, 55051, 'premio', 10.00, 56797, '2021-11-20 00:39:15', 'completada', '', ''),
(4, 3, 78642, 'recarga', 10.00, 202110, '2021-11-20 01:24:17', 'aprobada', '', ''),
(5, 1, 62311, 'recarga', 10.00, 202110, '2021-11-20 01:24:25', 'aprobada', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `rol` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `imagen_perfil` varchar(255) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `pais` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `patrocinador` varchar(255) NOT NULL,
  `codigo` int(11) NOT NULL,
  `termCond` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `contrasena`, `rol`, `nombre`, `apellido`, `imagen_perfil`, `fecha_nacimiento`, `pais`, `ciudad`, `correo`, `patrocinador`, `codigo`, `termCond`) VALUES
(1, 'krixsman', 's72MpYgWLjUbISwpuoYlzQ==', 'jugador', 'krixsman', 'Perez', 'vistas/img/perfil/default.jpg', '1994-11-19', 'Venezuela', 'caracas', 'krixsmanp@gmail.com', 'admin', 0, ''),
(2, 'mcdartete', '9BjzrnklOodG6S5ydn6qbg==', 'jugador', 'Darwing', 'P', 'vistas/img/perfil/default.jpg', '2001-02-06', 'Venzuela', 'ZUlia', 'darwingperfer@hotmail.es', 'admin', 0, ''),
(3, 'jheremi', 'KNr2ymdg4QrTwUzdlRkBrQ==', 'jugador', 'jheremi', 'perez', 'vistas/img/perfil/default.jpg', '2002-03-14', 'venezuela ', 'caracas', 'jheremi303@gmail.com', 'admin', 0, ''),
(4, 'key', 'duWXbehcL0jB7f8sBe+ePg==', 'jugador', 'Key', 'Millers', 'vistas/img/perfil/default.jpg', '1997-09-09', 'venezuela', 'caracas', 'thegreypf @gmail.com', 'admin', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_admin`
--

CREATE TABLE `usuario_admin` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8mb3_spanish_ci NOT NULL,
  `usuario` text COLLATE utf8mb3_spanish_ci NOT NULL,
  `password` text COLLATE utf8mb3_spanish_ci NOT NULL,
  `perfil` text COLLATE utf8mb3_spanish_ci NOT NULL,
  `foto` text COLLATE utf8mb3_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `ultimo_login` datetime NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Volcado de datos para la tabla `usuario_admin`
--

INSERT INTO `usuario_admin` (`id`, `nombre`, `usuario`, `password`, `perfil`, `foto`, `estado`, `ultimo_login`, `fecha`) VALUES
(1, 'Administrador', 'admin', '$2a$07$asxx54ahjppf45sd87a5auXBm1Vr2M1NV5t/zNQtGHGpS5fFirrbG', 'Administrador', 'vistas/img/usuarios/admin/672.jpg', 1, '2021-11-29 09:43:52', '2021-11-29 14:43:52'),
(1, 'Administrador', 'admin', '$2a$07$asxx54ahjppf45sd87a5auXBm1Vr2M1NV5t/zNQtGHGpS5fFirrbG', 'Administrador', 'vistas/img/usuarios/admin/672.jpg', 1, '2021-11-29 09:43:52', '2021-11-29 14:43:52');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `apuesta`
--
ALTER TABLE `apuesta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `cuenta_banco`
--
ALTER TABLE `cuenta_banco`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ganador`
--
ALTER TABLE `ganador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `juego`
--
ALTER TABLE `juego`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `parametros`
--
ALTER TABLE `parametros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `premio`
--
ALTER TABLE `premio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sala` (`id_sala`);

--
-- Indices de la tabla `recargas`
--
ALTER TABLE `recargas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `retirar`
--
ALTER TABLE `retirar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sala`
--
ALTER TABLE `sala`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tmp_apuesta`
--
ALTER TABLE `tmp_apuesta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tmp_juego`
--
ALTER TABLE `tmp_juego`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tmp_jugando`
--
ALTER TABLE `tmp_jugando`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sala` (`id_sala`);

--
-- Indices de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `apuesta`
--
ALTER TABLE `apuesta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `carousel`
--
ALTER TABLE `carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cuenta_banco`
--
ALTER TABLE `cuenta_banco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `ganador`
--
ALTER TABLE `ganador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `juego`
--
ALTER TABLE `juego`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `parametros`
--
ALTER TABLE `parametros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `premio`
--
ALTER TABLE `premio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `recargas`
--
ALTER TABLE `recargas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `retirar`
--
ALTER TABLE `retirar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sala`
--
ALTER TABLE `sala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tmp_apuesta`
--
ALTER TABLE `tmp_apuesta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT de la tabla `tmp_juego`
--
ALTER TABLE `tmp_juego`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;

--
-- AUTO_INCREMENT de la tabla `tmp_jugando`
--
ALTER TABLE `tmp_jugando`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `premio`
--
ALTER TABLE `premio`
  ADD CONSTRAINT `premio_ibfk_1` FOREIGN KEY (`id_sala`) REFERENCES `sala` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tmp_jugando`
--
ALTER TABLE `tmp_jugando`
  ADD CONSTRAINT `tmp_jugando_ibfk_1` FOREIGN KEY (`id_sala`) REFERENCES `sala` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
