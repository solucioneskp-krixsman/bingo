<?php 

	require_once "conexion.php";



	class VistaModelo

	{

		public function enlacesPaginasModelo($enlacesModelo)

		{		

			$tabla='usuario';

			$p= new ConexionFront();

			$pdo= $p->conectar();

			$host = $_SERVER["HTTP_HOST"];

		    $url  = $_SERVER["REQUEST_URI"];

			$dir  = "/luismoreno/bingo/" ;

			$baseUrl="https://".$host;

			$baseUrl = $baseUrl.$dir;



			//traer usuarios

			$stmt = $pdo->prepare("SELECT * FROM $tabla where usuario = '$enlacesModelo'");



			$stmt->execute();	



			$usuario = $stmt -> fetchAll(PDO::FETCH_ASSOC);

			

			if(isset($usuario[0]['usuario']) && !empty($usuario[0]['usuario']))

			{

				$usuario = $usuario[0]['usuario'];

			}

			else

			{

				$usuario = "admin";

			}



			if($enlacesModelo == "sala" || 

			   $enlacesModelo == "jugar" ||

			   $enlacesModelo == "recargas" ||

			   $enlacesModelo == "perfil" ||

			   $enlacesModelo == "contacto" ||

			   $enlacesModelo == "inicio" ||

			   $enlacesModelo == "iniciar-sesion" ||

			   $enlacesModelo == "registrarse" ||

			   $enlacesModelo == "transacciones"||

			   $enlacesModelo == "retirar" ||

			   $enlacesModelo == "salir" ||

			   $enlacesModelo == "recuperaContrasena"||

			   $enlacesModelo == "recuperaContrasenaToken"||

			   $enlacesModelo == "recuperaContrasenaCambiar"||

			   $enlacesModelo == "terminos")

			{

				$modulo ="vistas/modulos/".$enlacesModelo.".php";

			}

			elseif($usuario == $enlacesModelo)

			{

				$modulo = "vistas/modulos/registro.php";

			}

			else

			{

				$modulo = "vistas/modulos/404.php";

			}



			if($enlacesModelo == "bingo")

			{

				$modulo = load($baseUrl.'vistas/modulos/bingo.php');



			}

			return $modulo;			

		}

	}



	class UsuarioModelo

	{



		/*=============================================

		REGISTRO DE USUARIO

		=============================================*/

		static public function crearUsuarioModelo($tabla, $rol, $datos)

		{

			$p= new ConexionFront();

			$pdo= $p->conectar();



			if($rol == 'jugador')

			{

				//Insertar usuario 

				$stmt = $pdo->prepare("INSERT INTO $tabla(usuario, contrasena, rol, nombre, apellido, pais, ciudad, imagen_perfil, fecha_nacimiento, correo, patrocinador) VALUES (:usuario, :contrasena, :rol, :nombre, :apellido, :pais, :ciudad, :imagen_perfil, :fecha_nacimiento, :correo, :patrocinador)");



				$stmt->bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);

				$stmt->bindParam(":contrasena", $datos["contrasena"], PDO::PARAM_STR);

				$stmt->bindParam(":rol", $datos["rol"], PDO::PARAM_STR);

				$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);

				$stmt->bindParam(":apellido", $datos["apellido"], PDO::PARAM_STR);

				$stmt->bindParam(":pais", $datos["pais"], PDO::PARAM_STR);

				$stmt->bindParam(":ciudad", $datos["ciudad"], PDO::PARAM_STR);

				$stmt->bindParam(":imagen_perfil", $datos["imagen"], PDO::PARAM_STR);

				$stmt->bindParam(":fecha_nacimiento", $datos["fecha_nacimiento"], PDO::PARAM_STR);

				$stmt->bindParam(":correo", $datos["correo"], PDO::PARAM_STR);	

				$stmt->bindParam(":patrocinador", $datos["patrocinador"], PDO::PARAM_STR);				



				if($stmt->execute())

				{



					return "ok";



				}

				else

				{



					return $stmt->errorInfo();

				

				}



				$stmt = null;

			}

		}



		/*=============================================

		CREAR CUENTA

		=============================================*/

		static public function crearCuentaModelo($tabla, $datos)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();



			//Insertar cuenta

			$stmt = $pdo->prepare("INSERT INTO $tabla(id_usuario, saldo, monedas) VALUES (:id_usuario, :saldo, :monedas)");



			$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);

			$stmt->bindParam(":saldo", $datos["saldo"], PDO::PARAM_STR);

			$stmt->bindParam(":monedas", $datos["monedas"], PDO::PARAM_STR);



			if($stmt->execute())

			{

				$stmt = $pdo->prepare("INSERT INTO cuenta_banco (id_usuario) VALUES (:id_usuario)");



				$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);



				if($stmt->execute())

				{

					return "ok";

				}
				else

				{

					return $stmt->errorInfo();

				}
			}

			else

			{

				return $stmt->errorInfo();

			}



			$stmt = null;		

		}



		/*=============================================

		ACEPTAR TERMINOS Y CONDICIONES

		=============================================*/

		static public function aceptarTerminosModelo($tabla, $item, $valor, $datos)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();



			//traer usuario

			$stmt = $pdo->prepare("UPDATE $tabla SET termCond = :termCond  WHERE $item = :item");



			$stmt -> bindParam(":item", $valor, PDO::PARAM_INT);

			$stmt -> bindParam(":termCond", $datos, PDO::PARAM_STR);



			if($stmt->execute())

			{

				return "ok";

			}

			else

			{

				return "error";

			}				

		}



		/*=============================================

		VALIDAR TERMINOS

		=============================================*/

		static public function verificarTerminosModelo($tabla, $item, $valor)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();



			//traer usuario

			$stmt = $pdo->prepare("SELECT * FROM $tabla WHERE $item = :item");



			$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);



			$stmt->execute();	



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);			

		}



		/*=============================================

		VALIDAR CORREO

		=============================================*/

		static public function validarCorreoModelo($tabla, $item, $valor)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();



			//traer usuario

			$stmt = $pdo->prepare("SELECT * FROM $tabla WHERE $item = :item");



			$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);



			$stmt->execute();	



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);			

		}



		/*=============================================

		VALIDAR TOKEN

		=============================================*/

		static public function validarTokenModelo($tabla, $item, $valor)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();



			//traer usuario

			$stmt = $pdo->prepare("SELECT * FROM $tabla WHERE $item = :item");



			$stmt -> bindParam(":item", $valor, PDO::PARAM_INT);



			$stmt->execute();	



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);			

		}		



		/*=============================================

		ACTUALIZAR CONTRASEÑA

		=============================================*/

		static public function actualizarContrasenaModelo($tabla, $item, $valor, $datos)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();



			//traer usuario

			$stmt = $pdo->prepare("UPDATE $tabla SET contrasena = :contrasena, codigo = :codigo WHERE $item = :item");



			$stmt -> bindParam(":item", $valor, PDO::PARAM_INT);

			$stmt -> bindParam(":contrasena", $datos["contrasena"], PDO::PARAM_STR);

			$stmt -> bindParam(":codigo", $datos["token"], PDO::PARAM_INT);





			if($stmt->execute())

			{

				return "ok";

			}	

			else

			{

				return "error";

			}		

		}



		/*=============================================

		ACTUALIZAR TOKEN

		=============================================*/

		static public function actualizarTokenModelo($tabla, $item, $valor, $token)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();



			//traer usuario

			$stmt = $pdo->prepare("UPDATE $tabla SET codigo = :codigo WHERE $item = :item");



			$stmt -> bindParam(":item", $valor, PDO::PARAM_INT);

			$stmt -> bindParam(":codigo", $token, PDO::PARAM_INT);



			if($stmt->execute())

			{

				return "ok";

			}	

			else

			{

				return "error";

			}		

		}				



		/*=============================================

		OBTENER USUARIO

		=============================================*/

		static public function obtenerUsuarioModelo($tabla)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();



			//traer usuario

			$stmt = $pdo->prepare("SELECT * FROM $tabla order by id desc limit 1");



			$stmt->execute();	



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);			

		}

		

		/*=============================================

		OBTENER USUARIO MODIFICAR

		=============================================*/

		static public function mostrarInfoPerfilModelo($tabla, $item, $valor)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();

            

			if($item != null && $tabla == 'usuario'){



				$stmt = $pdo->prepare("SELECT * FROM $tabla WHERE $item = :$item");



				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);



				$stmt -> execute();



				return $stmt -> fetch(PDO::FETCH_ASSOC);



			}

			elseif($tabla == 'cuenta_banco')

			{



				$stmt = $pdo->prepare("SELECT * FROM $tabla WHERE $item = :$item");



				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);



				$stmt -> execute();



				return $stmt -> fetch(PDO::FETCH_ASSOC);



			}

			else

			{



				$stmt = $pdo->prepare("SELECT * FROM $tabla");



				$stmt -> execute();



				return $stmt -> fetchAll(PDO::FETCH_ASSOC);



			}					



			$stmt = null;

		}

		

		/*=============================================

		USUARIO MODIFICAR

		=============================================*/

		static public function cambiarInfoPerfilrModelo($tabla, $item, $valor, $datos)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();

            

            if($valor == '1')

            {

    			//traer usuario

    			$stmt = $pdo->prepare("UPDATE $tabla SET fecha_nacimiento = :fecha_nacimiento, pais = :pais, ciudad = :ciudad, imagen_perfil = :imagen_perfil where $item = :item");

    			

    			$stmt -> bindParam(":item", $datos["id_usuario"], PDO::PARAM_INT);

    			$stmt -> bindParam(":fecha_nacimiento", $datos["fecha_nacimiento"], PDO::PARAM_STR);

    			$stmt -> bindParam(":pais", $datos["pais"], PDO::PARAM_STR);

    			$stmt -> bindParam(":ciudad", $datos["ciudad"], PDO::PARAM_STR);

    			$stmt -> bindParam(":imagen_perfil", $datos["imagen"], PDO::PARAM_STR);

    

    			if($stmt->execute())

    			{

    			    return "ok";    

    			}

    			else

    			{

    			    return "error";

    			}

            }

            elseif($valor == '2')

            {

                //traer usuario

    			$stmt = $pdo->prepare("UPDATE $tabla SET contrasena = :contrasena where $item = :item");

    			

    			$stmt -> bindParam(":item", $datos["id_usuario"], PDO::PARAM_INT);

    			$stmt -> bindParam(":contrasena", $datos["contrasena"], PDO::PARAM_STR);

    

    			if($stmt->execute())

    			{

    			    return "ok";    

    			}

    			else

    			{

    			    return "error";

    			}

            }

            elseif($valor == '3')

            {

    			$stmt = $pdo->prepare("UPDATE $tabla SET correo = :correo, telefono = :telefono where $item = :item");

    			

    			$stmt -> bindParam(":item", $datos["id_usuario"], PDO::PARAM_INT);

    			$stmt -> bindParam(":correo", $datos["correo"], PDO::PARAM_STR);

    			$stmt -> bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);

    

    			if($stmt->execute())

    			{

    			    return "ok";    

    			}

    			else

    			{

    			    return "error";

    			}

            }

            elseif($valor == '4')

            {

				$stmt = $pdo->prepare("SELECT * FROM $tabla WHERE $item = :item");



				$stmt -> bindParam(":item", $datos["id_usuario"], PDO::PARAM_INT);



				$stmt -> execute();



				$existe = $stmt -> fetch(PDO::FETCH_ASSOC);



    			if(isset($existe["id_usuario"]) && !empty($existe["id_usuario"]))

    			{

					$stmt = $pdo->prepare("UPDATE $tabla SET cedula = :cedula, titular = :titular, correo_titular = :correo_titular, banco = :banco, tipo_cuenta = :tipo_cuenta, numero_cuenta = :numero_cuenta where $item = :item");

	    			

					$stmt->bindParam(":item", $datos["id_usuario"], PDO::PARAM_INT);

					$stmt->bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);

					$stmt->bindParam(":titular", $datos["titular"], PDO::PARAM_STR);

					$stmt->bindParam(":correo_titular", $datos["correo"], PDO::PARAM_STR);

					$stmt->bindParam(":banco", $datos["banco"], PDO::PARAM_STR);

					$stmt->bindParam(":tipo_cuenta", $datos["tipo_cuenta"], PDO::PARAM_STR);

					$stmt->bindParam(":numero_cuenta", $datos["numero_cuenta"], PDO::PARAM_STR); 

	    

	    			if($stmt->execute())

	    			{

	    			    return "ok";    

	    			}

	    			else

	    			{

	    			    return "error";

	    			}    				

    			}

    			else

    			{

					$stmt = $pdo->prepare("INSERT INTO $tabla(id_usuario, cedula, titular, correo_titular, banco, tipo_cuenta, numero_cuenta) VALUES (:id_usuario, :cedula, :titular, :correo_titular, :banco, :tipo_cuenta, :numero_cuenta)");



					$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);

					$stmt->bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);

					$stmt->bindParam(":titular", $datos["titular"], PDO::PARAM_STR);

					$stmt->bindParam(":correo_titular", $datos["correo"], PDO::PARAM_STR);

					$stmt->bindParam(":banco", $datos["banco"], PDO::PARAM_STR);

					$stmt->bindParam(":tipo_cuenta", $datos["tipo_cuenta"], PDO::PARAM_STR);

					$stmt->bindParam(":numero_cuenta", $datos["numero_cuenta"], PDO::PARAM_STR);  



					if($stmt->execute())

	    			{

	    			    return "ok";    

	    			}

	    			else

	    			{

	    			    return "error";

	    			}    				

    			}

            }

            elseif($valor == '6')

            {

    			$stmt = $pdo->prepare("UPDATE $tabla SET logo = :logo, favicon = :favicon, footerImg = :footerImg, nombre = :nombre, minimo_parametros = :minimo_parametros, maximo_parametros = :maximo_parametros, minimo_retiro = :minimo_retiro, maximo_retiro = :maximo_retiro");

    			

    			$stmt -> bindParam(":logo", $datos["imagen"], PDO::PARAM_STR);

    			$stmt -> bindParam(":favicon", $datos["imagen2"], PDO::PARAM_STR);

    			$stmt -> bindParam(":footerImg", $datos["imagen3"], PDO::PARAM_STR);

    			$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);

    			$stmt -> bindParam(":minimo_parametros", $datos["minimo_recarga"], PDO::PARAM_STR);

    			$stmt -> bindParam(":maximo_parametros", $datos["maximo_recarga"], PDO::PARAM_STR);

    			$stmt -> bindParam(":minimo_retiro", $datos["minimo_retiro"], PDO::PARAM_STR);

    			$stmt -> bindParam(":maximo_retiro", $datos["maximo_retiro"], PDO::PARAM_STR);

    

    			if($stmt->execute())

    			{

    			    return "ok";    

    			}

    			else

    			{

    			    return "error";

    			}

            }

            elseif($valor == '7')

            {

    			$stmt = $pdo->prepare("UPDATE $tabla SET img_banco = :img_banco, titular_parametros = :titular_parametros, cedula_parametros = :cedula_parametros, correo_parametros = :correo_parametros, banco_parametros = :banco_parametros, tipo_cuenta_parametros = :tipo_cuenta_parametros, numero_cuenta_parametros = :numero_cuenta_parametros, telefono_parametros = :telefono_parametros");

    			

    			$stmt -> bindParam(":img_banco", $datos["imagen"], PDO::PARAM_STR);

    			$stmt -> bindParam(":titular_parametros", $datos["titular"], PDO::PARAM_STR);

    			$stmt -> bindParam(":cedula_parametros", $datos["cedula"], PDO::PARAM_STR);

    			$stmt -> bindParam(":correo_parametros", $datos["correo"], PDO::PARAM_STR);

    			$stmt -> bindParam(":banco_parametros", $datos["banco"], PDO::PARAM_STR);

    			$stmt -> bindParam(":tipo_cuenta_parametros", $datos["tipo"], PDO::PARAM_STR);

    			$stmt -> bindParam(":numero_cuenta_parametros", $datos["numero"], PDO::PARAM_STR);

    			$stmt -> bindParam(":telefono_parametros", $datos["telefono"], PDO::PARAM_STR);

    

    			if($stmt->execute())

    			{

    			    return "ok";    

    			}

    			else

    			{

    			    return "error";

    			}

            }



		}		



		/*=============================================

		INICIA SESION

		=============================================*/

		static public function iniciaSesionModelo($tabla, $item, $valor)

		{		

			$p= new ConexionFront();

			$pdo= $p->conectar();



			if($item != null){



				$stmt = ConexionFront::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :item");



				$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);



				$stmt -> execute();



				return $stmt -> fetchAll(PDO::FETCH_ASSOC);



			}else{

				//traer usuarios

				$stmt = $pdo->prepare("SELECT * FROM $tabla");



				$stmt->execute();	



				return $stmt -> fetchAll(PDO::FETCH_ASSOC);	

			}		

		}	



		/*=============================================

		RECERGAR BALANCE

		=============================================*/

		static public function recargaUsuarioModelo($tabla, $datos)

		{

            $p= new ConexionFront();

            $pdo= $p->conectar();



            $estado = 'pendiente';

            $id_transaccion = mt_rand(10000, 99999);



			$stmt =$pdo->prepare("INSERT INTO $tabla(id_usuario, titular_recarga, cedula_recarga, banco_recarga, monto_recarga, fecha_recarga, numero_referencia_recarga, estado_recarga, id_transaccion) VALUES (:id_usuario, :titular_recarga, :cedula_recarga, :banco_recarga, :monto_recarga, :fecha_recarga, :numero_referencia_recarga, :estado_recarga, :id_transaccion)");



			$stmt->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);

			$stmt->bindParam(":titular_recarga", $datos['titular_recarga'], PDO::PARAM_STR);

			$stmt->bindParam(":cedula_recarga", $datos['cedula_recarga'], PDO::PARAM_STR);

			$stmt->bindParam(":banco_recarga", $datos['banco_recarga'], PDO::PARAM_STR);

			$stmt->bindParam(":monto_recarga", $datos['monto_recarga'], PDO::PARAM_STR);

			$stmt->bindParam(":fecha_recarga", $datos['fecha_recarga'], PDO::PARAM_STR);

			$stmt->bindParam(":numero_referencia_recarga", $datos['numero_referencia'], PDO::PARAM_STR); 

			$stmt->bindParam(":estado_recarga", $datos['estado_recarga'], PDO::PARAM_STR);

			$stmt->bindParam(":id_transaccion", $id_transaccion, PDO::PARAM_INT);



            if($stmt->execute())

            {	           



				$stmt1 =$pdo->prepare("INSERT INTO transacciones(id_usuario, id_transaccion, tipo_transaccion, monto_transaccion, referencia_transaccion, estado_transaccion) VALUES (:id_usuario, :id_transaccion, :tipo_transaccion, :monto_transaccion, :referencia_transaccion, :estado_transaccion)");



				$stmt1->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);

				$stmt1->bindParam(":id_transaccion", $id_transaccion, PDO::PARAM_INT);

				$stmt1->bindParam(":tipo_transaccion", $datos['tipo_transaccion'], PDO::PARAM_STR);

				$stmt1->bindParam(":monto_transaccion", $datos['monto_transaccion'], PDO::PARAM_STR);

				$stmt1->bindParam(":referencia_transaccion", $datos['referencia_transaccion'], PDO::PARAM_INT);

				$stmt1->bindParam(":estado_transaccion", $datos['estado_transaccion'], PDO::PARAM_STR);



	            if($stmt1->execute())

	            {

	            	return "ok";

	            }

            }



            $stmt = null;	        

		} 



		/*=============================================

		MOSTRAR RETIROS

		=============================================*/

		static public function mostrarRetirarUsuarioModelo($tabla, $datos)

		{

            $p= new ConexionFront();

            $pdo= $p->conectar();



            $stmt = $pdo->prepare("SELECT sum(monto_retiro) as retiro FROM $tabla where id_usuario = :id_usuario and fecha_retiro = :fecha_retiro");



            $stmt -> bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);

            $stmt -> bindParam(":fecha_retiro", $datos['fecha_retiro'], PDO::PARAM_STR);



            $stmt->execute();   



            return $stmt -> fetch(PDO::FETCH_ASSOC);	        

		} 



		/*=============================================

		RETIRAR BALANCE

		=============================================*/

		static public function retirarUsuarioModelo($tabla, $datos)

		{

            $p= new ConexionFront();

            $pdo= $p->conectar();



            $estado = 'pendiente';

            $id_transaccion = mt_rand(10000, 99999);



			$stmt =$pdo->prepare("INSERT INTO $tabla(id_usuario, monto_retiro, estado_retiro, fecha_retiro, id_transaccion) VALUES (:id_usuario, :monto_retiro, :estado_retiro, :fecha_retiro, :id_transaccion)");



			$stmt->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);

			$stmt->bindParam(":monto_retiro", $datos['monto_retiro'], PDO::PARAM_STR); 

			$stmt->bindParam(":estado_retiro", $datos['estado_retiro'], PDO::PARAM_STR);

			$stmt->bindParam(":fecha_retiro", $datos['fecha_retiro'], PDO::PARAM_STR);

			$stmt->bindParam(":id_transaccion", $id_transaccion, PDO::PARAM_INT);



            if($stmt->execute())

            {



				$stmt1 =$pdo->prepare("INSERT INTO transacciones(id_usuario, id_transaccion, tipo_transaccion, monto_transaccion, referencia_transaccion, estado_transaccion) VALUES (:id_usuario, :id_transaccion, :tipo_transaccion, :monto_transaccion, :referencia_transaccion, :estado_transaccion)");



				$stmt1->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);

				$stmt1->bindParam(":id_transaccion", $id_transaccion, PDO::PARAM_INT);

				$stmt1->bindParam(":tipo_transaccion", $datos['tipo_transaccion'], PDO::PARAM_STR);

				$stmt1->bindParam(":monto_transaccion", $datos['monto_transaccion'], PDO::PARAM_STR);

				$stmt1->bindParam(":referencia_transaccion", $datos['referencia_transaccion'], PDO::PARAM_INT);

				$stmt1->bindParam(":estado_transaccion", $datos['estado_transaccion'], PDO::PARAM_STR);



	            if($stmt1->execute())

	            {	

    	           	$stmt = $pdo->prepare("SELECT * FROM cuenta where id_usuario = :id_usuario");



    	           	$stmt->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);



		            $stmt->execute();   



		            $saldoActual = $stmt -> fetch(PDO::FETCH_ASSOC);



		            $saldo = $saldoActual["saldo"] - $datos['monto_transaccion'];



					$stmt1 = $pdo->prepare("UPDATE cuenta set saldo = :saldo where id_usuario = :id_usuario");



	            	$stmt1->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);

	            	$stmt1->bindParam(":saldo", $saldo, PDO::PARAM_STR);

		            

		            if($stmt1->execute())

		            {

		            	return "ok";

		            }

	            }

            }



            $stmt = null;	        

		} 		



		/*=============================================

		MOSTRAR BALANCE

		=============================================*/		

		static public function mostrarBalanceModelo($tabla, $id_usuario)

		{

            $p= new ConexionFront();

            $pdo= $p->conectar();



            $stmt = $pdo->prepare("SELECT * FROM $tabla where id_usuario = '$id_usuario'");



            $stmt->execute();   



            return $stmt -> fetchAll(PDO::FETCH_ASSOC);			

		}



		/*=============================================

		ACTUALIZAR SALDO

		=============================================*/		

		static public function actualizarSaldoModelo($tabla, $item, $datos, $itemCond, $valorCond, $premio)

		{

            $p= new ConexionFront();

            $pdo= $p->conectar();



            $stmt = $pdo->prepare("UPDATE $tabla set $item = :$item where $itemCond = :$itemCond");



            $stmt->bindParam(":".$item, $datos['item_valor'], PDO::PARAM_STR);

            $stmt->bindParam(":".$itemCond, $valorCond, PDO::PARAM_STR);



            if($stmt->execute())

            {

	            $id_transaccion = mt_rand(10000, 99999);

	            if($premio != null)

	            {

	            	$item3 = 'premio_transaccion';	            	

	            }

	            else

	            {

	            	$item3 = 'monto_transaccion';

	            }



				$stmt1 =$pdo->prepare("INSERT INTO transacciones(id_usuario, id_transaccion, tipo_transaccion, $item3, referencia_transaccion, estado_transaccion) VALUES (:id_usuario, :id_transaccion, :tipo_transaccion, :$item3, :referencia_transaccion, :estado_transaccion)");



				$stmt1->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);

				$stmt1->bindParam(":id_transaccion", $id_transaccion, PDO::PARAM_INT);

				$stmt1->bindParam(":tipo_transaccion", $datos['tipo_transaccion'], PDO::PARAM_STR);

				$stmt1->bindParam(":".$item3, $datos['monto_transaccion'], PDO::PARAM_STR);

				$stmt1->bindParam(":referencia_transaccion", $datos['referencia_transaccion'], PDO::PARAM_INT);

				$stmt1->bindParam(":estado_transaccion", $datos['estado_transaccion'], PDO::PARAM_STR);



	            if($stmt1->execute() && $premio == null)

	            {

	            	return "ok";

	            }

	            elseif($stmt1->execute() && $premio != null)

	            {

	            	$datos['fecha_transaccion'] = date("Y-m-d");

					$stmt =$pdo->prepare("INSERT INTO retirar(id_usuario, premio_retiro, estado_retiro, fecha_retiro, id_transaccion) VALUES (:id_usuario, :premio_retiro, :estado_retiro, :fecha_retiro, :id_transaccion)");



					$stmt->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);

					$stmt->bindParam(":premio_retiro", $datos['monto_transaccion'], PDO::PARAM_STR); 

					$stmt->bindParam(":estado_retiro", $datos['estado_transaccion'], PDO::PARAM_STR);

					$stmt->bindParam(":fecha_retiro", $datos['fecha_transaccion'], PDO::PARAM_STR);

					$stmt->bindParam(":id_transaccion", $id_transaccion, PDO::PARAM_INT);



					if($stmt->execute())

		            {

		            	return "ok";

		            }	            	

	            }

            }

		}

		

		

		/*=============================================

		TRANSFERIR SALDO

		=============================================*/		

		static public function transferiSaldoModelo($tabla, $tabla2, $datos)

		{

            $p= new ConexionFront();

            $pdo= $p->conectar();



            //traer id de usuario receptor

            $stmt = $pdo->prepare("SELECT * FROM $tabla2 where usuario = :usuario");



            $stmt->bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);



            $stmt->execute();

            

            $datosUsuario = $stmt -> fetch(PDO::FETCH_ASSOC);



            $id_transferencia = $datosUsuario['id'];

            

            if($id_transferencia != '')

            {

	            if($id_transferencia == $datos["id_usuario"])

	            {

	            	return "igual";

	            	die();

	            }



	            //traer balance de usuario emisor

	            $stmt1 = $pdo->prepare("SELECT * FROM $tabla where id_usuario = :id_usuario");



	            $stmt1->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);



	            $stmt1->execute();   



	            $balanceActual = $stmt1 -> fetch(PDO::FETCH_ASSOC);



                if($balanceActual['saldo'] < $datos["monto"])

                {

	                return "insuficiente";

	                die();                    

                }

    

	            $total = $balanceActual['saldo'] - $datos["monto"];



	            if($total < 0)

	            {

	            	$total = 0;

	            }



	            //traer balance de usuario receptor

	            $stmt2 = $pdo->prepare("SELECT * FROM $tabla where id_usuario = :id_usuario ");



	            $stmt2->bindParam(":id_usuario", $id_transferencia, PDO::PARAM_INT);



	            $stmt2->execute();   



	            $balanceActualReceptor = $stmt2 -> fetch(PDO::FETCH_ASSOC);



	            $total_receptor = $balanceActualReceptor['saldo'] + $datos["monto"];



	            //Actualizar balance en usuario emisor

	            $stmt3 = $pdo->prepare("UPDATE $tabla set saldo = :saldo where id_usuario = :id_usuario");



	            $stmt3->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);

	            $stmt3->bindParam(":saldo", $total, PDO::PARAM_STR);



	            $stmt3->execute();



	            //Actualizar balance en usuario receptor

	            $stmt4 = $pdo->prepare("UPDATE $tabla set saldo = :saldo where id_usuario = :id_usuario");



	            $stmt4->bindParam(":id_usuario", $id_transferencia, PDO::PARAM_INT);

	            $stmt4->bindParam(":saldo", $total_receptor, PDO::PARAM_STR);



	            if($stmt4->execute())

	            {

		            $id_transaccion = mt_rand(10000, 99999);

		            $datos['referencia_transaccion'] = date("Y").$datos['monto'];

		            $datos['estado_transaccion'] = 'completada';

		            $datos['tipo_transaccion']   = 'Transferencia a usuario: '.ucwords($datos["usuario"]);



					$stmt1 =$pdo->prepare("INSERT INTO transacciones(id_usuario, id_transaccion, tipo_transaccion, monto_transaccion, referencia_transaccion, estado_transaccion) VALUES (:id_usuario, :id_transaccion, :tipo_transaccion, :monto_transaccion, :referencia_transaccion, :estado_transaccion)");



					$stmt1->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);

					$stmt1->bindParam(":id_transaccion", $id_transaccion, PDO::PARAM_INT);

					$stmt1->bindParam(":tipo_transaccion", $datos['tipo_transaccion'], PDO::PARAM_STR);

					$stmt1->bindParam(":monto_transaccion", $datos['monto'], PDO::PARAM_STR);

					$stmt1->bindParam(":referencia_transaccion", $datos['referencia_transaccion'], PDO::PARAM_INT);

					$stmt1->bindParam(":estado_transaccion", $datos['estado_transaccion'], PDO::PARAM_STR);



		            if($stmt1->execute())

		            {

			            $id_transaccion = mt_rand(10000, 99999);

			            $datos['referencia_transaccion'] = date("Y").$datos['monto'];

			            $datos['estado_transaccion'] = 'completada';

			            $datos['tipo_transaccion']   = 'Transferencia del usuario: '.ucwords($datos["usuario"]);



						$stmt1 =$pdo->prepare("INSERT INTO transacciones(id_usuario, id_transaccion, tipo_transaccion, monto_transaccion, referencia_transaccion, estado_transaccion) VALUES (:id_usuario, :id_transaccion, :tipo_transaccion, :monto_transaccion, :referencia_transaccion, :estado_transaccion)");



						$stmt1->bindParam(":id_usuario", $id_transferencia, PDO::PARAM_INT);

						$stmt1->bindParam(":id_transaccion", $id_transaccion, PDO::PARAM_INT);

						$stmt1->bindParam(":tipo_transaccion", $datos['tipo_transaccion'], PDO::PARAM_STR);

						$stmt1->bindParam(":monto_transaccion", $datos['monto'], PDO::PARAM_STR);

						$stmt1->bindParam(":referencia_transaccion", $datos['referencia_transaccion'], PDO::PARAM_INT);

						$stmt1->bindParam(":estado_transaccion", $datos['estado_transaccion'], PDO::PARAM_STR);



			            if($stmt1->execute())

			            {

			            	return "ok";

			            }

		            }

	            }

            }

            else

            {

                return "noexiste";

            }

		}	



		/*=============================================

		MOSTRAR REFERIDOS

		=============================================*/



		static public function mostrarReferidosModelo($tabla, $item, $valor)

		{

			if($item != null && $valor == 'emor'){



				$stmt = ConexionFront::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item OR $item = 'emorD'");



				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);



				$stmt -> execute();



				return $stmt -> fetchAll();



			}elseif($item != null){



				$stmt = ConexionFront::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");



				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);



				$stmt -> execute();



				return $stmt -> fetchAll();



			}else{



				$stmt = ConexionFront::conectar()->prepare("SELECT * FROM $tabla");



				$stmt -> execute();



				return $stmt -> fetchAll();



			}



			$stmt -> close();



			$stmt = null;		

		}



		/*=============================================

		MOSTRAR GANADORES

		=============================================*/



		static public function mostrarGanadorModelo($tabla, $tabla2, $item, $valor)

		{

			if($item != null){



				$stmt = ConexionFront::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");



				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);



				$stmt -> execute();



				return $stmt -> fetchAll();



			}else{



				$stmt = ConexionFront::conectar()->prepare("SELECT usuario.nombre, usuario.apellido, usuario.ciudad FROM $tabla INNER JOIN $tabla2 on usuario.id = ganador.id_usuario");



				$stmt -> execute();



				return $stmt -> fetchAll(PDO::FETCH_ASSOC);



			}



			$stmt -> close();



			$stmt = null;		

		}

	}



	class JuegoModelo

	{

		/*=============================================

		CONTAR CARTILLAS

		=============================================*/



		static public function contarCartilllaModelo($tabla, $item, $item2, $datos){



			$p= new ConexionFront();

            $pdo= $p->conectar();





			$stmt = $pdo->prepare("SELECT num_cartilla as totalCartilla, id_juego FROM $tabla WHERE $item = :$item AND $item2 = :$item2");



			$stmt->bindParam(":".$item, $datos["valor_usuario"], PDO::PARAM_INT);

			$stmt->bindParam(":".$item2, $datos["valor_sala"], PDO::PARAM_INT);



			$stmt -> execute();



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);	



			$stmt -> close();



			$stmt = null;



		}



		/*=============================================

		COMPRAR CARTILLAS

		=============================================*/



		static public function comprarCartillasModelo($tabla, $item , $item2, $datos){



			$p= new ConexionFront();

            $pdo= $p->conectar();



            if($item != NULL)

            {

	            $stmt = $pdo->prepare("UPDATE $tabla SET num_cartilla = :num_cartilla, precio_Cartilla = :precio_Cartilla WHERE $item = :$item AND $item2 = :$item2 AND id_sala = :id_sala");



				$stmt->bindParam(":id_sala", $datos["id_sala"], PDO::PARAM_INT);				

				$stmt->bindParam(":num_cartilla", $datos["num_cartilla"], PDO::PARAM_INT);

				$stmt->bindParam(":precio_Cartilla", $datos["precio"], PDO::PARAM_STR);

				$stmt->bindParam(":".$item, $datos["valor_usuario"], PDO::PARAM_INT);

				$stmt->bindParam(":".$item2, $datos["valor_juego"], PDO::PARAM_INT);



				if($stmt->execute())

				{					

		            $stmt = $pdo->prepare("UPDATE tmp_apuesta SET num_cartilla = :num_cartilla WHERE $item = :$item AND id_sala = :id_sala");



					$stmt->bindParam(":id_sala", $datos["id_sala"], PDO::PARAM_INT);

					$stmt->bindParam(":".$item, $datos["valor_usuario"], PDO::PARAM_INT);

					$stmt->bindParam(":num_cartilla", $datos["num_cartilla"], PDO::PARAM_INT);



					if($stmt->execute())

					{				

						return "ok";

					}

				} 

            }

            else

            {

            	

	            $stmt = $pdo->prepare("INSERT INTO $tabla(id_sala, id_usuario, num_cartilla, id_juego, fecha_apuesta) VALUES(:id_sala, :id_usuario, :num_cartilla, :id_juego, :fecha_apuesta)");



				$stmt->bindParam(":id_sala", $datos["id_sala"], PDO::PARAM_INT);

				$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);

				$stmt->bindParam(":num_cartilla", $datos["num_cartilla"], PDO::PARAM_INT);

				$stmt->bindParam(":id_juego", $datos["id_juego"], PDO::PARAM_INT);				

				$stmt->bindParam(":fecha_apuesta", $datos["valor_fecha"], PDO::PARAM_STR);



				if($stmt->execute())

				{

		            $stmt = $pdo->prepare("INSERT INTO tmp_apuesta(id_sala, id_usuario, num_cartilla, id_juego, fecha_apuesta) VALUES(:id_sala, :id_usuario, :num_cartilla, :id_juego, :fecha_apuesta)");



					$stmt->bindParam(":id_sala", $datos["id_sala"], PDO::PARAM_INT);

					$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);

					$stmt->bindParam(":num_cartilla", $datos["num_cartilla"], PDO::PARAM_INT);

					$stmt->bindParam(":id_juego", $datos["id_juego"], PDO::PARAM_INT);

					$stmt->bindParam(":fecha_apuesta", $datos["valor_fecha"], PDO::PARAM_STR);



					if($stmt->execute())

					{				

						return "ok";

					}

				}            	

            }



			$stmt = null;				

		}



		/*=============================================

		CONTAR CARTILLAS SALA

		=============================================*/



		static public function contarCartilllaSalaModelo($tabla, $item, $datos){



			$p= new ConexionFront();

            $pdo= $p->conectar();





			$stmt = $pdo->prepare("SELECT count(num_cartilla) as totalCartilla FROM $tabla WHERE $item = :$item");



			$stmt->bindParam(":".$item, $datos["item_valor"], PDO::PARAM_INT);				



			$stmt -> execute();



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);	



			$stmt -> close();



			$stmt = null;



		}



		/*=============================================

		MOSTRAR PREMIO SALA

		=============================================*/



		static public function mostrarPremioTipoMOdelo($tabla, $item, $datos){



			$p= new ConexionFront();

            $pdo= $p->conectar();





			$stmt = $pdo->prepare("SELECT SUM(num_cartilla) as totalCartilla FROM $tabla WHERE $item = :$item");



			$stmt->bindParam(":".$item, $datos["item_valor"], PDO::PARAM_INT);				



			$stmt -> execute();



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);	



			$stmt = null;



		}



		/*=============================================

		MOSTRAR CARTILLAS

		=============================================*/



		static public function obtenerCartillaModelo($num){



            $p= new ConexionFront();

            $pdo= $p->conectar();



            for($i = 1; $i < 6; $i++)

			{

				$stmt = $pdo->prepare("SELECT * FROM columna_$i where id_cartilla = '$num'");



				$stmt->execute();	



				$resultados = $stmt -> fetchAll(PDO::FETCH_ASSOC);



				$cartilla[] = array( 0 => $resultados[0]["b"],

									 1 => $resultados[0]["i"],

									 2 => $resultados[0]["n"],

									 3 => $resultados[0]["g"],

									 4 => $resultados[0]["o"]);		

			}



			return $cartilla;



		}	



		/*=============================================

		CANTAR NUMEROS SALA

		=============================================*/



		static public function cantarNumerosSalaModelo($tabla, $item, $datos){



			$p= new ConexionFront();

            $pdo= $p->conectar();



            if($item != null)

            {

				$stmt = $pdo->prepare("SELECT * FROM $tabla WHERE $item = :$item");



				$stmt->bindParam(":".$item, $datos["item_valor"], PDO::PARAM_INT);				



				$stmt -> execute();



				return $stmt -> fetchAll(PDO::FETCH_ASSOC);	

			}

			else

			{

	            $stmt = $pdo->prepare("INSERT INTO $tabla (id_juego, numero, bingo, id_usuario) VALUES(:id_juego, :numero, :bingo, :id_usuario)");



				$stmt->bindParam(":id_juego", $datos["id_juego"], PDO::PARAM_INT);

				$stmt->bindParam(":numero", $datos["numero"], PDO::PARAM_INT);

				$stmt->bindParam(":bingo", $datos["bingo"], PDO::PARAM_STR);							

				$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);



				if($stmt->execute())

				{

			

					return "ok";



				}  				

			}



			$stmt -> close();



			$stmt = null;



		}



		/*=============================================

		MOSTRAR GANADOR SALA

		=============================================*/



		static public function mostrarGanadorBingoModelo($tabla, $item, $valor){



			$p= new ConexionFront();

            $pdo= $p->conectar();



            if($item != null)

            {

				$stmt = $pdo->prepare("SELECT * FROM $tabla WHERE $item = :$item");



				$stmt->bindParam(":".$item, $valor, PDO::PARAM_INT);				



				$stmt -> execute();



				return $stmt -> fetchAll(PDO::FETCH_ASSOC);	

			}

			else

			{

	            $stmt = $pdo->prepare("INSERT INTO $tabla (id_juego, numero, bingo, id_usuario) VALUES(:id_juego, :numero, :bingo, :id_usuario)");



				$stmt->bindParam(":id_juego", $datos["id_juego"], PDO::PARAM_INT);

				$stmt->bindParam(":numero", $datos["numero"], PDO::PARAM_INT);

				$stmt->bindParam(":bingo", $datos["bingo"], PDO::PARAM_STR);							

				$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);



				if($stmt->execute())

				{

			

					return "ok";



				}  				

			}



			$stmt -> close();



			$stmt = null;



		}





		/*=============================================

		OPCIONES DE LA JUGADA EN LA SALA

		=============================================*/



		static public function cantar($tabla, $item, $datos){



			$p= new ConexionFront();

            $pdo= $p->conectar();



            if($item == 'consultaBingo')

            {

				$stmt = $pdo->prepare("SELECT * FROM $tabla where id_juego = :id_juego AND id_sala = :id_sala");	



				$stmt->bindParam(":id_juego", $datos["id_juego"], PDO::PARAM_INT);	

				$stmt->bindParam(":id_sala", $datos["id_sala"], PDO::PARAM_STR);							



				$stmt -> execute();



				return $stmt -> fetchAll(PDO::FETCH_ASSOC);

				

			}

			elseif($item == 'cambiar')

			{

				$stmt = $pdo->prepare("UPDATE sala SET juego_sala = :juego_sala, id_juego_sala = :id_juego_sala, tiempo_juego_sala = :tiempo_juego_sala WHERE id = :id");



				$stmt->bindParam(":id_juego_sala", $datos["id_juego"], PDO::PARAM_INT);				

				$stmt->bindParam(":tiempo_juego_sala", $datos["tiempo"], PDO::PARAM_INT);

				$stmt->bindParam(":id", $datos["id_sala"], PDO::PARAM_INT);

				$stmt->bindParam(":juego_sala", $datos["estado_juego"], PDO::PARAM_STR);



				if($stmt->execute())

				{

					$stmt = $pdo->prepare("INSERT INTO tmp_jugando (id_sala, id_juego) VALUES(:id_sala, :id_juego)");



					$stmt->bindParam(":id_sala", $datos["id_sala"], PDO::PARAM_INT);

					$stmt->bindParam(":id_juego", $datos["id_juego"], PDO::PARAM_INT);



					if($stmt->execute())

					{

						return "ok";

					}

				}

				else

				{

					return $stmt->execute();

				}

				

			}

			elseif($item == 'nuevoJuego') 

			{

				$stmt = $pdo->prepare("INSERT INTO $tabla (numero, id_juego) VALUES(:numero, :id_juego)");



				$stmt->bindParam(":numero", $datos["numero"], PDO::PARAM_INT);

				$stmt->bindParam(":id_juego", $datos["id_juego"], PDO::PARAM_INT);



				if($stmt->execute())

				{					

					return "ok";

				} 

			}



			$stmt = null;



		}



		/*=============================================

		REGISTRAR AL GANADOR

		=============================================*/



		static public function bingoRegistrarModelo($tabla, $item, $datos){



			$p= new ConexionFront();

            $pdo= $p->conectar();



            $stmt = $pdo->prepare("DELETE FROM tmp_juego  WHERE id_juego = :id_juego");



			$stmt->bindParam(":id_juego", $datos["id_juego_anterior"], PDO::PARAM_INT);				



			if($stmt->execute())

			{

				$stmt = $pdo->prepare("INSERT INTO $tabla (id_sala, id_usuario, id_juego, premio) VALUES(:id_sala, :id_usuario, :id_juego, :premio)");



				$stmt->bindParam(":id_sala", $datos["id_sala"], PDO::PARAM_INT);

				$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);

				$stmt->bindParam(":id_juego", $datos["id_juego_anterior"], PDO::PARAM_INT);

				$stmt->bindParam(":premio", $datos["premio"], PDO::PARAM_STR);	



				if($stmt->execute())

				{	

					$bingo = "si";

		            $stmt = $pdo->prepare("UPDATE tmp_jugando SET id_usuario = :id_usuario, bingo = :bingo WHERE id_juego = :id_juego");



					$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);				

					$stmt->bindParam(":bingo", $bingo, PDO::PARAM_STR);

					$stmt->bindParam(":id_juego", $datos["id_juego_anterior"], PDO::PARAM_INT);					

					

					if($stmt->execute())

					{

						$stmt = $pdo->prepare("DELETE FROM tmp_apuesta  WHERE id_juego = :id_juego");



						$stmt->bindParam(":id_juego", $datos["id_juego_anterior"], PDO::PARAM_INT);



						if($stmt->execute())

						{

							return "ok";

						}

						else

						{

							return "error delete";

						}

					}

					else

					{

						return "error actualizar";

					}

				}

				else

				{

					return "error insertar";

				}

			}

			else

			{

				return "error borrar";

			}			

		}		



	}



	class EstiloModelo

	{

		/*=============================================

		MOSTRAR ULTIMOS 5 GANADORES

		=============================================*/



		static public function mostrarUltimosGanadoresModelo()

		{

			$p= new ConexionFront();

            $pdo= $p->conectar();



			$stmt = $pdo->prepare("SELECT * FROM `usuario` INNER JOIN ganador ON usuario.id = ganador.id_usuario GROUP BY ganador.id_usuario ORDER BY SUM(ganador.premio) DESC LIMIT 10;");	



			$stmt -> execute();



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);			

		}



		/*=============================================

		MOSTRAR GANADORES

		=============================================*/



		static public function mostrarGanadoresModelo()

		{

			$p= new ConexionFront();

            $pdo= $p->conectar();



			$stmt = $pdo->prepare("SELECT usuario.usuario, sala.nombre_sala, ganador.premio FROM ganador INNER JOIN sala ON ganador.id_sala = sala.id INNER JOIN usuario ON ganador.id_usuario = usuario.id ");	



			$stmt -> execute();



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);			

		}		



		/*=============================================

		CONTAR GANADORES

		=============================================*/



		static public function contarGanadoresSalaModelo($item)

		{

			$p= new ConexionFront();

            $pdo= $p->conectar();



			$stmt = $pdo->prepare("SELECT count(*) as ganadores FROM ganador WHERE id_sala = :id_sala");



			$stmt->bindParam(":id_sala", $item, PDO::PARAM_INT);	



			$stmt -> execute();



			return $stmt -> fetch(PDO::FETCH_ASSOC);			

		}



		/*=============================================

		MOSTRAR SLIDER

		=============================================*/



		static public function mostrarSliderModelo()

		{

			$p= new ConexionFront();

            $pdo= $p->conectar();



            $estado = "activa";



			$stmt = $pdo->prepare("SELECT * FROM carousel WHERE estado = :estado");	



			$stmt->bindParam(":estado", $estado, PDO::PARAM_STR);



			$stmt -> execute();



			return $stmt -> fetchAll(PDO::FETCH_ASSOC);			

		}



		/*=============================================

		MOSTRAR PARAMETROS

		=============================================*/



		static public function mostrarParametrosModelo()

		{

			$p= new ConexionFront();

            $pdo= $p->conectar();



			$stmt = $pdo->prepare("SELECT * FROM parametros");				



			$stmt -> execute();



			return $stmt -> fetch(PDO::FETCH_ASSOC);			

		}					

	}	

?>