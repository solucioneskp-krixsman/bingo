<?php 
session_start();
	/*----------------------
		LETRA B
	------------------------*/

	function letraB()
	{
		// linea 1
		$b[0] = rand (1, 15);

		//liena 2
		do
		{			
			$b[1] = rand (1, 15);
		}while($b[1] == $b[0]);

		//liena 3
		do
		{			
			$b[2] = rand (1, 15);
		}while($b[2] == $b[0] || $b[2] == $b[1]);

		//liena 4
		do
		{			
			$b[3] = rand (1, 15);
		}while($b[3] == $b[0] || $b[3] == $b[1] || $b[3] == $b[2]);

		//liena 5
		do
		{			
			$b[4] = rand (1, 15);
		}while($b[4] == $b[0] || $b[4] == $b[1] || $b[4] == $b[2] || $b[4] == $b[3]);

		return $b;		
	}

	/*----------------------
		LETRA I
	------------------------*/

	function letraI()
	{
		// linea 1
		$i[0] = rand (16, 30);

		//liena 2
		do
		{			
			$i[1] = rand (16, 30);
		}while($i[1] == $i[0]);

		//liena 3
		do
		{			
			$i[2] = rand (16, 30);
		}while($i[2] == $i[0] || $i[2] == $i[1]);

		//liena 4
		do
		{			
			$i[3] = rand (16, 30);
		}while($i[3] == $i[0] || $i[3] == $i[1] || $i[3] == $i[2]);

		//liena 5
		do
		{			
			$i[4] = rand (16, 30);
		}while($i[4] == $i[0] || $i[4] == $i[1] || $i[4] == $i[2] || $i[4] == $i[3]);

		return $i;		
	}

	/*----------------------
		LETRA N
	------------------------*/

	function letraN()
	{
		// linea 1
		$n[0] = rand (31, 45);

		//liena 2
		do
		{			
			$n[1] = rand (31, 45);
		}while($n[1] == $n[0]);

		//liena 3
		do
		{			
			$n[2] = rand (31, 45);
		}while($n[2] == $n[0] || $n[2] == $n[1]);

		//liena 4
		do
		{			
			$n[3] = rand (31, 45);
		}while($n[3] == $n[0] || $n[3] == $n[1] || $n[3] == $n[2]);

		//liena 5
		do
		{			
			$n[4] = rand (31, 45);
		}while($n[4] == $n[0] || $n[4] == $n[1] || $n[4] == $n[2] || $n[4] == $n[3]);

		return $n;		
	}

	/*----------------------
		LETRA G
	------------------------*/

	function letraG()
	{
		// linea 1
		$g[0] = rand (46, 60);

		//liena 2
		do
		{			
			$g[1] = rand (46, 60);
		}while($g[1] == $g[0]);

		//liena 3
		do
		{			
			$g[2] = rand (46, 60);
		}while($g[2] == $g[0] || $g[2] == $g[1]);

		//liena 4
		do
		{			
			$g[3] = rand (46, 60);
		}while($g[3] == $g[0] || $g[3] == $g[1] || $g[3] == $g[2]);

		//liena 5
		do
		{			
			$g[4] = rand (46, 60);
		}while($g[4] == $g[0] || $g[4] == $g[1] || $g[4] == $g[2] || $g[4] == $g[3]);

		return $g;		
	}	

	/*----------------------
		LETRA O
	------------------------*/

	function letraO()
	{
		// linea 1
		$o[0] = rand (61, 75);

		//liena 2
		do
		{			
			$o[1] = rand (61, 75);
		}while($o[1] == $o[0]);

		//liena 3
		do
		{			
			$o[2] = rand (61, 75);
		}while($o[2] == $o[0] || $o[2] == $o[1]);

		//liena 4
		do
		{			
			$o[3] = rand (61, 75);
		}while($o[3] == $o[0] || $o[3] == $o[1] || $o[3] == $o[2]);

		//liena 5
		do
		{			
			$o[4] = rand (61, 75);
		}while($o[4] == $o[0] || $o[4] == $o[1] || $o[4] == $o[2] || $o[4] == $o[3]);

		return $o;		
	}	


$link = new PDO("mysql:host=localhost;dbname=bingo",
			            "root",
			            "");

		$link->exec("set names utf8");


$root = "http://".$_SERVER['HTTP_HOST'];
$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

echo $root;

	/*----------------------
		GENERAR CARTONES
	------------------------*/

	/*for($c = 1; $c < 100; $c++)
	{
		$b = letraB();
		$i = letraI();
		$n = letraN();
		$g = letraG();
		$o = letraO();

		$arrayName = array( 0 => $b, 1 => $i, 2 => $n, 3 => $g, 4 => $o); 

		$stmt = $link->prepare("INSERT INTO cartilla(id) VALUES (:id)");

		$stmt->bindParam(":id", $c, PDO::PARAM_INT);

		$stmt->execute();

		echo '  <table style="border: green 1px solid;">
					<thead>
						<tr>
							<th>B</th>
							<th>I</th>
							<th>N</th>
							<th>G</th>
							<th>O</th>
						</tr>
					</thead>
					<tbody>';

		$columna = 1; 

		for ($j = 0; $j < 5; $j++)
		{
			
			if($columna == 3)
			{
				
				$stmt = $link->prepare("INSERT INTO columna_$columna(id_cartilla, b, i, g, o) VALUES (:id_cartilla, :b, :i, :g, :o)");

				$stmt->bindParam(":id_cartilla", $c, PDO::PARAM_INT);
				$stmt->bindParam(":b", $arrayName[$j][0], PDO::PARAM_INT);
				$stmt->bindParam(":i", $arrayName[$j][1], PDO::PARAM_INT);
				$stmt->bindParam(":g", $arrayName[$j][3], PDO::PARAM_INT);
				$stmt->bindParam(":o", $arrayName[$j][4], PDO::PARAM_INT);

				$stmt->execute();

				echo '	<tr>
							<td style="border: green 1px solid; text-align: center;">'.$b[$j].'</td>
							<td style="border: green 1px solid; text-align: center;">'.$i[$j].'</td>
							<td style="border: green 1px solid; text-align: center;"></td>
							<td style="border: green 1px solid; text-align: center;">'.$g[$j].'</td>
							<td style="border: green 1px solid; text-align: center;">'.$o[$j].'</td>
						</tr>';			
			}
			else
			{
				$stmt = $link->prepare("INSERT INTO columna_$columna(id_cartilla, b, i, n, g, o) VALUES (:id_cartilla, :b, :i, :n, :g, :o)");

				$stmt->bindParam(":id_cartilla", $c, PDO::PARAM_INT);
				$stmt->bindParam(":b", $arrayName[$j][0], PDO::PARAM_INT);
				$stmt->bindParam(":i", $arrayName[$j][1], PDO::PARAM_INT);
				$stmt->bindParam(":n", $arrayName[$j][2], PDO::PARAM_INT);
				$stmt->bindParam(":g", $arrayName[$j][3], PDO::PARAM_INT);
				$stmt->bindParam(":o", $arrayName[$j][4], PDO::PARAM_INT);

				$stmt->execute();

				echo '	<tr>
							<td style="border: green 1px solid; text-align: center;">'.$b[$j].'</td>
							<td style="border: green 1px solid; text-align: center;">'.$i[$j].'</td>
							<td style="border: green 1px solid; text-align: center;">'.$n[$j].'</td>
							<td style="border: green 1px solid; text-align: center;">'.$g[$j].'</td>
							<td style="border: green 1px solid; text-align: center;">'.$o[$j].'</td>
						</tr>';	
			}
			$columna++;
		}
									
		echo '  	</tbody>
				</table> <br> <br>';		
	}*/

	/*----------------------
		MOSTRAR CARTONES
	------------------------*/
				/*$tabla = "ganador";
				$item  = 'nuevoJuego';
				$datos = array("id_sala"           => 24,
            				   "id_usuario"        => 1,
            				   "id_juego_anterior" => 93044,
            				   "premio"            => 1992);
					$bingo = "si";
		            $stmt = $link->prepare("UPDATE tmp_jugando SET id_usuario = :id_usuario, bingo = :bingo WHERE id_juego = :id_juego");

					$stmt->bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);				
					$stmt->bindParam(":bingo", $bingo, PDO::PARAM_STR);
					$stmt->bindParam(":id_juego", $datos["id_juego_anterior"], PDO::PARAM_INT);	

					if ($stmt->execute()) {
						echo "listo";
					}
					else
					{
						echo "erro";
					}*/
						
					/*$called= [];
						for ($i=0; $i < 108 ; $i++) 
	                	{
	                		if(count($called) == 75){

			                }else {
			                    $i = rand(1, 75);
			                    while(in_array($i, $called)){
			                        $i = rand(1, 75);
			                    }
			                    
			                    $called[] = $i;             		
	                		$stmt = $link->prepare("INSERT INTO tmp_juego(id_juego, numero) VALUES ('32870', '$i')");

							$stmt->execute();
						}

	                	}; 
	                	echo "<pre>";
	                	print_r($called);
	                	echo count($called);
	                	echo "</pre>";*/
	                	

	                	/*	    				$tabla = "sala";
	    				$item  = "id";
	    				$valor = $_SESSION["sala_jugar"];
	    				$orden = null;

	    				$sala = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);

		            	$tabla = "tmp_juego";
		            	$item  = "id_juego";

		            	$datos = array('item_valor' => $sala["id_juego_sala"] );

		            	$cantados = JuegoModelo::cantarNumerosSalaModelo($tabla, $item, $datos);

	                if(isset($cantados) && !empty($cantados)){
	                	for ($i=0; $i < count($cantados); $i++) 
	                	{ 
	                		$called[] = $cantados[$i]["numero"]; 
	                	}
	                	$call = [];
	                	if($_SESSION["cantar"] >0)
	                	{
	                		$_SESSION["cantar"] = $_SESSION["cantar"]+1;
	                	}
	                	else
	                	{
	                		$_SESSION["cantar"] = 1;
	                	}

	                    $call[$_SESSION["cantar"]] = $called[$_SESSION["cantar"]];

		            	$output['err'] = $call;
	                    $output['msg'] = $called[$_SESSION["cantar"]];
	                    $output['err'] = true;
	                    $output['msg'] = 'Todos los numero han sido llamados!';
	                }
	                else 
	                {	                                    
	                    
	                }*/
	/*

	$fila = 1;
	for($i = 0; $i < count($filas); $i++)
	{
		for($j = 0; $j < count($filas[$i]); $j++)
		{
			$carton[$j]["fila".$fila] =  array(	"b" => $filas[$i][$j]["b"],
												"i" => $filas[$i][$j]["i"],
												"n" => $filas[$i][$j]["n"],
												"g" => $filas[$i][$j]["g"],
												"o" => $filas[$i][$j]["o"]);
			
		}
		$fila++;	
	}

	for($c = 0; $c < count($carton); $c++)
	{
		echo '  <table style="border: green 1px solid;">
					<thead>
						<tr>
							<th>B</th>
							<th>I</th>
							<th>N</th>
							<th>G</th>
							<th>O</th>
						</tr>
					</thead>
					<tbody>';

		$fila = 1;

		for ($j = 0; $j < 5; $j++)
		{	
			if($carton[$c]["fila".$fila]["n"] == 0)
			{
				$n = '';
			}
			else
			{
				$n = $carton[$c]["fila".$fila]["n"];
			}

			echo '	<tr>
						<td style="border: green 1px solid; text-align: center;">'.$carton[$c]["fila".$fila]["b"].'</td>
						<td style="border: green 1px solid; text-align: center;">'.$carton[$c]["fila".$fila]["i"].'</td>
						<td style="border: green 1px solid; text-align: center;">'.$n.'</td>
						<td style="border: green 1px solid; text-align: center;">'.$carton[$c]["fila".$fila]["g"].'</td>
						<td style="border: green 1px solid; text-align: center;">'.$carton[$c]["fila".$fila]["o"].'</td>
					</tr>';	
			$fila++;
		}
									
		echo '  	</tbody>
				</table> <br> <br>';		
	}*/
	$h = '01020229950100023175';	
	echo substr($h, 0,4)."-".substr($h, 4,4)."-".substr($h, 8,4)."-".substr($h, 12,4)."-".substr($h, 16,4)."-".substr($h, 20,4);
	echo "<br>";
	echo strlen($h);

?>
<script src="vistas/js/jquery-3.5.1.min.js"></script>
<form action="#" method="post">
	<input type="text" name="1" id="countdown2">
	<button type="submit">a</button>
</form>

<div ></div>
<script>
/*var time = '11/10/2020 6:00 PM';
var end = new Date(time);

    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;

    function showRemaining() {
        var now = new Date();
        var distance = end - now;
        if (distance < 0) {

            clearInterval(timer);
            document.getElementById('countdown').innerHTML = 'EXPIRED!';

            return;
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);

        document.getElementById('countdown').innerHTML = days + ' dias, ';
        document.getElementById('countdown').innerHTML += hours + ' horas, ';
        document.getElementById('countdown').innerHTML += minutes + ' minutos y ';
        document.getElementById('countdown').innerHTML += seconds + ' segundos';
    }

    timer = setInterval(showRemaining, 1000);*/
</script>
<br>
<br>
<br>
<span id="countdown" class="timer"></span>
<script>
	var seconds = 1;
	var time = '12/1/2020 6:00 PM';
	var end = new Date(time);
	var now = new Date();
	var distance = end - now;
function secondPassed() {
var minutes = Math.round((seconds - 30)/60);
var remainingSeconds = seconds % 60;
if (remainingSeconds < 10) {
remainingSeconds = "0" + remainingSeconds;
}
document.getElementById("countdown").innerHTML = minutes + ":" +    remainingSeconds;
if (seconds == 0) {
clearInterval(countdownTimer);
document.getElementById("countdown").innerHTML = now+$("#countdown2").val(now);
} else {
seconds--;
}
}
var countdownTimer = setInterval("secondPassed()", 1000);
</script> 

<?php 

$pfx =  "da.p12";
            $password = "Danilo1984";
            $nombreKey = "firma_temp.pem";

$aux = 'openssl pkcs12 -in ' . $pfx . ' -nocerts -out ' . $nombreKey . ' -passin pass:' . $password . ' -passout pass:' . $password . ' 2>&1';
            $salida = shell_exec('openssl pkcs12 -in ' . $pfx . ' -nocerts -out ' . $nombreKey . ' -passin pass:' . $password . ' -passout pass:' . $password . ' 2>&1');

print_r($salida);
?>