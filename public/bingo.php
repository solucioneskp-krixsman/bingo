<?php
    session_start();

    require_once '../controlador/controlador.php';

    $baseUrl = Vista::baseUrl();    
    
    if(isset($_POST['restart'])){
        unset($_SESSION['bingo']);
    }

    if(!isset($_SESSION['bingo'])){
        $_SESSION['bingo'] = serialize(new Juego());    
    }

    $bingo = unserialize($_SESSION['bingo']);

    if(isset($_POST['action'])){
        header('Content-type: application/json');
        echo $bingo->adminitrarMuestra();
        $_SESSION['bingo'] = serialize($bingo);
        exit;
    }


?>
    <script src="vistas/js/bingo/funciones.js"></script>

    <div class="container">
        <h1><?php  ?></h1>
                
        <div class="row"> 
        <?php 
            
            for ($i=1; $i < $_POST["cartillas"]+1; $i++) 
            { 
                $cartilla = Juego::obtenerCartilla($i,$_SESSION["sala_jugar"]);

                echo Juego::pintarCartilla($cartilla, $i, $_SESSION["sala_jugar"]);  

            }

        ?>
        </div>            
    </div>
    <input type="hidden" id="baseUrl" value="<?php echo $baseUrl ?>">

