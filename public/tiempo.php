<?php 
	session_start();
	require_once "../controlador/controlador.php";
	require_once "../modelo/modelo.php";
	require_once "../admin/controladores/salas.controlador.php";
	require_once "../admin/modelos/salas.modelo.php";

  	$action = $_POST['action'];
    $output = [];
    
    switch($action){
        case 'conteo':
            $tiempoSala = new ControladorSalas();
  			$tiempoSala -> ctrCambiarTiempoSala();
            break;
        case 'juego':
            $juegoSala = new ControladorSalas();
  			$juegoSala -> ctrCambiarJuegoSala();
  			$totalCartilla = new Juego();
    		$cartillas = $totalCartilla ->datosSalas($_SESSION["sala_jugar"]);
    		echo $cartillas;
            break;
        case 'consulta':
        	$contarCartillas = new Juego();
        	$total=$contarCartillas->contarCartillasSala();
        	$json = array();
		    $json[] = array(
		      'total' => $total[0]['totalCartilla']
		    );
			  
			$jsonstring = json_encode($json);
			echo $jsonstring;
            break;
        case 'inicia':
        	$totalCartilla = new Juego();
        	$cartillas = $totalCartilla ->datosSalas();
        	echo $cartillas;	
        	break;
        case 'bingog':
    		$bingo = new Juego();
    		$valor=$bingo -> bingoRegistrar();
    		$json = array();

			if($valor == 'true')
			{
				$json['err'] = true;
				$json['msg'] = '¡Haz ganado!';						
			}
			elseif($valor == 'false')
			{
				$json['err'] = true;
				$json['msg'] = '¡Ocurrio un error al registrar tu premio contacta con soporte, con captura de esta pantalla!';
			}
			else
			{
				$json['err'] = true;
				$json['msg'] = $valor;
			}			    	
			$jsonstring = json_encode($json);
			echo $jsonstring;
        	break;
    }
?>