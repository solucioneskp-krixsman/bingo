<?php
	require_once "controlador/controlador.php";
	require_once "controlador/class.phpmailer.php";
	require_once "controlador/class.smtp.php";
	
	require_once "modelo/config.php";
	require_once "modelo/conexion.php";
	require_once "modelo/modelo.php";

	require_once "admin/controladores/salas.controlador.php";
	require_once "admin/controladores/clientes.controlador.php";

	require_once "admin/modelos/salas.modelo.php";
	require_once "admin/modelos/clientes.modelo.php";

	$plantilla = Vista::plantilla();
?>