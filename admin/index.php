<?php

require_once "controladores/plantilla.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/recarga.controlador.php";
require_once "controladores/salas.controlador.php";
require_once "controladores/clientes.controlador.php";
require_once "controladores/slider.controlador.php";
require_once "../controlador/controlador.php";

require_once "modelos/slider.modelo.php";
require_once "modelos/usuarios.modelo.php";
require_once "modelos/recarga.modelo.php";
require_once "modelos/salas.modelo.php";
require_once "modelos/clientes.modelo.php";
require_once "extensiones/vendor/autoload.php";
require_once "../modelo/modelo.php";

$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();