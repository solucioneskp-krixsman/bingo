<?php

require_once "../controladores/clientes.controlador.php";
require_once "../modelos/clientes.modelo.php";

class AjaxClientes{

	/*=============================================
	RECARGAR CLIENTE
	=============================================*/	

	public $idCliente;

	public function ajaxRecargarCliente(){

		$item = "usuario.id";
		$valor = $this->idCliente;

		$respuesta = ControladorClientes::ctrMostrarClientes($item, $valor);

		echo json_encode($respuesta);


	}
	
	/*=============================================
	EDITAR SALDO
	=============================================*/	

/*	public $idCliente;

	public function ajaxEditarCliente(){

		$item = "usuario.id";
		$valor = $this->idCliente;

		$respuesta = ControladorClientes::ctrMostrarClientes($item, $valor);

		echo json_encode($respuesta);


	}	*/
}

/*=============================================
RECARGAR CLIENTE
=============================================*/	

if(isset($_POST["idCliente"])){

	$cliente = new AjaxClientes();
	$cliente -> idCliente = $_POST["idCliente"];
	$cliente -> ajaxRecargarCliente();

}

/*=============================================
EDITAR SALDO
=============================================*/	

if(isset($_POST["idClienteEditar"])){

	$editar = new AjaxClientes();
	$editar -> idClienteEditar = $_POST["idClienteEditar"];
	$editar -> ajaxEditarCliente();

}