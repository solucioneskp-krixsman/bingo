<?php

require_once "../controladores/recarga.controlador.php";
require_once "../modelos/recarga.modelo.php";

class AjaxRecarga{

	/*=============================================
	EDITAR RETIRO
	=============================================*/	

	public $idRetiro;

	public function ajaxEditarRetiro(){

		$item = "id";
		$valor = $this->idRetiro;

		$respuesta = ControladorRecarga::ctrMostrarRetiro($item, $valor);

		echo json_encode($respuesta);

	}

	/*=============================================
	APROBAR RETIRO
	=============================================*/	

	public $idRetiroA;

	public function ajaxAprobarRetiro(){


		$respuesta = ControladorRecarga::ctrAprobarRetiro();

		echo json_encode($respuesta);

	}
}

/*=============================================
EDITAR RETIRO
=============================================*/	
if(isset($_POST["idRetiro"])){

	$retiro = new AjaxRecarga();
	$retiro -> idRetiro = $_POST["idRetiro"];
	$retiro -> ajaxEditarRetiro();
}

/*=============================================
APROBAR RETIRO
=============================================*/	
if(isset($_POST["idRetiroA"])){

	$aprobarRetiro = new AjaxRecarga();
	$aprobarRetiro -> idRetiroA = $_POST["idRetiroA"];
	$aprobarRetiro -> ajaxAprobarRetiro();
}
