<?php

require_once "../controladores/salas.controlador.php";
require_once "../modelos/salas.modelo.php";

class AjaxSalas{

  /*=============================================
  EDITAR SALA
  =============================================*/ 

  public $idSala;

  public function ajaxEditarSala(){

    $item  = "id";
    $orden = null;
    $valor = $this->idSala;

    $respuesta = ControladorSalas::ctrMostrarSalas($item, $valor, $orden);

    echo json_encode($respuesta);

  }

  /*=============================================
  PREMIO CARTILLA
  =============================================*/ 

  public $idCartilla;

  public function ajaxPremioCartilla(){

    $item  = null;
    $item2 = "id";
    $valor = $this->idCartilla;
    $orden = null;

    $respuesta = ControladorSalas::ctrMostrarCartillas($item, $item2, $valor, $orden);

    echo json_encode($respuesta);


  }

  /*=============================================
  GANADORES CARTILLA
  =============================================*/ 

  public $idGanador;

  public function ajaxGanadorCartilla(){

    $item  = null;
    $item2 = "id";
    $valor = $this->idGanador;
    $orden = null;

    $respuesta = ControladorSalas::ctrGanadorCartilla($item, $item2, $valor, $orden);

    echo json_encode($respuesta);


  }


}

/*=============================================
ACTIVAR FUNCION
=============================================*/ 

if(isset($_POST["idSala"])){

  $Sala = new AjaxSalas();
  $Sala -> idSala = $_POST["idSala"];
  $Sala -> ajaxEditarSala();

}

/*=============================================
ACTIVAR FUNCION PREMIO
=============================================*/ 

if(isset($_POST["idCartilla"])){

  $cartilla = new AjaxSalas();
  $cartilla -> idCartilla = $_POST["idCartilla"];
  $cartilla -> ajaxPremioCartilla();

}

/*=============================================
ACTIVAR FUNCION GANADOR
=============================================*/ 

if(isset($_POST["idGanador"])){

  $cartillaGanador = new AjaxSalas();
  $cartillaGanador -> idGanador = $_POST["idGanador"];
  $cartillaGanador -> ajaxGanadorCartilla();

}






