<?php

require_once "../controladores/slider.controlador.php";
require_once "../modelos/slider.modelo.php";

class AjaxSlider{

	/*=============================================
	EDITAR SLIDER
	=============================================*/	

	public $idSlider;

	public function ajaxEditarSlider(){

		$item = "id";
		$valor = $this->idSlider;

		$respuesta = ControladorSlider::ctrMostrarSlider($item, $valor);

		echo json_encode($respuesta);

	}
}

/*=============================================
EDITAR SLIDER
=============================================*/	
if(isset($_POST["idSlider"])){

	$slider = new AjaxSlider();
	$slider -> idSlider = $_POST["idSlider"];
	$slider -> ajaxEditarSlider();
}
