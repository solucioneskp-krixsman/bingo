<?php

require_once "../controladores/salas.controlador.php";
require_once "../modelos/salas.modelo.php";

require_once "../controladores/recarga.controlador.php";
require_once "../modelos/recarga.modelo.php";


class TablaSalas{

 	/*=============================================
 	 MOSTRAR LA TABLA DE SALAS
  	=============================================*/ 

	public function mostrarTablaSalas(){

		$item = null;
		$item2 = null;
    	$valor = null;
    	$orden = "id";

  		$salas = ControladorSalas::ctrMostrarSalas($item, $valor, $orden);	

  		if(count($salas) == 0){

  			echo '{"data": []}';

		  	return;
  		}
		
  		$datosJson = '{
		  "data": [';

		  for($i = 0; $i < count($salas); $i++){

		  	/*=============================================
 	 		TRAEMOS LA IMAGEN
  			=============================================*/

		  	$imagen = "<img src='".$salas[$i]["imagen_sala"]."' width='60px'>";
		  	$imagenPremio  = "<img src='vistas/img/premio/default.jpg' width='60px'>";

		  	/*=============================================
		  	FILTRO PARA EL TIPO DE SALA
		  	=============================================*/

		  	if($salas[$i]["tipo_sala"] == '0')
		  	{
		  		$tipo_sala = "Normal";
		  	}
		  	elseif ($salas[$i]["tipo_sala"] == '1') 
		  	{
		  		$tipo_sala = "Especial";
		  	}

		  	/*=============================================
 	 		TRAEMOS EL PREMIO
  			=============================================*/ 

		  	$item  = "id_sala";
		  	$valor = $salas[$i]["id"];
		  	$orden = null;

		  	$premio = ControladorSalas::ctrMostrarPremio($item, $valor, $orden);

		  	/*=============================================
		  	FILTRO PARA DESACTIVAR BOTON DE PREMIO
		  	=============================================*/

		  	if($salas[$i]["premio_sala"] == '0')
		  	{
		  		$premio_sala   = "disabled";
		  		$mensaje       = "Premio calculado";
		  		$premioMuestra = "75% de las cartillas compradas";
		  	}
		  	else
		  	{
		  		$premio_sala = "";
		  		$mensaje     = "Asignar premio";
		  		if(isset($premio["monto_premio"]))
		  		{
		  			$premioMuestra = $premio["monto_premio"];
		  		}
		  		else
		  		{
		  			$premioMuestra = "Debe asignar un premio";
		  		}
		  		
		  	}

		  	if($salas[$i]["premio_sala"] == '2')
		  	{
		  		$premioMuestra = $premio["descripcion_premio"];
		  		$imagenPremio  = "<img src='".$premio["imagen_premio"]."' width='60px'>";
		  	}
		  	/*=============================================
 	 		TRAEMOS LAS ACCIONES
  			=============================================*/ 

  			if(isset($_GET["perfilOculto"]) && $_GET["perfilOculto"] == "Especial"){

  				$botones =  "<div class='btn-group'><button title='Ingresar Resultados' class='btn btn-success btnResultadoPartido' idSala='".$salas[$i]["id"]."' data-toggle='modal' data-target='#modalResultadoSala'><i class='fa fa-list-ol'></i></button></div>"; 

  			}else{
  				if($salas[$i]["estado_sala"] == 'activa')
				{
				  $valor = "Inactivar";
				  $color = "btn-danger";
				  $icon  = "fa fa-times" ;
				}
				else
				{
				  $valor = "Activar";
				  $color = "btn-success";
				  $icon  = "fa fa-check" ;                              
				}

  				 $botones =  "<div class='btn-group'><button title='".$mensaje."' ".$premio_sala." class='btn btn-success btnPremioSala' idSala='".$salas[$i]["id"]."' tipoPremio='".$salas[$i]["premio_sala"]."'><i class='fa fa-dollar'></i></button><button title='Editar Sala' class='btn btn-info btnEditarSala' idSala='".$salas[$i]["id"]."' data-toggle='modal' data-target='#modalEditarSala'><i class='fa fa-pencil'></i></button><button title='".$valor." Sala' class='btn ".$color." btnActivaSala' idSala='".$salas[$i]["id"]."' codigo='".$salas[$i]["estado_sala"]."'><i class='".$icon."'></i></button><button title='eliminar sala' class='btn btn-danger btn btnEliminarSala' idSala='".$salas[$i]["id"]."' imagen='".$salas[$i]["imagen_sala"]."'><i class='fa fa-times'></i></button></div>"; 

  			}
		 
		  	$datosJson .='[
			      "'.($i+1).'",
			      "'.ucwords($salas[$i]["nombre_sala"]).'",
			      "'.$imagen.'",			    
			      "'.$tipo_sala.'",
			      "'.$imagenPremio.'",
			      "'.$premioMuestra.'",
			      "'.$salas[$i]["fecha_creacion_sala"].'",
			      "'.$salas[$i]["fecha_creacion_sala"].'",
			      "'.ucwords($salas[$i]["estado_sala"]).'",			      
			      "'.$botones.'"
			    ],';

		  }

		  $datosJson = substr($datosJson, 0, -1);

		 $datosJson .=   '] 

		 }';
		
		echo $datosJson;


	}



}

/*=============================================
ACTIVAR TABLA DE partidos
=============================================*/ 
$activarsalas = new TablaSalas();
$activarsalas -> mostrarTablaSalas();

