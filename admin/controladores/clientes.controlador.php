<?php

class ControladorClientes{

	/*=============================================
	MOSTRAR GANADORES
	=============================================*/

	static public function mostrarGanadores()

	{
		$tabla  = "ganador";
		$item   = null;

		$respuesta = ModeloClientes::mdlMostrarGanadores($tabla, $item);



		return $respuesta; 			

	}
	/*=============================================
	MOSTRAR CLIENTES
	=============================================*/

	static public function ctrMostrarClientes($item, $valor){

		$tabla  = "cuenta";
		$tabla2 = "usuario";

		$respuesta = ModeloClientes::mdlMostrarClientes($tabla, $tabla2, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR CLIENTES RECARGAS
	=============================================*/

	static public function ctrMostrarTransacciones($item, $valor){

		$tabla = "transacciones";
		$tabla2 = "usuario";

		$respuesta = ModeloClientes::mdlMostrarTransacciones($tabla, $tabla2, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	EDITAR CLIENTE
	=============================================*/

	static public function ctrRecargarCliente(){

		if(isset($_POST["nuevoMonto"]))
		{

		   	$tabla = "cuenta";

		   	$datos = array("id"=>$_POST["idCliente"],
		   				   "monto"=>$_POST['nuevoMonto']);

		   	$respuesta = ModeloClientes::mdlRecargarCliente($tabla, $datos);
		   	
		   	if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "Recarga efectuada con exito",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "clientes";

								}
							})

				</script>';

			}
			else
			{
				echo'<script>

				swal({
					  type: "error",
					  title: "Error al recargar, intente nuevamente si el problema persiste contacte con soporte",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "clientes";

								}
							})

				</script>';				
			}

		}

	}
	
	static public function ctrEditarSaldoCliente(){

		if(isset($_POST["editarMonto"]))
		{

		   	$tabla = "cuenta";

		   	$datos = array("id"=>$_POST["idClienteEditar"],
		   				   "monto"=>$_POST['editarMonto']);

		   	$respuesta = ModeloClientes::mdlEditarSaldoCliente($tabla, $datos);
		   	
		   	if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "Saldo editado con exito",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "clientes";

								}
							})

				</script>';

			}
			else
			{
				echo'<script>

				swal({
					  type: "error",
					  title: "Error al editar, intente nuevamente si el problema persiste contacte con soporte",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "clientes";

								}
							})

				</script>';				
			}

		}

	}	

	/*=============================================
	ELIMINAR CLIENTE
	=============================================*/

	static public function ctrEliminarCliente(){

		if(isset($_GET["idCliente"])){

			$tabla ="usuario";
			$datos = $_GET["idCliente"];

			$respuesta = ModeloClientes::mdlEliminarCliente($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "El cliente ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result){
								if (result.value) {

								window.location = "clientes";

								}
							})

				</script>';

			}		

		}

	}

}

