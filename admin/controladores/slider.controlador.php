<?php

class ControladorSlider{

	/*=============================================
	CREAR SLIDER
	=============================================*/

	static public function ctrCrearSlider(){

		if(isset($_POST["nuevoSlider"])){

				$tabla = "carousel";

		   		/*=============================================
				VALIDAR IMAGEN
				=============================================*/

			   	$ruta = "vistas/img/slider/default.jpg";

			   	if(isset($_FILES["nuevaImagenSlider"]["tmp_name"])){

					list($ancho, $alto) = getimagesize($_FILES["nuevaImagenSlider"]["tmp_name"]);

					$nuevoAncho = 1920;
					$nuevoAlto = 923;

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directorio = "vistas/img/slider/".$_POST["nuevoSlider"];

					mkdir($directorio, 0755);

					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

					if($_FILES["nuevaImagenSlider"]["type"] == "image/jpeg"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/slider/".$_POST["nuevoSlider"]."/".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($_FILES["nuevaImagenSlider"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);

					}

					if($_FILES["nuevaImagenSlider"]["type"] == "image/png"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/slider/".$_POST["nuevoSlider"]."/".$aleatorio.".png";

						$origen = imagecreatefrompng($_FILES["nuevaImagenSlider"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagepng($destino, $ruta);

					}
				}				

				$datos = array("estado" => "activa",
							   "titulo" => $_POST["nuevoTitulo"],
							   "texto"  => $_POST["nuevoTexto"],
 							   "imagen" => $ruta); 

				$respuesta = ModeloSlider::mdlIngresarSlider($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El banner ha sido guardado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "slider";

									}
								})

					</script>';

				}

		}

	}

	/*=============================================
	MOSTRAR SLIDER
	=============================================*/

	static public function ctrMostrarSlider($item, $valor){

		$tabla = "carousel";

		$respuesta = ModeloSlider::mdlMostrarSlider($tabla, $item, $valor);

		return $respuesta;
	
	}

	/*=============================================
	EDITAR SLIDER
	=============================================*/

	static public function ctrEditarSlider(){

		if(isset($_POST["idSlider"])){

				$tabla = "carousel";

		   		/*=============================================
				VALIDAR IMAGEN
				=============================================*/

			   	$ruta = $_POST["imagenActual"];

			   	if(isset($_FILES["editarImagenSlider"]["tmp_name"]) && !empty($_FILES["editarImagenSlider"]["tmp_name"])){

					list($ancho, $alto) = getimagesize($_FILES["editarImagenSlider"]["tmp_name"]);

					$nuevoAncho = 1000;
					$nuevoAlto = 500;

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directorio = "vistas/img/slider/".$_POST["idSlider"];

					/*=============================================
					PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD
					=============================================*/

					if(!empty($_POST["imagenActual"]) && $_POST["imagenActual"] != "vistas/img/slider/default.jpg"){

						unlink($_POST["imagenActual"]);

					}else{

						mkdir($directorio, 0755);	
					
					}
					
					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

					if($_FILES["editarImagenSlider"]["type"] == "image/jpeg"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/slider/".$_POST["idSlider"]."/".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($_FILES["editarImagenSlider"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);

					}

					if($_FILES["editarImagenSlider"]["type"] == "image/png"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/slider/".$_POST["idSlider"]."/".$aleatorio.".png";

						$origen = imagecreatefrompng($_FILES["editarImagenSlider"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagepng($destino, $ruta);

					}

				}				

				$datos = array( "id"      =>  $_POST["idSlider"],
								"titulo" => $_POST["editarTitulo"],
							  	"texto"  => $_POST["editarTexto"],
							   	"imagen"  =>  $ruta);

				$respuesta = ModeloSlider::mdlEditarSlider($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El banner ha sido cambiado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "slider";

									}
								})

					</script>';

				}
		}

	}

	/*=============================================
	ACTIVAR DESACTIVAR SLIDER
	=============================================*/

	static public function ctrActivaSlider(){

		if(isset($_GET["idSlider"])){

			$tabla ="carousel";
			$datos = $_GET["idSlider"];
		    $item = "id";

    		$estado = ModeloSlider::mdlMostrarSlider($tabla, $item, $datos);
    

			/*if($_GET["imagen"] != "" && $_GET["imagen"] != "vistas/img/slider/default/404.jpg"){

				unlink($_GET["imagen"]);
				rmdir('vistas/img/slider/'.$_GET["codigo"]); 

			}	*/	
			
			if($estado['estado']== 'activa')
			{
			    $valor   = "inactiva";
			    $mensaje = "El banner ha sido inactivado correctamente";
			}
			else
			{
			    $valor = "activa";
			    $mensaje = "El banner ha sido activado correctamente";
			}

			$respuesta = ModeloSlider::mdlActivaSlider($tabla, $datos, $valor);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "'.$mensaje.'",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "slider";

									}
								})

					</script>';
			}
			else
			{
				echo'<script>

					swal({
						  type: "success",
						  title: "Ha ocurrido un error contacta con desarrollo",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "slider";

									}
								})

					</script>';			    
			}
		}
		
	}
	
	/*=============================================
	BORRAR SLIDER
	=============================================*/

	static public function ctrBorrarSlider(){

		if(isset($_GET["idSliderBorrar"])){

			$tabla ="carousel";
			$datos = $_GET["idSliderBorrar"];
		    $item = "id";

			if($_GET["imagen"] != "" && $_GET["imagen"] != "vistas/img/slider/default/404.jpg"){

				unlink($_GET["imagen"]);
				rmdir('vistas/img/slider/'.$_GET["codigo"]); 

			}		

			$respuesta = ModeloSlider::mdlBorrarSlider($tabla, $datos, $item);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "El banner ha sido eliminado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "slider";

									}
								})

					</script>';
			}
			else
			{
				echo'<script>

					swal({
						  type: "success",
						  title: "Ha ocurrido un error contacta con desarrollo",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "slider";

									}
								})

					</script>';			    
			}
		}
		
	}	
}
