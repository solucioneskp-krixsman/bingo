<?php

class ControladorRecarga{

	/*=============================================
	MOSTRAR RECARGAS 
	=============================================*/

	static public function ctrMostrarRecargas()
	{
		$tabla = "recargas";

		$respuesta = ModeloRecarga::mdlMostrarRecargas($tabla);

		return $respuesta; 			
	}

	/*=============================================
	SUMAR RECARGAS 
	=============================================*/

	static public function ctrSumarRecargas()
	{
		$tabla = "recargas";

		$respuesta = ModeloRecarga::mdlSumarRecargas($tabla);

		return $respuesta; 			
	}

	/*=============================================
	MOSTRAR RETIROS 
	=============================================*/

	static public function ctrMostrarRetiro($item,$valor)
	{
		$tabla = "retirar";

		$respuesta = ModeloRecarga::mdlMostrarRetiro($tabla,$item,$valor);

		return $respuesta; 			
	}

	/*=============================================
	SUMAR RETIROS 
	=============================================*/

	static public function ctrSumarRetiro()
	{
		$tabla = "retirar";

		$respuesta = ModeloRecarga::mdlSumarRetiro($tabla);

		return $respuesta; 			
	}

	/*=============================================
	APROBAR RECARGA
	=============================================*/

	static public function ctrAprobarRecarga(){

		if(isset($_GET["idRecarga"])){

			$tabla ="recargas";
			$datos = array( 'id_recarga'    => $_GET["idRecarga"],
							'id_usuario'    => $_GET["idUsuario"],
							'monto_recarga' => $_GET["montoRecarga"],
							'codigo'        => $_GET["codigo"]);
		    $item = "id";
			
		    $valor = "aprobada";
		    $mensaje = "La recarga ha sido aprobada correctamente";

			$respuesta = ModeloRecarga::mdlAprobarRecarga($tabla, $datos, $valor);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "'.$mensaje.'",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "recargas";

									}
								})

					</script>';
			}
			else
			{
				echo'<script>

					swal({
						  type: "error",
						  title: "Ha ocurrido un error contacta con desarrollo",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "recargas";

									}
								})

					</script>';			    
			}
		}
	}

	/*=============================================
	APROBAR RETIRO
	=============================================*/

	static public function ctrAprobarRetiro(){

		if(isset($_POST["idRetiro"])){

			if(isset($_FILES["nuevaImagenRetiro"]["tmp_name"])){

				list($ancho, $alto) = getimagesize($_FILES["nuevaImagenRetiro"]["tmp_name"]);

				$nuevoAncho = 1285;
				$nuevoAlto = 637;

				/*=============================================
				CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
				=============================================*/

				$directorio = "vistas/img/retiros/".$_POST["codigo"];

				mkdir($directorio, 0755);

				/*=============================================
				DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
				=============================================*/

				if($_FILES["nuevaImagenRetiro"]["type"] == "image/jpeg"){

					/*=============================================
					GUARDAMOS LA IMAGEN EN EL DIRECTORIO
					=============================================*/

					$aleatorio = mt_rand(100,999);

					$ruta = "vistas/img/retiros/".$_POST["codigo"]."/".$aleatorio.".jpg";

					$origen = imagecreatefromjpeg($_FILES["nuevaImagenRetiro"]["tmp_name"]);						

					$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

					imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

					imagejpeg($destino, $ruta);

				}

				if($_FILES["nuevaImagenRetiro"]["type"] == "image/png"){

					/*=============================================
					GUARDAMOS LA IMAGEN EN EL DIRECTORIO
					=============================================*/

					$aleatorio = mt_rand(100,999);

					$ruta = "vistas/img/retiros/".$_POST["codigo"]."/".$aleatorio.".png";

					$origen = imagecreatefrompng($_FILES["nuevaImagenRetiro"]["tmp_name"]);						

					$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

					imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

					imagepng($destino, $ruta);

				}
			}

			$tabla ="retirar";

			$datos = array( 'id_retiro'      => $_POST["idRetiro"],
							'id_usuario'     => $_POST["idUsuario"],
							'monto_retiro'   => $_POST["montoRetiro"],
							'codigo'         => $_POST["codigo"],
							'imagen'         => $ruta,
							'num_referencia' => $_POST["referenciaRetiro"]);
		    $item = "id";
			
		    $valor = "aprobado";
		    $mensaje = "El retiro ha sido cargado y aprobado correctamente";

			$respuesta = ModeloRecarga::mdlAprobarRetiro($tabla, $datos, $valor);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "'.$mensaje.'",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "recargas";

									}
								})

					</script>';
			}
			else
			{
				echo'<script>

					swal({
						  type: "error",
						  title: "'.$respuesta.'",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "recargas";

									}
								})

					</script>';			    
			}
		}
	}	

	
}
