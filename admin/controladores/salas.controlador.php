<?php

class ControladorSalas{

	/*=============================================
	CREAR SALAS
	=============================================*/

	static public function ctrCrearSala(){

		if(isset($_POST["nuevaSala"])){

			$tabla  = "sala";
			$tabla2 = "premio";
			$estado = "inactiva";
			$item = null;			
	    	$valor = null;
	    	$orden = "id";			

			$salas = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);

			foreach ($salas as $key => $value) 
			{
				if (str_replace(" ", "", $value["nombre_sala"]) == str_replace(" ", "", strtolower($_POST["nuevaSala"]))) 
				{
					$existe = "si";
				}
			}

			if($existe != 'si')
			{
		   		/*=============================================
				VALIDAR IMAGEN
				=============================================*/

			   	$ruta = "vistas/img/sala/default/default.jpg";

			   	if(isset($_FILES["nuevaImagenSala"]["tmp_name"])){

					list($ancho, $alto) = getimagesize($_FILES["nuevaImagenSala"]["tmp_name"]);

					$nuevoAncho = 250;
					$nuevoAlto = 350;

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directorio = "vistas/img/sala/".str_replace(" ", "", $_POST["nuevaSala"]);

					mkdir($directorio, 0755);

					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

					if($_FILES["nuevaImagenSala"]["type"] == "image/jpeg"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/sala/".str_replace(" ", "", $_POST["nuevaSala"])."/".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($_FILES["nuevaImagenSala"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);

					}

					if($_FILES["nuevaImagenSala"]["type"] == "image/png"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/sala/".str_replace(" ", "", $_POST["nuevaSala"])."/".$aleatorio.".png";

						$origen = imagecreatefrompng($_FILES["nuevaImagenSala"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagepng($destino, $ruta);

					}
				}				


				$datos = array(	"nombre"      => strtolower($_POST["nuevaSala"]),
								"tipo_sala"   => $_POST["nuevoTipoSala"],
								"tipo_premio" => $_POST["nuevoTipoPremio"],
								"precio"      => $_POST["nuevoPrecioCartilla"],
								"imagen"      => $ruta,
								"estado"      => $estado);

				$respuesta = ModeloSalas::mdlIngresarSalas($tabla, $tabla2, $datos);

				$respuesta = explode(" ", $respuesta);

				if($respuesta[0] == "ok")
				{

					$tabla = "tmp_juego";
					$item  = 'nuevoJuego';
					$id_juego = mt_rand(10000, 99999);

					$called= [];
					$contador =0;

					for ($i=0; $i < 108 ; $i++) 
		        	{
		        		if(count($called) == 75)
		        		{}
		                else 
		                {
		                    $i = rand(1, 75);
		                    while(in_array($i, $called))
		                    {
		                        $i = rand(1, 75);
		                    }
		                    
		                    $called[] = $i;

		                    $datos = array("id_juego"    => $id_juego,
		                				   "numero"      => $i,
		                				   "id_sala"     => $respuesta[1],
		                			       "tiempo"      => 360,
		                			   	   "estado_juego" => 'esperando');

		                    $cambiarIdJuego = JuegoModelo::cantar($tabla, $item, $datos);

		                    if($cambiarIdJuego == 'ok')
		                    {
		                    	$contador++;
		                    }
		                }   
		            }
	                
	                if($contador == count($called))
	                {
	                	$item  = 'cambiar';

						$cambiarJuego = JuegoModelo::cantar($tabla, $item, $datos);

						if($cambiarJuego == 'ok')
						{
							echo'<script>

								swal({
									  type: "success",
									  title: "La sala ha sido creada correctamente",
									  showConfirmButton: true,
									  confirmButtonText: "Cerrar"
									  }).then(function(result){
												if (result.value) {

												window.location = "salas";

												}
											})

								</script>';
						}
	                }  
				}
				elseif($respuesta == 'errorPremio')
				{
					echo'<script>

						swal({
							  type: "error",
							  title: "La sala se creo pero ocurrio un error no sera posible registrar premios para esta sala, eliminela y intente nuevamente",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "salas";

										}
									})

						</script>';			
				}				
				else
				{
					echo'<script>

						swal({
							  type: "error",
							  title: "Ocurrio un error al crear la sala",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "salas";

										}
									})

						</script>';			
				}
			}
			else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El nombre de la sala ya esta en uso!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "salas";

							}
						})

			  	</script>';

			}	
	
		}
	}

	/*=============================================
	MOSTRAR SALAS
	=============================================*/

	static public function ctrMostrarSalas($item, $valor, $orden){

		$tabla = "sala";

		$respuesta = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);

		return $respuesta;
	}	

	/*=============================================
	EDITAR SALA
	=============================================*/

	static public function ctrEditarSala(){

		if(isset($_POST["editarSala"])){

			$tabla  = "sala";
			$item   = null;			
	    	$valor  = null;
	    	$orden  = "id";
	    	$contar = 0;

			$salas = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);

			foreach ($salas as $key => $value) 
			{
				if (str_replace(" ", "", $value["nombre_sala"]) == str_replace(" ", "", strtolower($_POST["editarSala"]))) 
				{
					$existe = "si";
					$contar++;
				}
			}

			if($existe != 'si' || $existe == 'si' && $contar < '2'){

		   		/*=============================================
				VALIDAR IMAGEN
				=============================================*/

			   	$ruta = $_POST["imagenActualSala"];

			   	if(isset($_FILES["editarImagenSala"]["tmp_name"]) && !empty($_FILES["editarImagenSala"]["tmp_name"])){

					list($ancho, $alto) = getimagesize($_FILES["editarImagenSala"]["tmp_name"]);

					$nuevoAncho = 250;
					$nuevoAlto = 350;

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directorio = "vistas/img/sala/".str_replace(" ", "", $_POST["editarSala"]);

					mkdir($directorio, 0755);

					/*=============================================
					PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD
					=============================================*/

					if(!empty($_POST["imagenActualSala"]) && $_POST["imagenActualSala"] != "vistas/img/sala/default/default.jpg"){

						//unlink($_POST["imagenActualSala"]);
						rmdir($_POST["imagenActualSala"]);

					}else{

						mkdir($directorio, 0755);	
					
					}

					
					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

					if($_FILES["editarImagenSala"]["type"] == "image/jpeg"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/sala/".str_replace(" ", "", $_POST["editarSala"])."/".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($_FILES["editarImagenSala"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);

					}

					if($_FILES["editarImagenSala"]["type"] == "image/png"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/sala/".str_replace(" ", "", $_POST["editarSala"])."/".$aleatorio.".png";

						$origen = imagecreatefrompng($_FILES["editarImagenSala"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagepng($destino, $ruta);

					}

				}				

				$datos = array(	"id"          => $_POST["idSala"], 
								"nombre"      => $_POST["editarSala"],
								"tipo_sala"   => $_POST["editarTipoSala"],
								"tipo_premio" => $_POST["editarTipoPremio"],
								"precio"      => $_POST["editarPrecioCartilla"],
								"imagen"      => $ruta);

				$respuesta = ModeloSalas::mdlEditarSala($tabla, $datos);

				if($respuesta == "ok")
				{

					echo'<script>

					swal({
						  type: "success",
						  title: "Sala editada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "salas";

									}
								})

					</script>';				

				}
				else
				{
					echo'<script>

						swal({
							  type: "error",
							  title: "¡Ocurrio un error al editar la sala intente de nuevo!",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
								if (result.value) {

								window.location = "salas";

								}
							})

				  	</script>';					
				}


			}
			else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El nombre de la sala ya esta en uso!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "salas";

							}
						})

			  	</script>';

			}

		}

	}	

	/*=============================================
	PREMIO SALA
	=============================================*/

	static public function ctrPremioSala(){

		if(isset($_POST['nuevoPremio']))
		{
			$tabla = "sala";
			$item  = "id";
			$valor = $_POST["idSalaPremio"];
			$orden = null;

			$sala = ControladorSalas::ctrMostrarSalas($item, $valor, $orden);

			if($sala["premio_sala"] == '0')
			{}
			elseif($sala["premio_sala"] == '1')
			{
				$tabla = "premio";
				$item  = "id_sala";
				$valor = $_POST['idSalaPremio'];
				$datos = array(	"monto_premio"       => number_format($_POST['nuevoPremio'],2),
								"imagen_premio"      => '',
								"descripcion_premio" => 'Premio definido por el administrador',
								"tipo_premio"        => $sala["premio_sala"]);

				$respuesta = ModeloSalas::mdlPremioSala($tabla, $item, $valor, $datos);

				if($respuesta == 'ok')
				{
					echo '<script>

						swal({
							  type: "success",
							  title: "Premio asignado correctamente",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "salas";

										}
									})

						</script>';
				}
				else
				{
					echo '<script>

						swal({
							  type: "error",
							  title: "Problemas al registrar el premio",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "salas";

										}
									})

						</script>';
				}				
			}
		}
		elseif(isset($_POST['nuevoPremio2']))
		{
			$tabla = "sala";
			$item  = "id";
			$valor = $_POST["idSalaPremio2"];
			$orden = null;

			$sala = ControladorSalas::ctrMostrarSalas($item, $valor, $orden);

			if($sala["premio_sala"] == '2')
			{

		   		/*=============================================
				VALIDAR IMAGEN
				=============================================*/

			   	$ruta = "vistas/img/premio/default.jpg";

			   	if(isset($_FILES["nuevaImagenPremio"]["tmp_name"])){

					list($ancho, $alto) = getimagesize($_FILES["nuevaImagenPremio"]["tmp_name"]);

					$nuevoAncho = 260;
					$nuevoAlto = 210;

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directorio = "vistas/img/premio/".str_replace(" ", "", $_POST["nuevoPremio2"]);

					mkdir($directorio, 0755);

					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

					if($_FILES["nuevaImagenPremio"]["type"] == "image/jpeg"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/premio/".str_replace(" ", "", $_POST["nuevoPremio2"])."/".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($_FILES["nuevaImagenPremio"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);

					}

					if($_FILES["nuevaImagenPremio"]["type"] == "image/png"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/premio/".str_replace(" ", "", $_POST["nuevoPremio2"])."/".$aleatorio.".png";

						$origen = imagecreatefrompng($_FILES["nuevaImagenPremio"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagepng($destino, $ruta);

					}
				}

				$time  = explode(":", $_POST["nuevaHoraPremio"]);

				if($time[0] > 12)
				{
					$time = 'pm';
				} 
				else
				{
					$time = 'am';
				}

				$fecha = explode("-", $_POST["nuevaFechaPremio"]);

				$fecha = $fecha[1]."/".$fecha[2]."/".$fecha[0];

				$hora  = explode(":", $_POST["nuevaHoraPremio"]);

				$hora  = ($hora[0]-12).":".$hora[1];

				$tabla = "premio";
				$item  = "id_sala";
				$valor = $_POST['idSalaPremio2'];
				$datos = array(	"monto_premio"       => '0.00',
								"imagen_premio"      => $ruta,
								"descripcion_premio" => $_POST["nuevoPremio2"],
								"tipo_premio"        => $sala["premio_sala"],
								"fecha_premio"       => $fecha." ".$hora." ".$time);

				$respuesta = ModeloSalas::mdlPremioSala($tabla, $item, $valor, $datos);

				if($respuesta == 'ok')
				{
					echo '<script>

						swal({
							  type: "success",
							  title: "Premio asignado correctamente",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "salas";

										}
									})

						</script>';
				}
				else
				{
					echo '<script>

						swal({
							  type: "error",
							  title: "Problemas al registrar el premio",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "salas";

										}
									})

						</script>';
				}	
			}						
		}
	}

	/*=============================================
	ACTIVAR DESACTIVAR SALA
	=============================================*/

	static public function ctrActivaSala(){

		if(isset($_GET["idSalaA"])){

			$tabla = "sala";
			$datos = $_GET["idSalaA"];
		    $item  = "id";
		    $orden = null;

    		$estado = ModeloSalas::mdlMostrarSalas($tabla, $item, $datos, $orden);
			
			if($estado['estado']== 'activa')
			{
			    $valor   = "inactiva";
			    $mensaje = "La sala ha sido inactivada correctamente";
			}
			else
			{
			    $valor = "activa";
			    $mensaje = "La sala ha sido activada correctamente";
			}

			$respuesta = ModeloSalas::mdlActivaSala($tabla, $datos, $valor);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "'.$mensaje.'",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "salas";

									}
								})

					</script>';
			}
			else
			{
				echo'<script>

					swal({
						  type: "error",
						  title: "Ha ocurrido un error contacta con desarrollo",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "salas";

									}
								})

					</script>';			    
			}
		}
		
	}

	/*=============================================
	MOSTRAR PREMIO
	=============================================*/

	static public function ctrMostrarPremio($item, $valor, $orden){

		$tabla = "premio";

		$respuesta = ModeloSalas::mdlMostrarSalas($tabla, $item, $valor, $orden);

		return $respuesta;
	}	

	/*=============================================
	BORRAR SALA
	=============================================*/
	static public function ctrEliminarSala(){

		if(isset($_GET["idSala"])){

			$tabla ="sala";
			$datos = $_GET["idSala"];

			if($_GET["imagen"] != "" && $_GET["imagen"] != "vistas/img/sala/default/default.jpg"){

				$imagen = explode("/", $_GET["imagen"]);
				$ruta   = $imagen[0]."/".$imagen[1]."/".$imagen[2]."/".$imagen[3];			
				unlink($_GET["imagen"]);
				rmdir($ruta);
			}


			$respuesta = ModeloSalas::mdlEliminarSala($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "La sala ha sido borrada correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "salas";

								}
							})

				</script>';

			}		
		}


	}

	/*=============================================
	CAMBIAR TIEMPO SALA
	=============================================*/

	static public function ctrCambiarTiempoSala(){

		if(isset($_POST["idSala"]))
		{
			$tabla    = "sala";
			$item     = "tiempo_juego_sala";
			$itemCond = "id";

			$meses =  array ( 	"Jan" =>  1,
								"Feb" =>  2, 
								"Mar" =>  3,
								"Apr" =>  4,
								"May" =>  5,
								"Jun" =>  6,
								"Jul" =>  7,
								"Aug" =>  8,
								"Sep" =>  9,
								"Oct" =>  10,
								"Nov" =>  11,
								"Dec" =>  12);

			$h =explode(" ", $_POST["tiempo"]);
			
			$mes = $meses[$h[1]];
			$dia = $h[2];
			$año = $h[3];

			$hora = explode(":", $h[4]);
			$minutos = $hora[1]+6;
			if($hora[0] > 12)
			{
				$hora = $hora[0]-12;
				$paam = 'PM';
			}
			elseif($hora[0] < 12)
			{
				$hora = $hora[0];
				$paam = 'AM';
			}
			elseif($hora[0] == 12)
			{
				$hora = $hora[0];
				$paam = 'PM';
			}

			if($minutos > 59)
			{
				$min = $minutos-59;
				$hora = $hora + 1;
				if($min <10)
				{
					$minutos = "0".$min;
				}
				else
				{
					$minutos = $min;
				}

				if($hora > 12)
				{
					$hora = $hora-12;
					$paam = 'PM';
				}
				if($hora == 12)
				{
					$hora = $hora;
					$paam = 'PM';
				}
			}

			$datos = array("item_valor" => $mes."/".$dia."/".$año." ".$hora.":".$minutos." ".$paam,
						   "item_cond"  => $_POST["idSala"]);

			$respuesta = ModeloSalas::mdlCambiarTiempoSala($tabla, $item, $itemCond, $datos);

			return $respuesta;
		}

	}	

	/*=============================================
	CAMBIAR JUEGO SALA
	=============================================*/

	static public function ctrCambiarJuegoSala(){

		if(isset($_POST["idSala"]))
		{
			$tabla    = "sala";
			$item     = null;
			$item2    = "juego_sala";
			$itemCond = "id";

			$datos = array("item_valor2" => $_POST["valor"],
						   "item_cond"   => $_POST["idSala"]);

			$respuesta = ModeloSalas::mdlCambiarJuegoSala($tabla, $item, $item2, $itemCond, $datos);

			return $respuesta;
		}

	}	

	/*=============================================
	MOSTRAR SUMA VENTAS
	=============================================*/

	static public function ctrMostrarSumaApuestas(){

		$tabla = "apuesta";

		$respuesta = ModeloSalas::mdlMostrarSumaApuesta($tabla);

		return $respuesta;

	}


}