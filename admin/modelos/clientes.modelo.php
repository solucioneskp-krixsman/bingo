<?php

require_once "conexion.php";

class ModeloClientes{

	/*=============================================
	CREAR CLIENTE
	=============================================*/

	static public function mdlMostrarGanadores($tabla, $item){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :item");

			$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	} 

	/*=============================================
	MOSTRAR CLIENTES
	=============================================*/

	static public function mdlMostrarClientes($tabla, $tabla2, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT usuario.id, usuario.usuario, usuario.rol, usuario.pais, usuario.identificacion, usuario.telefono, usuario.ciudad, usuario.estado, usuario.nombre, usuario.apellido, usuario.direccion, usuario.correo, cuenta.balance, cuenta.saldo, cuenta.retirar, cuenta.bitcoin  FROM $tabla INNER JOIN $tabla2 ON usuario.id = cuenta.id_usuario WHERE $item = :item");

			$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT usuario.id, usuario.usuario, usuario.rol, usuario.pais, usuario.ciudad, usuario.nombre, usuario.apellido, usuario.correo, cuenta.saldo, cuenta.monedas  FROM $tabla INNER JOIN $tabla2 ON usuario.id = cuenta.id_usuario");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR TRANSACCIONES
	=============================================*/

	static public function mdlMostrarTransacciones($tabla, $tabla2, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla INNER JOIN usuario ON $tabla.id_usuario = usuario.id  WHERE $tabla.$item = :item ORDER by $tabla.fecha_transaccion DESC");

			$stmt -> bindParam(":item", $valor, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_ASSOC);

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla INNER JOIN $tabla2 ON $tabla.id_usuario = $tabla2.id ORDER by $tabla.fecha_transaccion DESC");

			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_ASSOC);

		}

		$stmt -> close();

		$stmt = null;

	}	
 
	/*=============================================
	RECARGAR CLIENTE
	=============================================*/

	static public function mdlRecargarCliente($tabla, $datos){

        $stmt0 = Conexion::conectar()->prepare("SELECT * FROM $tabla where id_usuario = :id");

        $stmt0->bindParam(":id", $datos["id"], PDO::PARAM_INT);

	    $stmt0->execute();   

	    $actual = $stmt0 -> fetchAll(PDO::FETCH_ASSOC);

		$total = $actual[0]["saldo"]+$datos["monto"];

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET saldo = :monto WHERE id_usuario = :id");

		$stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);
		$stmt->bindParam(":monto", $total, PDO::PARAM_STR);

		if($stmt->execute()){
            
			$id_transaccion = mt_rand(10000, 99999);

			$stmt1 =$pdo->prepare("INSERT INTO transacciones(id_usuario, id_transaccion, tipo_transaccion, monto_transaccion, referencia_transaccion, estado_transaccion) VALUES (:id_usuario, :id_transaccion, :tipo_transaccion, :monto_transaccion, :referencia_transaccion, :estado_transaccion)");

			$stmt1->bindParam(":id_usuario", $datos['id_usuario'], PDO::PARAM_INT);
			$stmt1->bindParam(":id_transaccion", $id_transaccion, PDO::PARAM_INT);
			$stmt1->bindParam(":tipo_transaccion", $datos['tipo_transaccion'], PDO::PARAM_STR);
			$stmt1->bindParam(":monto_transaccion", $datos['monto_transaccion'], PDO::PARAM_STR);
			$stmt1->bindParam(":referencia_transaccion", $datos['referencia_transaccion'], PDO::PARAM_INT);
			$stmt1->bindParam(":estado_transaccion", $datos['estado_transaccion'], PDO::PARAM_STR);

            if($stmt1->execute())
            {
            	return "ok";
            }

		}else{

			return "error";
		
		}

		$stmt0->close();
		$stmt0 = null;
		$stmt->close();
		$stmt = null;

	}
	
	static public function mdlEditarSaldoCliente($tabla, $datos){

        $stmt0 = Conexion::conectar()->prepare("SELECT * FROM $tabla where id_usuario = :id");

        $stmt0->bindParam(":id", $datos["id"], PDO::PARAM_INT);

	    $stmt0->execute();   

	    $actual = $stmt0 -> fetchAll(PDO::FETCH_ASSOC);

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET saldo = :monto WHERE id_usuario = :id");

		$stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);
		$stmt->bindParam(":monto", $datos["monto"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt0->close();
		$stmt0 = null;
		$stmt->close();
		$stmt = null;

	}	

	/*=============================================
	ELIMINAR CLIENTE
	=============================================*/

	static public function mdlEliminarCliente($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR CLIENTE
	=============================================*/

	static public function mdlActualizarCliente($tabla, $item1, $valor1, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE id = :id");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":id", $valor, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

}