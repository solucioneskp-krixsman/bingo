<?php

require_once "conexion.php";

class ModeloSlider{

	/*=============================================
	CREAR PAIS
	=============================================*/

	static public function mdlIngresarSlider($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(titulo, texto, imagen, estado) VALUES (:titulo, :texto, :imagen, :estado)");

		$stmt->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
		$stmt->bindParam(":texto", $datos['texto'], PDO::PARAM_STR);
		$stmt->bindParam(":imagen", $datos['imagen'], PDO::PARAM_STR);
		$stmt->bindParam(":estado", $datos['estado'], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR SLIDER
	=============================================*/

	static public function mdlMostrarSlider($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :item");

			$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla order by id desc");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	EDITAR SLIDER
	=============================================*/

	static public function mdlEditarSlider($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET titulo = :titulo, texto = :texto, imagen = :imagen WHERE id = :id");

		$stmt ->bindParam(":id", $datos["id"], PDO::PARAM_INT);
		$stmt->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
		$stmt->bindParam(":texto", $datos['texto'], PDO::PARAM_STR);
		$stmt ->bindParam(":imagen", $datos['imagen'], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	ACTIVAR DESACTIVAR SLIDER
	=============================================*/

	static public function mdlActivaSlider($tabla, $datos, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = :estado WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);
		$stmt -> bindParam(":estado", $valor, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}
	
	/*=============================================
	BORRAR SLIDER
	=============================================*/

	static public function mdlBorrarSlider($tabla, $datos, $item){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}	

}

