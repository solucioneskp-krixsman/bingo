<?php

require_once "conexion.php";

class ModeloRecarga{

	/*=============================================
	MOSTRAR RECARGAS
	=============================================*/

	static public function mdlMostrarRecargas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT *, $tabla.id as idRecarga FROM $tabla INNER JOIN usuario on $tabla.id_usuario = usuario.id order by $tabla.estado_recarga desc");	

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_ASSOC);			
	}

	/*=============================================
	SUMAR RECARGAS
	=============================================*/

	static public function mdlSumarRecargas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT *, $tabla.id as idRecarga FROM $tabla INNER JOIN usuario on $tabla.id_usuario = usuario.id order by estado_recarga desc ");	

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_ASSOC);			
	}

	/*=============================================
	MOSTRAR RETIRO
	=============================================*/

	static public function mdlMostrarRetiro($tabla,$item,$valor){

		if($item != null)
		{
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ");	

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

		}			
		else
		{
			$stmt = Conexion::conectar()->prepare("SELECT *, $tabla.id as idRetiro FROM $tabla INNER JOIN usuario on $tabla.id_usuario = usuario.id order by $tabla.estado_retiro desc ");	

			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_ASSOC);
		}
	}

	/*=============================================
	SUMAR RETIRO
	=============================================*/

	static public function mdlSumarRetiro($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT *, $tabla.id as idRetiro FROM $tabla INNER JOIN usuario on $tabla.id_usuario = usuario.id order by estado_retiro desc ");	

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_ASSOC);			
	}

	/*=============================================
	APROBAR RECARGA
	=============================================*/

	static public function mdlAprobarRecarga($tabla, $datos, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado_recarga = :estado_recarga WHERE id = :id");

		$stmt -> bindParam(":id", $datos["id_recarga"], PDO::PARAM_INT);
		$stmt -> bindParam(":estado_recarga", $valor, PDO::PARAM_STR);

		if($stmt -> execute()){

			$estado_transaccion = 'aprobada';
			$datos['referencia_transaccion'] = date("Y").$datos['monto_recarga'];

			$stmt = Conexion::conectar()->prepare("UPDATE transacciones SET referencia_transaccion = :referencia_transaccion, estado_transaccion = :estado_transaccion WHERE id_usuario= :id_usuario AND id_transaccion = :id_transaccion");

			$stmt -> bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);
			$stmt -> bindParam(":id_transaccion", $datos["codigo"], PDO::PARAM_INT);
			$stmt -> bindParam(":estado_transaccion", $estado_transaccion, PDO::PARAM_STR);
			$stmt -> bindParam(":referencia_transaccion", $datos['referencia_transaccion'], PDO::PARAM_INT);

			if($stmt -> execute()){

				$stmt1 = Conexion::conectar()->prepare("SELECT * FROM cuenta where id_usuario = :id_usuario");

				$stmt1 -> bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);

	            $stmt1->execute();   

	            $saldoActual = $stmt1 -> fetch(PDO::FETCH_ASSOC);

	            $saldo = $saldoActual["saldo"] + $datos["monto_recarga"];	

	            $stmt = Conexion::conectar()->prepare("UPDATE cuenta set saldo = :saldo where id_usuario = :id_usuario");

	            $stmt -> bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);
	            $stmt -> bindParam(":saldo", $saldo, PDO::PARAM_STR);

				if($stmt -> execute()){

					return "ok";
				
				}
			
			}
		
		}
		else
		{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}
	
	/*=============================================
	APROBAR RETIRO
	=============================================*/

	static public function mdlAprobarRetiro($tabla, $datos, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado_retiro = :estado_retiro, numero_referencia_retiro = :numero_referencia_retiro, imagen_referencia = :imagen_referencia WHERE id = :id");

		$stmt -> bindParam(":id", $datos["id_retiro"], PDO::PARAM_INT);
		$stmt -> bindParam(":estado_retiro", $valor, PDO::PARAM_STR);
		$stmt -> bindParam(":imagen_referencia", $datos["imagen"], PDO::PARAM_STR);
		$stmt -> bindParam(":numero_referencia_retiro", $datos['num_referencia'], PDO::PARAM_INT);

		if($stmt -> execute()){

			$estado_transaccion = 'aprobado';
			$datos['referencia_transaccion'] = date("Y").$datos['monto_retiro'];
			$datos["imagen"] = "admin/".$datos["imagen"];

			$stmt = Conexion::conectar()->prepare("UPDATE transacciones SET referencia_transaccion = :referencia_transaccion, estado_transaccion = :estado_transaccion, imagen = :imagen WHERE id_usuario= :id_usuario AND id_transaccion = :id_transaccion");

			$stmt -> bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);
			$stmt -> bindParam(":id_transaccion", $datos["codigo"], PDO::PARAM_INT);
			$stmt -> bindParam(":estado_transaccion", $estado_transaccion, PDO::PARAM_STR);
			$stmt -> bindParam(":referencia_transaccion", $datos['referencia_transaccion'], PDO::PARAM_INT);
			$stmt -> bindParam(":imagen", $datos["imagen"], PDO::PARAM_STR);

			if($stmt -> execute()){

				return "ok";						
			}
			else
			{
				return "errorTransaccion";
			}
		
		}
		else
		{

			return "errorActualizar";	

		}

		$stmt -> close();

		$stmt = null;

	}	

}

