<?php

require_once "conexion.php";

class ModeloSalas{

	/*=============================================
	REGISTRO DE SALAS
	=============================================*/
	static public function mdlIngresarSalas($tabla, $tabla2, $datos){

		//Insertar sala
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre_sala, tipo_sala, premio_sala, imagen_sala, estado_sala, precio_cartilla_sala) VALUES (:nombre_sala, :tipo_sala, :premio_sala, :imagen_sala, :estado_sala, :precio_cartilla_sala)");

		$stmt->bindParam(":nombre_sala", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":tipo_sala", $datos["tipo_sala"], PDO::PARAM_INT);
		$stmt->bindParam(":premio_sala", $datos["tipo_premio"], PDO::PARAM_INT);
		$stmt->bindParam(":imagen_sala", $datos["imagen"], PDO::PARAM_STR);
		$stmt->bindParam(":estado_sala", $datos["estado"], PDO::PARAM_STR);
		$stmt->bindParam(":precio_cartilla_sala", $datos["precio"], PDO::PARAM_STR);

		if($stmt->execute())
		{
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY id DESC limit 1");

			$stmt->execute();	

			$idSala = $stmt -> fetchAll(PDO::FETCH_ASSOC);	

			if(isset($idSala) && !empty($idSala))
			{
				$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla2(id_sala, tipo_premio) VALUES (:id_sala, :tipo_premio)");

				$stmt->bindParam(":id_sala", $idSala[0]["id"], PDO::PARAM_INT);
				$stmt->bindParam(":tipo_premio", $idSala[0]["premio_sala"], PDO::PARAM_INT);

				if($stmt->execute())
				{
					return "ok ".$idSala[0]["id"];
				}
				else
				{
					return "errorPremio";
				}
			}
			else
			{
				return "errorPremio";
			}
			
		}
		else
		{
			return "error";
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR SALAS
	=============================================*/

	static public function mdlMostrarSalas($tabla, $item, $valor, $orden){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY id DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $orden DESC");

			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_ASSOC);

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	EDITAR SALA
	=============================================*/

	static public function mdlEditarSala($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre_sala = :nombre_sala, tipo_sala = :tipo_sala, premio_sala = :premio_sala, imagen_sala = :imagen_sala, precio_cartilla_sala = :precio_cartilla_sala WHERE id = :id");

		$stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);
		$stmt->bindParam(":nombre_sala", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":tipo_sala", $datos["tipo_sala"], PDO::PARAM_INT);
		$stmt->bindParam(":premio_sala", $datos["tipo_premio"], PDO::PARAM_INT);
		$stmt->bindParam(":imagen_sala", $datos["imagen"], PDO::PARAM_STR);	
		$stmt->bindParam(":precio_cartilla_sala", $datos["precio"], PDO::PARAM_STR);	

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	REGISTRO DE PREMIO SALA
	=============================================*/
	static public function mdlPremioSala($tabla, $item, $valor, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE premio SET imagen_premio = :imagen_premio, descripcion_premio = :descripcion_premio, monto_premio = :monto_premio, tipo_premio = :tipo_premio, fecha_premio = :fecha_premio WHERE $item = :$item");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_INT);
		$stmt -> bindParam(":imagen_premio", $datos["imagen_premio"], PDO::PARAM_STR);
		$stmt -> bindParam(":descripcion_premio", $datos["descripcion_premio"], PDO::PARAM_STR);
		$stmt -> bindParam(":monto_premio", $datos["monto_premio"], PDO::PARAM_STR);
		$stmt -> bindParam(":tipo_premio", $datos["tipo_premio"], PDO::PARAM_INT);
		$stmt -> bindParam(":fecha_premio", $datos["fecha_premio"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;
	}

	/*=============================================
	ACTIVAR DESACTIVAR SALA
	=============================================*/

	static public function mdlActivaSala($tabla, $datos, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado_sala = :estado WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);
		$stmt -> bindParam(":estado", $valor, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR PREMIO
	=============================================*/

	static public function mdlMostrarPremio($tabla, $item, $valor, $orden){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY id DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $orden DESC");

			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_ASSOC);

		}

		$stmt -> close();

		$stmt = null;

	}	

	/*=============================================
	BORRAR SALA
	=============================================*/

	static public function mdlEliminarSala($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}	

	/*=============================================
	CAMBIAR TIEMPO SALA
	=============================================*/

	static public function mdlCambiarTiempoSala($tabla, $item, $itemCond, $datos){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla set $item = :$item where $itemCond = :$itemCond");

            $stmt->bindParam(":".$item, $datos['item_valor'], PDO::PARAM_STR);
            $stmt->bindParam(":".$itemCond, $datos['item_cond'], PDO::PARAM_STR);

            if($stmt -> execute())
            {
            	return "ok";
            }
		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE estado = 'activa' ");

			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_ASSOC);

		}

		$stmt = null;

	}	

	/*=============================================
	CAMBIAR JUEGO SALA
	=============================================*/

	static public function mdlCambiarJuegoSala($tabla, $item, $item2, $itemCond, $datos){

		if($datos['item_valor2'] == 'esperando'){

			$id_juego = mt_rand(10000, 99999);
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla set $item = :$item, $item2 = :$item2, id_juego_sala = :id_juego_sala where $itemCond = :$itemCond");

            $stmt->bindParam(":".$item, $datos['item_valor'], PDO::PARAM_STR);
            $stmt->bindParam(":".$item2, $datos['item_valor2'], PDO::PARAM_STR);
            $stmt->bindParam(":id_juego_sala", $id_juego, PDO::PARAM_INT);
            $stmt->bindParam(":".$itemCond, $datos['item_cond'], PDO::PARAM_STR);

            if($stmt -> execute())
            {
            	return "ok";
            }
		}else{
			
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla set $item2 = :$item2 where $itemCond = :$itemCond");
            
            $stmt->bindParam(":".$item2, $datos['item_valor2'], PDO::PARAM_STR);            
            $stmt->bindParam(":".$itemCond, $datos['item_cond'], PDO::PARAM_STR);

            if($stmt -> execute())
            {
            	return "ok";
            }
		}

		$stmt = null;

	}	

	/*=============================================
	MOSTRAR TOTAL DE APUESTAS EN SALAS
	=============================================*/

	static public function mdlMostrarSumaApuesta($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT SUM($tabla.num_cartilla) as total, $tabla.precio_cartilla as precio, sala.nombre_sala as nombre FROM $tabla INNER JOIN sala ON $tabla.id_sala = sala.id GROUP BY $tabla.id_sala");

        $stmt -> execute();
        
        return $stmt -> fetchAll(PDO::FETCH_ASSOC);
	}	
}