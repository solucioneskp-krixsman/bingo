<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Ganadores
    
    </h1>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">


      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Usuario</th>
           <th>Sala</th>
           <th>Premio</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          $item = null;
          $valor = null;

          $ganadores = Estilo::mostrarGanadores();

          foreach ($ganadores as $key => $value) {
           
            echo ' <tr>

                    <td>'.($key+1).'</td>

                    <td>'.ucwords($value["usuario"]).'</td>
                    
                    <td class="text-uppercase">'.ucwords($value["nombre_sala"]).'</td>

                    <td class="text-uppercase">'.ucwords($value["premio"]).'</td>

                  </tr>';
          }

        ?>

        </tbody>

       </table>

      </div>

    </div>
  </section>
</div>

