<?php

if($_SESSION["perfil"] == "Vendedor"){

 



  echo '<script>



    window.location = "inicio";



  </script>';



  return;



}

 $baseUrl = Vista::baseUrl();



?>

<input type="hidden" id="baseUrl" value="<?php echo $baseUrl ?>">

<div class="content-wrapper">



  <section class="content-header">

    

    <h1>

      

      Administrar Salas

    

    </h1>



    <ol class="breadcrumb">

      

      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

      

      <li class="active">Administrar Salas</li>

    

    </ol>



  </section>



  <section class="content">



    <div class="box">



      <div class="box-header with-border">

  

        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarSala">

          

          Agregar Sala



        </button>



      </div>



      <div class="box-body">

        

       <table class="table table-bordered table-striped dt-responsive tablaSalas" width="100%">

         

        <thead>

         

         <tr>

           

           <th style="width:10px">#</th>

           <th>Nombre</th>

           <th>ImagenSala</th>

           <th>Tipo</th>

           <th>ImagenPremio</th>

           <th>Premio</th>

           <th>Fecha Juego</th>

           <th>Fecha Creacion</th>           

           <th>Estado</th>

           <th>Acciones</th>

           

         </tr> 



        </thead>      



       </table>



       <input type="hidden" value="<?php echo $_SESSION['perfil']; ?>" id="perfilOculto">



      </div>



    </div>

  </section>

</div>



<!--=====================================

MODAL AGREGAR Sala

======================================-->



<div id="modalAgregarSala" class="modal fade" role="dialog">

  

  <div class="modal-dialog">



    <div class="modal-content">



      <form role="form" method="post" enctype="multipart/form-data">



        <!--=====================================

        CABEZA DEL MODAL

        ======================================-->



        <div class="modal-header" style="background:#3c8dbc; color:white">



          <button type="button" class="close" data-dismiss="modal">&times;</button>



          <h4 class="modal-title">Agregar Sala</h4>



        </div>



        <!--=====================================

        CUERPO DEL MODAL

        ======================================-->



        <div class="modal-body">



          <div class="box-body">



            <!-- ENTRADA PARA NOMBRE DE SALA-->

            

            <div class="form-group">

              

              <div class="input-group">



                <span class="input-group-addon"><i class="fa fa-th"></i></span> 



                <input type="text" class="form-control input-lg" name="nuevaSala" placeholder="Ingresar el nombre de la sala" required>

                  

              </div>



            </div>  



            <!-- ENTRADA PARA SELECCIONAR TIPO DE SALA-->



            <div class="form-group">

              

              <div class="input-group">

              

                <span class="input-group-addon"><i class="fa fa-th"></i></span> 



                <select class="form-control input-lg" id="nuevoTipoSala" name="nuevoTipoSala" required>

                  

                  <option value="">Selecionar tipo de sala</option>

                  <option value="0">Normal</option>

                  <option value="1">Especial</option>

  

                </select>



              </div>



            </div>



            <!-- ENTRADA PARA SELECCIONAR TIPO DE PREMIO-->



            <div class="form-group">

              

              <div class="input-group" id="tipoPremioDiv">

              

                



              </div>



            </div>            

          

            <!-- ENTRADA PARA EL MONTO -->

            

            <div class="form-group">

              

              <div class="input-group">

              

                <span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



                <input type="text" class="form-control input-lg" id="nuevoPrecioCartilla" name="nuevoPrecioCartilla" placeholder="Ingresar el precio de la cartilla" required>

                



              </div>



            </div>



            <!-- ENTRADA PARA SUBIR FOTO -->



             <div class="form-group">

              

              <div class="panel">SUBIR IMAGEN</div>



              <input type="file" class="nuevaImagenSala" name="nuevaImagenSala">



              <p class="help-block">Peso máximo de la imagen 2MB</p>



              <img src="vistas/img/sala/default/default.jpg" class="img-thumbnail previsualizarSala" width="100px">



            </div>          



          </div>



        </div>

        <!--=====================================

        PIE DEL MODAL

        ======================================-->



        <div class="modal-footer">



          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>



          <button type="submit" class="btn btn-primary">Guardar Sala</button>



        </div>



      </form>



        <?php



          $crearSala = new ControladorSalas();

          $crearSala -> ctrCrearSala();



        ?>  



    </div>



  </div>



</div>



<!--=====================================

MODAL EDITAR SALA

======================================-->



<div id="modalEditarSala" class="modal fade" role="dialog">

  

  <div class="modal-dialog">



    <div class="modal-content">



      <form role="form" method="post"  enctype="multipart/form-data">



        <!--=====================================

        CABEZA DEL MODAL

        ======================================-->



        <div class="modal-header" style="background:#3c8dbc; color:white">



          <button type="button" class="close" data-dismiss="modal">&times;</button>



          <h4 class="modal-title">Editar Salar</h4>



        </div>



        <!--=====================================

        CUERPO DEL MODAL

        ======================================-->



        <div class="modal-body">



          <div class="box-body">





            <input type="hidden"  name="idSala" id="idSala" required>



            <!-- ENTRADA PARA NOMBRE DE SALA-->

            

            <div class="form-group">

              

              <div class="input-group">



                <span class="input-group-addon"><i class="fa fa-th"></i></span> 



                <input type="text" class="form-control input-lg" name="editarSala" id="editarSala" required>

                  

              </div>



            </div>  



            <!-- ENTRADA PARA SELECCIONAR TIPO DE SALA-->



            <div class="form-group">

              

              <div class="input-group">

              

                <span class="input-group-addon"><i class="fa fa-th"></i></span> 



                <select class="form-control input-lg" id="editarTipoSala" name="editarTipoSala" required>

                  

                  <option value="">Selecionar tipo de sala</option>

                  <option value="0">Normal</option>

                  <option value="1">Especial</option>

  

                </select>



              </div>



            </div>



            <!-- ENTRADA PARA SELECCIONAR TIPO DE PREMIO-->



            <div class="form-group">

              

              <div class="input-group" id="editartipoPremioDiv">

              



              </div>



            </div>     



            <!-- ENTRADA PARA EL MONTO -->

            

            <div class="form-group">

              

              <div class="input-group">

              

                <span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



                <input type="text" class="form-control input-lg" id="editarPrecioCartilla" name="editarPrecioCartilla" placeholder="Ingresar el precio de la cartilla" required>

                



              </div>



            </div>





            <!-- ENTRADA PARA SUBIR FOTO -->



             <div class="form-group">

              

              <div class="panel">SUBIR IMAGEN</div>



              <input type="file" class="nuevaImagenSala" name="editarImagenSala">



              <p class="help-block">Peso máximo de la imagen 2MB</p>



              <img src="vistas/img/sala/default/default.jpg" class="img-thumbnail previsualizarSala" width="100px">



              <input type="hidden" name="imagenActualSala" id="imagenActualSala">



            </div>

          </div>



        </div>



        <!--=====================================

        PIE DEL MODAL

        ======================================-->



        <div class="modal-footer">



          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>



          <button type="submit" class="btn btn-primary">Guardar cambios</button>



        </div>



      <?php



         $editarSala = new ControladorSalas();

         $editarSala -> ctrEditarSala();

        ?> 



      </form>



    </div>



  </div>

</div>



<!--=====================================

MODAL ASIGNAR PREMIO

======================================-->



<div id="modalPremioSala2" class="modal fade" role="dialog">

  

  <div class="modal-dialog">



    <div class="modal-content">



      <form role="form" method="post" enctype="multipart/form-data">



        <!--=====================================

        CABEZA DEL MODAL

        ======================================-->



        <div class="modal-header" style="background:#3c8dbc; color:white">



          <button type="button" class="close" data-dismiss="modal">&times;</button>



          <h4 class="modal-title">Agregar premio</h4>



        </div>



        <!--=====================================

        CUERPO DEL MODAL

        ======================================-->



        <div class="modal-body">



          <div class="box-body">



            <!-- ENTRADA PARA EL MONTO -->

            

            <div class="form-group">

              

              <div class="input-group">

              

                <span class="input-group-addon"><i class="fa fa-th"></i></span> 



                <input type="text" class="form-control input-lg" id="nuevoPremio2" name="nuevoPremio2" placeholder="Ingresar el nombre del premio" required>

                <input type="hidden" class="form-control input-lg" id="idSalaPremio2" name="idSalaPremio2" required>



              </div>



            </div>



            <!-- ENTRADA PARA LA FECHA -->

            

            <div class="form-group">

              

              <div class="input-group">

              

                <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 



                <input type="date" class="form-control input-lg" id="nuevaFechaPremio" name="nuevaFechaPremio" required>                



              </div>



            </div>



            <!-- ENTRADA PARA LA HORA -->

            

            <div class="form-group">

              

              <div class="input-group">

              

                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span> 



                <input type="time" class="form-control input-lg" id="nuevaHoraPremio" name="nuevaHoraPremio" required>                



              </div>



            </div>                        



            <!-- ENTRADA PARA SUBIR FOTO -->



             <div class="form-group">

              

              <div class="panel">SUBIR IMAGEN</div>



              <input type="file" class="nuevaImagenPremio" name="nuevaImagenPremio">



              <p class="help-block">Peso máximo de la imagen 2MB</p>



              <img src="vistas/img/premio/default.jpg" class="img-thumbnail previsualizarPremio" width="100px">



              <input type="hidden" name="imagenActualPremio" id="imagenActualPremio">



            </div>



          </div>

        </div>



        <!--=====================================

        PIE DEL MODAL

        ======================================-->



        <div class="modal-footer">



          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>



          <button type="submit" class="btn btn-primary">Asignar premio</button>



        </div>



      </form>



        <?php



          $premioSala = new ControladorSalas();

          $premioSala -> ctrPremioSala();



        ?>  



    </div>



  </div>



</div>



<!--=====================================

MODAL ASIGNAR PREMIO

======================================-->



<div id="modalPremioSala1" class="modal fade" role="dialog">

  

  <div class="modal-dialog">



    <div class="modal-content">



      <form role="form" method="post" enctype="multipart/form-data">



        <!--=====================================

        CABEZA DEL MODAL

        ======================================-->



        <div class="modal-header" style="background:#3c8dbc; color:white">



          <button type="button" class="close" data-dismiss="modal">&times;</button>



          <h4 class="modal-title">Agregar premio</h4>



        </div>



        <!--=====================================

        CUERPO DEL MODAL

        ======================================-->



        <div class="modal-body">



          <div class="box-body">



            <!-- ENTRADA PARA EL MONTO -->

            

            <div class="form-group">

              

              <div class="input-group">

              

                <span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



                <input type="text" class="form-control input-lg" id="nuevoPremio" name="nuevoPremio" placeholder="Ingresar el monto" required>

                <input type="hidden" class="form-control input-lg" id="idSalaPremio1" name="idSalaPremio" required>



              </div>



            </div>

          </div>

        </div>



        <!--=====================================

        PIE DEL MODAL

        ======================================-->



        <div class="modal-footer">



          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>



          <button type="submit" class="btn btn-primary">Asignar premio</button>



        </div>



      </form>



        <?php



          $premioSala = new ControladorSalas();

          $premioSala -> ctrPremioSala();



        ?>  



    </div>



  </div>



</div>



<?php 

  $activarSala = new ControladorSalas();

  $activarSala -> ctrActivaSala();



  $cerrarCartilla = new ControladorSalas();

  $cerrarCartilla -> ctrEliminarSala();

?>      







