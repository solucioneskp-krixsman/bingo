<?php

if($_SESSION["perfil"] == "Vendedor"){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar Recargas  Y Retiros
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar Recargas Y Retiros</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
       <button disabled="" class="btn btn-primary">
          
          Recargas

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablasPendiente tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Usuario</th>
           <th>Fecha</th>
           <th>Titular Cuenta</th>
           <th>Identificacion</th>
           <th>Banco de recarga</th>
           <th>Monto</th>
           <th>Número de referencia</th>
           <th>Estado</th>
           <th>Accion</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          $item = null;
          $valor = null;

          $recargas = ControladorRecarga::ctrMostrarRecargas();

          foreach ($recargas as $key => $value) {
           
            if($value["estado_recarga"] == 'aprobada')
            {
                $valor    = "Aprobada";
                $color    = "btn-info";
                $icon     = "fa fa-check-circle" ;
                $disabled = "disabled";
            }
            else
            {
                $valor = "Aprobar";
                $color = "btn-success";
                $icon  = "fa fa-check" ;                              
                $disabled = "";
            }
           
            echo ' <tr>

                <td>'.($key+1).'</td>

                <td class="text-uppercase">'.ucwords($value["usuario"]).'</td>

                <td class="text-uppercase">'.$value["fecha_recarga"].'</td>

                <td class="text-uppercase">'.$value["titular_recarga"].'</td>

                <td class="text-uppercase">'.$value["cedula_recarga"].'</td>

                <td class="text-uppercase">'.$value["banco_recarga"].'</td>

                <td class="text-uppercase">'.$value["monto_recarga"].'</td>

                <td class="text-uppercase">'.$value["numero_referencia_recarga"].'</td>

                <td class="text-uppercase">'.ucwords($value["estado_recarga"]).'</td>

                <td>

                  <div class="btn-group">
                      
                    <button '.$disabled.' title="'.$valor.' Recarga" class="btn '.$color.' btnAprobarRecarga" montoRecarga="'.$value["monto_recarga"].'" idRecarga="'.$value["idRecarga"].'" idUsuario="'.$value["id_usuario"].'" codigo="'.$value["id_transaccion"].'"><i class="'.$icon.'"></i></button>';

                    if($_SESSION["perfil"] == "Administrador"){

                      /*echo '<!--<button class="btn btn-danger btnEliminarRecarga" idPais="'.$value["idRecarga"].'" codigo="'.$value["pais"].'" imagen="'.$value["imagen"].'"><i class="fa fa-times"></i></button>-->';*/

                    }

                  echo '</div>  

                </td>

              </tr>';              
        }

        ?>

        </tbody>

       </table>

      </div>

    </div>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
       <button disabled="" class="btn btn-primary">
          
          Retiros

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablaRetiro tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Usuario</th>
           <th>Fecha de solicitud</th>          
           <th>Monto</th>
           <th>Fecha aprobacion</th>
           <th>Número de referencia</th>
           <th>Imagen de Referencia</th>
           <th>Estado</th>
           <th>Accion</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          $item = null;
          $valor = null;

          $retiros = ControladorRecarga::ctrMostrarRetiro($item,$valor);

          foreach ($retiros as $key => $value) {
           
            if($value["estado_retiro"] == 'aprobado')
            {
                $valor    = "Aprobado";
                $color    = "btn-info";
                $icon     = "fa fa-check-circle" ;
                $fecha    = $value["fecha_aprobacion"];
                $numeroR  = $value["numero_referencia_retiro"];
                $imagen   = '<img src="'.$value["imagen_referencia"].'" width="150px">';
                $disabled = "disabled";
                $modal    = 'data-toggle="modal" data-target="#modalImagen" id="imagenModal" url="'.$value["imagen_referencia"].'"';              
            }
            else
            {
                $valor    = "Cargar";
                $color    = "btn-success";
                $icon     = "fa fa-check" ;
                $fecha    = 'No ha sido aprobado';  
                $numeroR  = 'No ha sido aprobado';
                $imagen   = 'No ha sido aprobado';
                $disabled = ""; 
                $modal    = '';                          
            }
            if($value["monto_retiro"] == 0 && !empty($value["premio_retiro"]))
            {
              $monto = ucwords($value["premio_retiro"]);
            }
            else
            {
              $monto = $value["monto_retiro"];
            }
           
            echo ' <tr>

                <td>'.($key+1).'</td>

                <td class="text-uppercase">'.ucwords($value["usuario"]).'</td>

                <td class="text-uppercase">'.$value["fecha_retiro"].'</td>
             
                <td class="text-uppercase">'.$monto.'</td>

                <td class="text-uppercase">'.$fecha.'</td>

                <td class="text-uppercase">'.$numeroR.'</td>

                <td class="text-uppercase" '.$modal.'>'.$imagen.'</td>

                <td class="text-uppercase">'.ucwords($value["estado_retiro"]).'</td>

                <td>

                  <div class="btn-group">
                      
                    <button '.$disabled.' title="'.$valor.' Retiro" class="btn '.$color.' btnAprobarRetiro" idRetiro="'.$value["idRetiro"].'" data-toggle="modal" data-target="#modalAprobarRetiro"><i class="'.$icon.'"></i></button>';

                    if($_SESSION["perfil"] == "Administrador"){

                      /*echo '<!--<button class="btn btn-danger btnEliminarRecarga" idPais="'.$value["idRecarga"].'" codigo="'.$value["pais"].'" imagen="'.$value["imagen"].'"><i class="fa fa-times"></i></button>-->';*/

                    }

                  echo '</div>  

                </td>

              </tr>';              
        }


        ?>

        </tbody>

       </table>

      </div>

    </div>

  </section>



</div>

<!--=====================================
MODAL ASIGNAR PREMIO
======================================-->

<div id="modalAprobarRetiro" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" id="retiroAprobar" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar referencia retiro</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL MONTO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-th"></i></span> 

                <input type="text" class="form-control input-lg" id="referenciaRetiro" name="referenciaRetiro" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" placeholder="Ingresar el numero de referencia" required>                

              </div>

            </div>                       

            <!-- ENTRADA PARA SUBIR FOTO -->

             <div class="form-group">
              
              <div class="panel">SUBIR IMAGEN DE REFERENCIA DE LA TRANSFERENCIA</div>

              <input type="file" required="" class="nuevaImagenRetiro" name="nuevaImagenRetiro">

              <p class="help-block">Peso máximo de la imagen 2MB</p>

              <img src="vistas/img/premio/default.jpg" class="img-thumbnail previsualizarRetiro" width="100px">

              <input type="hidden" name="idRetiro" id="idRetiro">
              <input type="hidden" name="idUsuario" id="idUsuario">
              <input type="hidden" name="montoRetiro" id="montoRetiro">
              <input type="hidden" name="codigo" id="codigo">

            </div>

          </div>
        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit"  class="btn btn-primary btnAprobarRetiroGuardar">Aprobar</button>

        </div>

      </form>

        <?php

          $retiro = new ControladorRecarga();
          $retiro -> ctrAprobarRetiro();

        ?>  

    </div>

  </div>

</div>

<!--=====================================
MODAL IMAGEN REFERENCIA
======================================-->

<div id="modalImagen" class="modal fade" role="dialog">
  
  <div class="modal-dialog modal-lg">

    <div class="modal-content">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Imagen referencia</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA SUBIR FOTO -->

             <div class="form-group">
              
              <div class="panel">IMAGEN DE REFERENCIA DE LA TRANSFERENCIA</div>

              <img src="vistas/img/premio/default.jpg" class="img-thumbnail previsualizarReferencia" style="width: 100%;">

            </div>

          </div>
        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

        </div> 

    </div>

  </div>

</div>

<?php

  $borrarPais = new ControladorRecarga();
  $borrarPais -> ctrAprobarRecarga();

?>


