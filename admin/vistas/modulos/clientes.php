<?php

if($_SESSION["perfil"] == "Especial"){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar clientes
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar clientes</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
       <!-- <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCliente">
          
          Agregar cliente

        </button>-->

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Usuario</th>
           <th>Saldo</th>
           <th>Monedas</th>
           <th>Accion</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          $item = null;
          $valor = null;

          $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);

          foreach ($clientes as $key => $value) {
            

            echo '<tr>

                    <td>'.($key+1).'</td>

                    <td>'.ucwords($value["usuario"]).'</td>

                    <td>'.$value["saldo"].'</td>

                    <td>'.$value["monedas"].'</td>            

                    <td>

                    <!--  <div class="btn-group">
                          
                        <button class="btn btn-warning btnEditarCliente" data-toggle="modal" data-target="#modalAgregarSaldo" idCliente="'.$value["id"].'"><i class="fa fa-pencil"></i></button>-->';

                      if($_SESSION["perfil"] == "Administrador"){

                          echo '<button title="Eliminar Cliente"  class="btn btn-danger btnEliminarCliente" idCliente="'.$value["id"].'"><i class="fa fa-times"></i></button>';

                      }

                      echo '</div>  

                    </td>

                  </tr>';
          
            }

        ?>
   
        </tbody>

       </table>

      </div>

    </div>

  </section>

  <section class="content-header">
    
    <h1>
      
      Historial Transacciones
    
    </h1>

  </section>
  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
       <!-- <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCliente">
          
          Agregar cliente

        </button>-->

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Tipo</th>
           <th>Transaccion</th>           
           <th>Usuario</th>
           <th>Monto o Premio</th>
           <th>Referencia</th>
           <th>Estado</th>
           <th>Fecha</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          $item = null;
          $valor = null;

          $clientes = ControladorClientes::ctrMostrarTransacciones($item, $valor);

          foreach ($clientes as $key => $value) {
            

            echo '<tr>

                    <td>'.($key+1).'</td>

                    <td>'.ucwords($value["tipo_transaccion"]).'</td>

                    <td>'.$value["id_transaccion"].'</td>                    

                    <td>'.ucwords($value["usuario"]).'</td>

                    <td>'.$value["monto_transaccion"].'</td>

                    <td>'.$value["referencia_transaccion"].'</td>            

                    <td>'.$value["estado_transaccion"].'</td>

                    <td>'.$value["fecha_transaccion"].'</td>

                  </tr>';
          
            }

        ?>
   
        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<?php

  $eliminarCliente = new ControladorClientes();
  $eliminarCliente -> ctrEliminarCliente();

?>

