<aside class="main-sidebar">

	 <section class="sidebar">

		<ul class="sidebar-menu">

		<?php

		if($_SESSION["perfil"] == "Administrador"){

			echo '<li class="active">

				<a href="inicio">

					<i class="fa fa-home"></i>
					<span>Inicio</span>

				</a>

			</li>

			<li>

				<a href="usuarios">

					<i class="fa fa-user"></i>
					<span>Usuarios</span>

				</a>

			</li>';

		}

		if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Especial"){

			echo '<li>

				<a href="recargas">

					<i class="fa fa-dollar"></i>
					<span>Recargas y Retiros</span>

				</a>

			</li>

			<li>

				<a href="salas">

					<i class="fa fa-calendar"></i>
					<span>Salas</span>

				</a>

			</li>

			<!--<li>

				<a href="slider">

					<i class="fa fa-file-image-o"></i>
					<span>Slider</span>

				</a>

			</li>-->

			<li>

				<a href="ganadores">

					<i class="fa fa-users"></i>
					<span>Ganadores</span>

				</a>

			</li>
			
			<li>

				<a href="parametros">

					<i class="fa fa-cogs"></i>
					<span>Parametros</span>

				</a>

			</li>';

		}

		if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Vendedor"){

			echo '<li>

				<a href="clientes">

					<i class="fa fa-user-circle"></i>
					<span>Clientes</span>

				</a>

			</li>
			
			<!--<li>

				<a href="distribuidor">

					<i class="fa fa-users"></i>
					<span>Distribuidores</span>

				</a>

			</li>-->';

		}

		?>

		</ul>

	 </section>

</aside>