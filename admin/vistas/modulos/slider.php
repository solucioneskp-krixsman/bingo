<?php

if($_SESSION["perfil"] == "Vendedor"){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar banner
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar banner</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarSlider">
          
          Agregar banner

        </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablaSlider" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Imagen</th>
           <th>Estado</th>
           <th>Acciones</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          $item = null;
          $valor = null;

          $slider = ControladorSlider::ctrMostrarSlider($item, $valor);

          foreach ($slider as $key => $value) {
           
            echo ' <tr>

                    <td>'.($key+1).'</td>

                    <td><img src="'.$value["imagen"].'" width="60px"></td>
                    
                    <td class="text-uppercase">'.ucwords($value["estado"]).'</td>

                    <td>

                      <div class="btn-group">
                          
                        <button title="Editar Banner" class="btn btn-warning btnEditarSlider" idSlider="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarSlider"><i class="fa fa-pencil"></i></button>';

                        if($_SESSION["perfil"] == "Administrador"){
                          if($value["estado"] == 'activa')
                          {
                              $valor = "Inactivar";
                              $color = "btn-danger";
                              $icon  = "fa fa-times" ;
                          }
                          else
                          {
                              $valor = "Activar";
                              $color = "btn-success";
                              $icon  = "fa fa-check" ;                              
                          }
                          echo '<button title="'.$valor.' Banner" class="btn '.$color.' btnActivaSlider" idSlider="'.$value["id"].'" codigo="'.$value["estado"].'" imagen="'.$value["imagen"].'"><i class="'.$icon.'"></i></button><button title="Eliminar Banner" class="btn btn-danger btnEliminarSlider" idSlider="'.$value["id"].'" codigo="'.$value["estado"].'" imagen="'.$value["imagen"].'"><i class="fa fa-times"></i></button>';

                        }

                      echo '</div>  

                    </td>

                  </tr>';
          }

        ?>

        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL AGREGAR SLIDER
======================================-->

<div id="modalAgregarSlider" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post"  enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar banner</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">
            <input type="hidden" name="nuevoSlider" value="<?php if(isset($slider[0]) && !empty($slider[0])){ echo $slider[0]['id']+1;}else{echo 1;} ?>">
          <div class="box-body">

            <!-- ENTRADA PARA TITULO BANNER-->
            
            <div class="form-group">
              
              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span> 
                <label class="text-danger">(Para resaltar un texto coloquelo dentro de los signos % @ Ejemplo Hola %Texto resaltado@)</label>
                <input type="text" class="form-control input-lg" name="nuevoTitulo" placeholder="Ingrese titulo %texto resaltado@" required>
                  
              </div>

            </div> 


            <!-- ENTRADA PARA TEXTO BANNER-->
            
            <div class="form-group">
              
              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span>                                 
                <textarea class="form-control input-lg" name="nuevoTexto"  placeholder="Ingrese descripcion" required=""></textarea>
                  
              </div>

            </div> 

            <!-- ENTRADA PARA SUBIR FOTO -->

             <div class="form-group">
              
              <div class="panel">SUBIR IMAGEN</div>

              <input type="file" class="nuevaImagenSlider" name="nuevaImagenSlider">

              <p class="help-block">Peso máximo de la imagen 2MB</p>

              <img src="vistas/img/slider/default.jpg" class="img-thumbnail previsualizarNuevo" width="100px">

            </div>
  
          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar banner</button>

        </div>

        <?php

          $crearSlider = new ControladorSlider();
          $crearSlider -> ctrCrearSlider();

        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR SLIDER
======================================-->

<div id="modalEditarSlider" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post"  enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar banner</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">


            <input type="hidden"  name="idSlider" id="idSlider" required>

            <!-- ENTRADA PARA TITULO BANNER-->
            
            <div class="form-group">
              
              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span> 
                <label class="text-danger">(Para resaltar un texto coloquelo dentro de los signos % @. Ejemplo Hola %Texto resaltado@)</label>
                <input type="text" class="form-control input-lg" name="editarTitulo" id="editarTitulo" placeholder="Ingrese titulo %texto resaltado%" required>
                  
              </div>

            </div> 


            <!-- ENTRADA PARA TEXTO BANNER-->
            
            <div class="form-group">
              
              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span>                                 
                <textarea class="form-control input-lg" name="editarTexto"  id="editarTexto" placeholder="Ingrese descripcion" required=""></textarea>
                  
              </div>

            </div> 

            <!-- ENTRADA PARA SUBIR FOTO -->

             <div class="form-group">
              
              <div class="panel">SUBIR IMAGEN</div>

              <input type="file" class="nuevaImagenEditar" name="editarImagenSlider">

              <p class="help-block">Peso máximo de la imagen 2MB</p>

              <img src="vistas/img/slider/default.jpg" class="img-thumbnail previsualizarEditar" width="100px">

              <input type="hidden" name="imagenActual" id="imagenActual">

            </div>
          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar cambios</button>

        </div>

      <?php

          $editarSlider = new ControladorSlider();
          $editarSlider -> ctrEditarSlider();

        ?> 

      </form>

    </div>

  </div>

</div>

<?php

  $activarSlider = new ControladorSlider();
  $activarSlider -> ctrActivaSlider();

  $borrarSlider = new ControladorSlider();
  $borrarSlider -> ctrBorrarSlider();

?>


