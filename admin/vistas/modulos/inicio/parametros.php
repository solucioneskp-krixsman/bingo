
  <form action="#" id="formDatosUsuario" method="post" enctype="multipart/form-data">
    <section class="content-header">
      
      <h1>
        
        Datos Banco
      
      </h1>

    </section>

    <section class="content">

      <div class="box">

        <div class="box-header with-border">
    
        <button type="submit" class="btn btn-primary">
            
            Guardar Cambios Banco

          </button>
          <label class="btn btn-success">Cambiar Imagen Banco<input class="form-control nuevaFotoB" type="file" id="editarImagenBanco" name="editarImagenBanco" class="nuevaImagenPerfil" style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;color: white;"></label>

        </div>

        <div class="box-body">
          <input required="" type="hidden" value="cambio" name="7">
        <table class="table table-bordered table-striped dt-responsive tablasPendiente tablas" width="100%">
          
          <thead>
          
          <tr>
            
            <th style="width:10px">#</th>
            <th>Imagen Banco</th>
            <th>Titular cuenta</th>           
            <th>Identificacion</th>
            <th>Correo</th>
            <th>Banco</th>
            <th>Tipo de Cuenta</th>
            <th>Número de Cuenta</th>
            <th>Telefono</th>

          </tr> 

          </thead>

          <tbody>

          <?php

            $item = null;
            $valor = null;

            $parametros = Estilo::mostrarParametros();

              echo ' <tr>

                  <td>1</td>

                  <td class="text-uppercase"><input type="hidden" name="imagenBancoActual" value="'.$parametros["img_banco"].'"><img class="b" src="'.$parametros["img_banco"].'" width="100px"></td>

                  <td class="text-uppercase"><input required="" class="form-control" type="text" name="editarTitular" value="'.ucwords($parametros["titular_parametros"]).'"></td>

                  <td class="text-uppercase"><input required="" class="form-control" type="text" name="editarCedula" value="'.$parametros["cedula_parametros"].'"></td>

                  <td class="text-uppercase"><input required="" class="form-control" type="text" name="editarCorreo" value="'.$parametros["correo_parametros"].'"></td>

                  <td class="text-uppercase"><input required="" class="form-control" type="text" name="editarBanco" value="'.$parametros["banco_parametros"].'"></td>

                  <td class="text-uppercase"><input required="" class="form-control" type="text" name="editarTipo" value="'.$parametros["tipo_cuenta_parametros"].'"></td>

                  <td class="text-uppercase"><input required="" class="form-control" type="text" name="editarNumero" value="'.$parametros["numero_cuenta_parametros"].'"></td>

                  <td class="text-uppercase"><input required="" class="form-control" type="text" name="editarTelefono" value="'.$parametros["telefono_parametros"].'"></td>

                </tr>';              

          ?>

          </tbody>

        </table>

        </div>

      </div>

    </section>
  </form>


