<?php

$item = null;
$valor = null;
$orden = "id";

$apuestas = ControladorSalas::ctrMostrarSumaApuestas();

$recargas = ControladorRecarga::ctrSumarRecargas();

$retiros = ControladorRecarga::ctrSumarRetiro();

$clientes = ControladorClientes::ctrMostrarClientes($item, $valor);
$totalClientes = count($clientes);

$ganadores = ControladorClientes::mostrarGanadores();

?>

<div class="row">

  <div class="col-lg-3 col-xs-6">

    <div class="small-box bg-green">
      
      <div class="inner">
      
        <h3><?php echo number_format($totalClientes); ?></h3>

        <p>Usuarios</p>
    
      </div>
      
      <div class="icon">
      
        <i class="ion ion-person-add"></i>
      
      </div>
      
      <a href="clientes" class="small-box-footer">

        Más info <i class="fa fa-arrow-circle-right"></i>

      </a>

    </div>

  </div>

  <div class="col-lg-3 col-xs-6">

    <div class="small-box bg-yellow">
    
      <div class="inner">
      
        <h3><?php echo number_format(count($recargas)); ?></h3>

        <p>Recargas Pendientes</p>
      
      </div>
      
      <div class="icon">
        
        <i class="ion ion-social-usd"></i>
      
      </div>
      
      <a href="recargas" class="small-box-footer">
        
        Más info <i class="fa fa-arrow-circle-right"></i>
      
      </a>

    </div>

  </div>

  <div class="col-lg-3 col-xs-6">

    <div class="small-box bg-red">
    
      <div class="inner">
      
        <h3><?php echo number_format(count($retiros)); ?></h3>

        <p>Retiros Pendientes</p>
      
      </div>
      
      <div class="icon">
        
        <i class="ion ion-social-usd"></i>
      
      </div>
      
      <a href="recargas" class="small-box-footer">
        
        Más info <i class="fa fa-arrow-circle-right"></i>
      
      </a>

    </div>

  </div>

  <div class="col-lg-3 col-xs-6">

    <div class="small-box bg-blue">
    
      <div class="inner">
      
        <h3><?php echo number_format(count($ganadores)); ?></h3>

        <p>Ganadores</p>
      
      </div>
      
      <div class="icon">
        
        <i class="ion ion-social-usd"></i>
      
      </div>
      
      <a href="ganadores" class="small-box-footer">
        
        Más info <i class="fa fa-arrow-circle-right"></i>
      
      </a>

    </div>

  </div>

<?php 
  foreach ($apuestas as $key => $value) 
  {
    echo '<div class="col-lg-3 col-xs-6">

            <div class="small-box bg-aqua">
              
              <div class="inner">
                
                <h3>$ '.number_format($value["total"]*$value["precio"],2).'</h3>

                <p>'.ucwords($value["nombre"]).'</p>
              
              </div>
              
              <div class="icon">
                
                <i class="ion ion-social-usd"></i>
              
              </div>
              
              <a href="salas" class="small-box-footer">
                
                Más info <i class="fa fa-arrow-circle-right"></i>
              
              </a>

            </div>

          </div>';
  }
?>

 <!-- <div class="col-lg-3 col-xs-6">

    <div class="small-box bg-green">
      
      <div class="inner">
      
        <h3><?php echo number_format(count($ga)); ?></h3>

        <p>Categorías</p>
      
      </div>
      
      <div class="icon">
      
        <i class="ion ion-clipboard"></i>
      
      </div>
      
      <a href="categorias" class="small-box-footer">
        
        Más info <i class="fa fa-arrow-circle-right"></i>
      
      </a>

    </div>

  </div>-->
</div>
