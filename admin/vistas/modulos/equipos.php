<div id="equipos" class="equipos">
	<!-- ENTRADA PARA EL EQUIPO LOCAL-->

	<div class="form-group">
	  <label>(Tildea para eliminar)<input type="checkbox" name="item_equipos[]" /></label>  
	  <div class="input-group">
	  
	    <span class="input-group-addon"><i class="fa fa-futbol-o"></i></span> 

	    <input type="text" class="form-control input-lg" id="nuevoEquipoLocal" name="nuevoEquipoLocal[]" placeholder="Ingresar equipo local" required>

	  </div>

	</div>

	<!-- ENTRADA PARA EL EQUIPO VISITANTE -->

	 <div class="form-group">
	  
	  <div class="input-group">
	  
	    <span class="input-group-addon"><i class="fa fa-futbol-o"></i></span> 

	    <input type="text" class="form-control input-lg" id="nuevoEquipoVisitante" name="nuevoEquipoVisitante[]" placeholder="Ingresar equipo visitante" required>

	  </div>

	</div>
</div>	