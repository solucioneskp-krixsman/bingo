<?php

if($_SESSION["perfil"] == "Especial"){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar clientes
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar clientes</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
       <!-- <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCliente">
          
          Agregar cliente

        </button>-->

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablaDistribuidor tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Usuario</th>
           <th>Nombre y Apellido</th>
           <th>País</th>
           <th>Ciudad</th>
           <th>Identificacion</th>
           <th>Dirección</th>
           <th>Estado</th>
           <th>Acciones</th>
           <th>Telefono</th>
           <th>Correo</th> 

         </tr> 

        </thead>

        <tbody>

        <?php

          $item = null;
          $valor = null;

          $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);

          foreach ($clientes as $key => $value) 
          {
            
            if($value["rol"] == 'distribuidor')
            {
                echo '<tr>
    
                        <td>'.($key+1).'</td>
    
                        <td>'.ucwords($value["usuario"]).'</td>
                        
                        <td>'.ucwords($value["nombre"]).' '.ucwords($value["apellido"]).'</td>
                        
                        <td>'.ucwords($value["pais"]).'</td>
                        
                        <td>'.ucwords($value["ciudad"]).'</td>
    
                        <td>'.$value["identificacion"].'</td>
    
                        <td>'.ucwords($value["direccion"]).'</td>
                        
                        <td>'.ucwords($value["estado"]).'</td>
                        
                        <td>
    
                        <!--  <div class="btn-group">
                              
                            <button class="btn btn-warning btnEditarCliente" data-toggle="modal" data-target="#modalAgregarSaldo" idCliente="'.$value["id"].'"><i class="fa fa-pencil"></i></button>-->';
    
                          if($_SESSION["perfil"] == "Administrador"){
                        if($value["estado"] == 'activo')
                          {
                              $valor = "Inactivar";
                              $color = "btn-danger";
                              $icon  = "fa fa-times" ;
                          }
                          else
                          {
                              $valor = "Activar";
                              $color = "btn-success";
                              $icon  = "fa fa-check" ;                              
                          }
                          echo '<button title="'.$valor.' Distribuidor" class="btn '.$color.' btnActivaDistribuidor" idDistribuidor="'.$value["id"].'" codigo="'.$value["estado"].'"><i class="'.$icon.'"></i></button>';                              
    
                             
    
                          }
    
                          echo '</div>  
    
                        </td>                        
    
                        <td>'.$value["telefono"].'</td>
    
                        <td>'.$value["correo"].'</td>

                      </tr>';
            }
         
        }

        ?>
   
        </tbody>

       </table>

      </div>

    </div>

  </section>
 </div>
 
 <?php
   $activarDistribuidor = new ControladorClientes();
  $activarDistribuidor -> ctrActivaDistribuidor();
 ?>