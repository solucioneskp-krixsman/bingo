/*=============================================
RECARGAR CLIENTE
=============================================*/
$(".tablas tbody").on("click", ".btnRecargarCliente", function(){

	var idCliente = $(this).attr("idCliente");

	var datos = new FormData();
    datos.append("idCliente", idCliente);

    $.ajax({

      url:"ajax/clientes.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success:function(respuesta){
        //console.log(respuesta);
      	 $("#idCliente").val(respuesta["id"]);
	  }

  	})

})

/*=============================================
EDITAR CLIENTE
=============================================*/
$(".tablas tbody").on("click", ".btnEditarCliente", function(){

	var idCliente = $(this).attr("idCliente");

	var datos = new FormData();
    datos.append("idCliente", idCliente);

    $.ajax({

      url:"ajax/clientes.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success:function(respuesta){
        //console.log(respuesta);
      	 $("#idClienteEditar").val(respuesta["id"]);
	  }

  	})

})

/*=============================================
ACTIVAR DESACTIVAR SLIDER
=============================================*/
$(".tablaDistribuidor").on("click", ".btnActivaDistribuidor", function(){

	var idDistribuidor = $(this).attr("idDistribuidor");
	var codigo = $(this).attr("codigo");	 
    var texto = '';
    
    if(codigo == 'activo')
    {
        texto= 'desactivar';
    }
    else
    {
        texto= 'activar';
    }
    
	 swal({
	 	title: '¿Está seguro de '+texto+' el distribuidor?',
	 	//text: "¡Si no lo está puede cancelar la acción!",
	 	type: 'warning',
	 	showCancelButton: true,
	 	confirmButtonColor: '#3085d6',
	 	cancelButtonColor: '#d33',
	 	cancelButtonText: 'Cancelar',
	 	confirmButtonText: 'Si, '+texto+' distribuidor!'
	 }).then(function(result){

	 	if(result.value){

	 		window.location = "index.php?ruta=distribuidor&idDistribuidor="+idDistribuidor+"&codigo="+codigo;

	 	}

	 })

})

/*=============================================
ELIMINAR CLIENTE
=============================================*/
$(".tablas tbody").on("click", ".btnEliminarCliente", function(){

	var idCliente = $(this).attr("idCliente");
	
	swal({
        title: '¿Está seguro de borrar el cliente?',
        text: "¡Si no lo está puede cancelar la acción!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar cliente!'
      }).then(function(result){
        if (result.value) {
          
            window.location = "index.php?ruta=clientes&idCliente="+idCliente;
        }

  })

})