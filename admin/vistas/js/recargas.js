/*=============================================
ACTIVAR DESACTIVAR SALA
=============================================*/
$(".tablasPendiente").on("click", ".btnAprobarRecarga", function(){

  var idRecarga    = $(this).attr("idRecarga");
  var idUsuario    = $(this).attr("idUsuario");
  var montoRecarga = $(this).attr("montoRecarga");
  var codigo       = $(this).attr("codigo"); 
  var texto = 'Aprobar';
    
   swal({
    title: '¿Está seguro de '+texto+' la Recarga?',
    text: "¡Esta accion no se puede deshacer!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Si, '+texto+' Recarga!'
   }).then(function(result){

    if(result.value){

         window.location = "index.php?ruta=recargas&idRecarga="+idRecarga+"&idUsuario="+idUsuario+"&montoRecarga="+montoRecarga+"&codigo="+codigo;                
    }

   })

})

/*=============================================
TRAER DATOS PARA APROBAR
=============================================*/
$(".tablaRetiro").on("click", ".btnAprobarRetiro", function(){

  var idRetiro = $(this).attr("idRetiro");

  var datos = new FormData();
  datos.append("idRetiro", idRetiro);

  $.ajax({
    url: "ajax/recarga.ajax.php",
    method: "POST",
        data: datos,
        cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success: function(respuesta){

        $("#idRetiro").val(respuesta["id"]);
        $("#idUsuario").val(respuesta["id_usuario"]);
        $("#montoRetiro").val(respuesta["monto_retiro"]);
        $("#codigo").val(respuesta["id_transaccion"]);       

      }

  })


})

/*=============================================
APROBAR RETIRO
=============================================*/
$(".btnAprobarRetiroGuardar").on("click", function(e){
  e.preventDefault();
  var texto       = 'Aprobar';
    
   swal({
    title: '¿Está seguro de '+texto+' el Retiro?',
    text: "¡Esta accion no se puede deshacer!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Si, '+texto+' Retiro!'
   }).then(function(result){

    if(result.value){

        $("#retiroAprobar").submit();
    }

   })


})

/*=============================================
SUBIENDO LA FOTO DE REFERENCIA
=============================================*/

$(".nuevaImagenRetiro").change(function(){

  var imagen = this.files[0];
  
  /*=============================================
    VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
    =============================================*/

    if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

      $(".nuevaImagenRetiro").val("");

       swal({
          title: "Error al subir la imagen",
          text: "¡La imagen debe estar en formato JPG o PNG!",
          type: "error",
          confirmButtonText: "¡Cerrar!"
        });

    }else if(imagen["size"] > 2000000){

      $(".nuevaImagenRetiro").val("");

       swal({
          title: "Error al subir la imagen",
          text: "¡La imagen no debe pesar más de 2MB!",
          type: "error",
          confirmButtonText: "¡Cerrar!"
        });

    }else{

      var datosImagen = new FileReader;
      datosImagen.readAsDataURL(imagen);

      $(datosImagen).on("load", function(event){

        var rutaImagen = event.target.result;

        $(".previsualizarRetiro").attr("src", rutaImagen);

      })

    }
})

/*=============================================
IMAGEN REFERENCIA
=============================================*/
$("#imagenModal").on("click", function(){
	var url = $(this).attr("url");
	$(".previsualizarReferencia").attr("src", url);
	
})
