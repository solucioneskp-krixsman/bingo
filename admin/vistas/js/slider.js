/*=============================================
SUBIENDO LA FOTO DEL SLIDER
=============================================*/

$(".nuevaImagenSlider").change(function(){

	var imagen = this.files[0];
	
	/*=============================================
  	VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
  	=============================================*/

  	if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

  		$(".nuevaImagenSlider").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen debe estar en formato JPG o PNG!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else if(imagen["size"] > 2000000){

  		$(".nuevaImagenSlider").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen no debe pesar más de 2MB!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else{

  		var datosImagen = new FileReader;
  		datosImagen.readAsDataURL(imagen);

  		$(datosImagen).on("load", function(event){

  			var rutaImagen = event.target.result;

  			$(".previsualizarNuevo").attr("src", rutaImagen);

  		})

  	}
})

/*=============================================
SUBIENDO LA FOTO DEL SLIDER
=============================================*/

$(".nuevaImagenEditar").change(function(){

	var imagen = this.files[0];
	
	/*=============================================
  	VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
  	=============================================*/

  	if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

  		$(".nuevaImagenEditar").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen debe estar en formato JPG o PNG!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else if(imagen["size"] > 2000000){

  		$(".nuevaImagenEditar").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen no debe pesar más de 2MB!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else{

  		var datosImagen = new FileReader;
  		datosImagen.readAsDataURL(imagen);

  		$(datosImagen).on("load", function(event){

  			var rutaImagen = event.target.result;

  			$(".previsualizarEditar").attr("src", rutaImagen);

  		})

  	}
})

/*=============================================
EDITAR SLIDER
=============================================*/
$(".tablaSlider").on("click", ".btnEditarSlider", function(){

	var idSlider = $(this).attr("idSlider");

	var datos = new FormData();
	datos.append("idSlider", idSlider);

	$.ajax({
		url: "ajax/slider.ajax.php",
		method: "POST",
      	data: datos,
      	cache: false,
     	contentType: false,
     	processData: false,
     	dataType:"json",
     	success: function(respuesta){
     		console.log(respuesta["imagen"]);
     		$("#editarTitulo").val(respuesta["titulo"]);
     		$("#editarTexto").val(respuesta["texto"]);
     		$("#idSlider").val(respuesta["id"]);
            if(respuesta["imagen"] != ""){

           	$("#imagenActual").val(respuesta["imagen"]);

           	$(".previsualizarEditar").attr("src",  respuesta["imagen"]);

           }     		

     	}

	})


})

/*=============================================
ACTIVAR DESACTIVAR SLIDER
=============================================*/
$(".tablaSlider").on("click", ".btnActivaSlider", function(){

	var idBanner = $(this).attr("idSlider");
	var codigo = $(this).attr("codigo");	 
	var imagen = $(this).attr("imagen");
    var texto = '';
    
    if(codigo == 'activa')
    {
        texto= 'desactivar';
    }
    else
    {
        texto= 'activar';
    }
    
	 swal({
	 	title: '¿Está seguro de '+texto+' el banner?',
	 	//text: "¡Si no lo está puede cancelar la acción!",
	 	type: 'warning',
	 	showCancelButton: true,
	 	confirmButtonColor: '#3085d6',
	 	cancelButtonColor: '#d33',
	 	cancelButtonText: 'Cancelar',
	 	confirmButtonText: 'Si, '+texto+' banner!'
	 }).then(function(result){

	 	if(result.value){

	 		window.location = "index.php?ruta=slider&idSlider="+idBanner+"&imagen="+imagen+"&codigo="+codigo;

	 	}

	 })

})

/*=============================================
ELIMINAR SLIDER
=============================================*/
$(".tablaSlider").on("click", ".btnEliminarSlider", function(){

	var idBanner = $(this).attr("idSlider");
	var codigo = $(this).attr("codigo");	 
	var imagen = $(this).attr("imagen");
    
	 swal({
	 	title: '¿Está seguro de eliminar el banner?',
	 	//text: "¡Si no lo está puede cancelar la acción!",
	 	type: 'warning',
	 	showCancelButton: true,
	 	confirmButtonColor: '#3085d6',
	 	cancelButtonColor: '#d33',
	 	cancelButtonText: 'Cancelar',
	 	confirmButtonText: 'Si, eliminar banner!'
	 }).then(function(result){

	 	if(result.value){

	 		window.location = "index.php?ruta=slider&idSliderBorrar="+idBanner+"&imagen="+imagen+"&codigo="+codigo;

	 	}

	 })

})