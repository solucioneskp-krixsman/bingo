/*=============================================

CARGAR LA TABLA DINÁMICA DE PRODUCTOS

=============================================*/



$.ajax({



	url: "ajax/datatable-salas.ajax.php",

	success:function(respuesta){

		

		//console.log("respuesta", respuesta);



	}



})



var perfilOculto = $("#perfilOculto").val();



$('.tablaSalas').DataTable( {

    "ajax": "ajax/datatable-salas.ajax.php?perfilOculto="+perfilOculto,

    "deferRender": true,

	"retrieve": true,

	"processing": true,

	 "language": {



			"sProcessing":     "Procesando...",

			"sLengthMenu":     "Mostrar _MENU_ registros",

			"sZeroRecords":    "No se encontraron resultados",

			"sEmptyTable":     "Ningún dato disponible en esta tabla",

			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",

			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",

			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",

			"sInfoPostFix":    "",

			"sSearch":         "Buscar:",

			"sUrl":            "",

			"sInfoThousands":  ",",

			"sLoadingRecords": "Cargando...",

			"oPaginate": {

			"sFirst":    "Primero",

			"sLast":     "Último",

			"sNext":     "Siguiente",

			"sPrevious": "Anterior"

			},

			"oAria": {

				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",

				"sSortDescending": ": Activar para ordenar la columna de manera descendente"

			}



	}



} );



/*=============================================

SUBIENDO LA FOTO DE LA SALA

=============================================*/



$(".nuevaImagenSala").change(function(){



  var imagen = this.files[0];

  

  /*=============================================

    VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG

    =============================================*/



    if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){



      $(".nuevaImagenSala").val("");



       swal({

          title: "Error al subir la imagen",

          text: "¡La imagen debe estar en formato JPG o PNG!",

          type: "error",

          confirmButtonText: "¡Cerrar!"

        });



    }else if(imagen["size"] > 2000000){



      $(".nuevaImagenSala").val("");



       swal({

          title: "Error al subir la imagen",

          text: "¡La imagen no debe pesar más de 2MB!",

          type: "error",

          confirmButtonText: "¡Cerrar!"

        });



    }else{



      var datosImagen = new FileReader;

      datosImagen.readAsDataURL(imagen);



      $(datosImagen).on("load", function(event){



        var rutaImagen = event.target.result;



        $(".previsualizarSala").attr("src", rutaImagen);



      })



    }

})



/*=============================================

CAMBIO SEGUN TIPO DE SALA

=============================================*/



$("#nuevoTipoSala").change(function(){



  var valor  = $(this).val();

  let select = '';

  

  if(valor != 1)

  {

    select +=`<span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



    <select class="form-control input-lg" id="nuevoTipoPremio" name="nuevoTipoPremio" required>

      

      <option value="">Selecionar tipo de premio</option>

      <option value="0">Monto calculado</option>

      <option value="1">Monto Designado por administrador</option>



    </select>`;

    $("#tipoPremioDiv").html(select);

  }

  else

  {

    select +=`<span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



    <select class="form-control input-lg" id="nuevoTipoPremio" name="nuevoTipoPremio" required>

      

      <option value="">Selecionar tipo de premio</option>

      <option value="2">Especial</option>      



    </select>`;

    $("#tipoPremioDiv").html(select);    

  }

})



/*=============================================

CAMBIO SEGUN TIPO DE SALA

=============================================*/



$("#editarTipoSala").change(function(){



  var valor  = $(this).val();

  let select = '';

  

  if(valor != 1)

  {

    select +=`<span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



    <select class="form-control input-lg" id="nuevoTipoPremio" name="nuevoTipoPremio" required>

      

      <option value="">Selecionar tipo de premio</option>

      <option value="0">Monto calculado</option>

      <option value="1">Monto Designado por administrador</option>



    </select>`;

    $("#editartipoPremioDiv").html(select);

  }

  else

  {

    select +=`<span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



    <select class="form-control input-lg" id="nuevoTipoPremio" name="nuevoTipoPremio" required>

      

      <option value="">Selecionar tipo de premio</option>

      <option value="2">Especial</option>      



    </select>`;

    $("#editartipoPremioDiv").html(select);    

  }

})



/*=============================================

EDITAR SALA

=============================================*/

$(".tablaSalas").on("click", ".btnEditarSala", function(){



  var idSala = $(this).attr("idSala");



  var datos = new FormData();

  datos.append("idSala", idSala);



  $.ajax({

    url: "ajax/salas.ajax.php",

    method: "POST",

        data: datos,

        cache: false,

      contentType: false,

      processData: false,

      dataType:"json",

      success: function(respuesta){



        let opciones  = '';

        let opciones2 = '';



        if(respuesta["tipo_sala"] == 0)

        {

          opciones =`<option value="">Selecionar tipo de sala</option>

                     <option value="0" selected="TRUE">Normal</option>

                     <option value="1">Especial</option>`;



          if(respuesta["premio_sala"] == 0)

          {

            opciones2 =`<span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



                        <select class="form-control input-lg" id="nuevoTipoPremio" name="nuevoTipoPremio" required>

                          

                          <option value="">Selecionar tipo de premio</option>

                          <option value="0" selected="TRUE">Monto calculado</option>

                          <option value="1">Monto Designado por administrador</option>



                        </select>`;

          }

          else if(respuesta["premio_sala"] == 1)

          {

            opciones2 =`<span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



                        <select class="form-control input-lg" id="nuevoTipoPremio" name="nuevoTipoPremio" required>

                          

                          <option value="">Selecionar tipo de premio</option>

                          <option value="0">Monto calculado</option>

                          <option value="1" selected="TRUE">Monto Designado por administrador</option>



                        </select>`;

          }                     

        }

        else if(respuesta["tipo_sala"] == 1)

        {

          opciones =`<option value="">Selecionar tipo de sala</option>

                     <option value="0">Normal</option>

                     <option value="1" selected="TRUE">Especial</option>`;

          if(respuesta["premio_sala"] == 2)

          {

            opciones2 =`<span class="input-group-addon"><i class="fa fa-dollar"></i></span> 



                        <select class="form-control input-lg" id="nuevoTipoPremio" name="nuevoTipoPremio" required>

                          

                          <option value="">Selecionar tipo de premio</option>

                          <option value="2" selected="TRUE">Especial</option>      



                        </select>`;

          }                     

        }



        $("#editarSala").val(respuesta["nombre_sala"]);

        $("#editarTipoSala").html(opciones);

        $("#editartipoPremioDiv").html(opciones2);

        $("#editarPrecioCartilla").html(respuesta["precio_cartilla_sala"]);

        $("#idSala").val(respuesta["id"]);

            if(respuesta["imagen_sala"] != ""){



            $("#imagenActualSala").val(respuesta["imagen_sala"]);



            $(".previsualizarSala").attr("src", respuesta["imagen_sala"]);



           }        



      }



  })





})



/*=============================================

SUBIENDO LA FOTO DEL PREMIO

=============================================*/



$(".nuevaImagenPremio").change(function(){



  var imagen = this.files[0];

  

  /*=============================================

    VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG

    =============================================*/



    if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){



      $(".nuevaImagenPremio").val("");



       swal({

          title: "Error al subir la imagen",

          text: "¡La imagen debe estar en formato JPG o PNG!",

          type: "error",

          confirmButtonText: "¡Cerrar!"

        });



    }else if(imagen["size"] > 2000000){



      $(".nuevaImagenPremio").val("");



       swal({

          title: "Error al subir la imagen",

          text: "¡La imagen no debe pesar más de 2MB!",

          type: "error",

          confirmButtonText: "¡Cerrar!"

        });



    }else{



      var datosImagen = new FileReader;

      datosImagen.readAsDataURL(imagen);



      $(datosImagen).on("load", function(event){



        var rutaImagen = event.target.result;



        $(".previsualizarPremio").attr("src", rutaImagen);



      })



    }

})



/*=============================================

PREMIO SALA

=============================================*/

$(".tablaSalas").on("click", ".btnPremioSala", function(){



  var idSala = $(this).attr("idSala");

  var tipoPremio = $(this).attr("tipoPremio");



  $("#modalPremioSala"+tipoPremio).modal("show");



  $("#idSalaPremio"+tipoPremio).val(idSala);

  

})



/*=============================================

ACTIVAR DESACTIVAR SALA

=============================================*/

$(".tablaSalas").on("click", ".btnActivaSala", function(){



  var idSalaA = $(this).attr("idSala");

  var codigo = $(this).attr("codigo");  

  var now = new Date(); 

  var baseUrl = $("#baseUrl").val();

  var texto = '';

    

    if(codigo == 'activa')

    {

        texto= 'desactivar';

    }

    else

    {

        texto= 'activar';

    }

    

   swal({

    title: '¿Está seguro de '+texto+' la sala?',

    //text: "¡Si no lo está puede cancelar la acción!",

    type: 'warning',

    showCancelButton: true,

    confirmButtonColor: '#3085d6',

    cancelButtonColor: '#d33',

    cancelButtonText: 'Cancelar',

    confirmButtonText: 'Si, '+texto+' sala!'

   }).then(function(result){



    if(result.value){

      $.ajax({

              url: baseUrl+"public/tiempo.php",

              data: {action: "conteo",

                   tiempo: now,

                   idSala : idSalaA},

              type: "POST",

              success: function (response) 

              {

                window.location = "index.php?ruta=salas&idSalaA="+idSalaA+"&codigo="+codigo;

                

              }

        });

      



    }



   })



})



/*=============================================

ELIMINAR SALA

=============================================*/

$(".tablaSalas").on("click", ".btnEliminarSala", function(){



  var idSala = $(this).attr("idSala");  

  var imagen = $(this).attr("imagen");



   swal({

    title: '¿Está seguro de borrar la sala?',

    text: "¡Si no lo está puede cancelar la acción!",

    type: 'warning',

    showCancelButton: true,

    confirmButtonColor: '#3085d6',

    cancelButtonColor: '#d33',

    cancelButtonText: 'Cancelar',

    confirmButtonText: 'Si, borrar sala!'

   }).then(function(result){



    if(result.value){



      window.location = "index.php?ruta=salas&idSala="+idSala+"&imagen="+imagen;



    }



   })



})



